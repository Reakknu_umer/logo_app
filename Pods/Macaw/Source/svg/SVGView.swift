import Foundation

#if os(iOS)
import UIKit
#elseif os(OSX)
import AppKit
#endif

// Src4954571_C code200818_started_ByteVite
/*
open class SVGView: MacawView {

    @IBInspectable open var fileName: String? {
        didSet {
                        node = (try? SVGParser.parse(path: fileName ?? "")) ?? Group()
            node = (try? SVGParser.parseWith(fullPath: fileName ?? "")) ?? Group()
        }
    }

    // Src4954570_A code160818_ByteVite
    @IBInspectable open var fileNameWithFullPath: String? {
        didSet {
            node = (try? SVGParser.parseWith(fullPath: fileName ?? "")) ?? Group()
        }
    }

    public init(node: Node = Group(), frame: CGRect) {
        super.init(frame: frame)
        self.node = node
    }

    override public init?(node: Node = Group(), coder aDecoder: NSCoder) {
        super.init(node: node, coder: aDecoder)
    }

    required public convenience init?(coder aDecoder: NSCoder) {
        self.init(node: Group(), coder: aDecoder)
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
    }

    override func initializeView() {
        super.initializeView()
        self.contentLayout = ContentLayout.of(contentMode: contentMode)
    }

} */
// Src4954571_C code200818_ended_ByteVite


//  SVGView.swift
//
//  Created by Md Shafi Ahmed on 17/08/18.
//  Copyright © 2018 Md Shafi Ahmed and ByteVite
//

open class SVGView: MacawView {
    
    private var _node: Node?
    private var _penColor: Color = Color.clear
    private var _penUIColor: UIColor = UIColor.lightGray //-- lightgray is hard coded as our svgs are gray colored
    private var svgBackgroundColor: Fill = Color.clear
    private var _svgAtualViewFrame: CGRect = CGRect.zero
    
    public init(node: Node = Group(), frame: CGRect) {
        super.init(frame: frame)
        self.node = node
    }
    
    override public init?(node: Node = Group(), coder aDecoder: NSCoder) {
        super.init(node: node, coder: aDecoder)
    }
    
    required public convenience init?(coder aDecoder: NSCoder) {
        self.init(node: Group(), coder: aDecoder)
    }
    
    public init(template: String, frame: CGRect, penColor: UIColor = UIColor.clear, backgroundColor: UIColor = UIColor.clear) {
        
        super.init(frame: frame)
        self.backgroundColor = backgroundColor
        
        if (penColor == UIColor.clear) {
            self._penColor = Color.clear
        }
        else {
            self._penColor = self.uicolorToMacawColorWith(color: penColor)
        }
        
        if (backgroundColor == UIColor.clear) {
            self.svgBackgroundColor = Color.clear
        }
        else {
            self.svgBackgroundColor = self.uicolorToMacawColorWith(color: backgroundColor)
        }
        
        setUp(template: template, frame: frame)
    }
    
    private func setUp (template: String, frame: CGRect) {
        if let node = try? SVGParser.parseWith(fullPath: template) {
            
            self.setSVGAtualViewFrame(node.bounds!)
//            self.frame = self.calculateSVGActualFrame
            
            if self._penColor != Color.clear {
                setSvgColor(node: node)
            }
            
            if let group = node as? Group {
                //                let w:Double = Double(frame.size.width)
                //                let h:Double = Double(frame.size.height)
                ////                if backgroundColor != UIColor.clear {
                //                    let rect = Rect.init(x: 0, y: 0, w: w, h: h)
                //                    let backgroundShape = Shape(form: rect, fill: self.svgBackgroundColor, tag: ["background"])
                //                    var contents = group.contents
                //                    contents.insert(backgroundShape, at: 0)
                //                    group.contents = contents
                ////                }
                self.node = group
            }else {
                self.node = node
            }
            self.contentMode = .scaleAspectFit
            self._node = node
        }
    }
    
    private func setSVGAtualViewFrame(_ svgFrame: Rect) {
        var frame: CGRect = self.frame
        let size = self.frame.size
        
        if frame.size.width == frame.size.height {
            if (svgFrame.w < svgFrame.h) {
                let diff = CGFloat(svgFrame.w / svgFrame.h)
                frame.size.width = frame.size.width * diff
                frame.origin.x = frame.origin.x + (size.width - frame.size.width) / 2
            }
            else if (svgFrame.h < svgFrame.w) {
                let diff = CGFloat(svgFrame.h / svgFrame.w)
                frame.size.height = frame.size.height * diff
                frame.origin.y = frame.origin.y + (size.height - frame.size.height) / 2
            }
        }
        else if frame.size.width < frame.size.height {
            let diff = CGFloat(frame.size.width / frame.size.height)
            frame.size.height = frame.size.width  * diff
            frame.origin.y = frame.origin.y + (size.height - frame.size.height) / 2
            
            //            let diff = CGFloat(svgFrame.h - svgFrame.w)/100
            //            let delta = (diff * frame.size.height)/100
            //            frame.size.height = frame.size.height + (frame.size.height * delta)
            //            frame.origin.y = frame.origin.y + (size.height - frame.size.height) / 2
        }
        else if frame.size.height < frame.size.width {
            let diff = CGFloat(frame.size.height / frame.size.width)
            frame.size.width = frame.size.height  * diff
            frame.origin.x = frame.origin.x + (size.width - frame.size.width) / 2
            
            //            let diff = CGFloat(svgFrame.w - svgFrame.h)/100
            //            let delta = (diff * frame.size.height)/100
            //            frame.size.width = frame.size.height + (frame.size.height * delta)
            //            frame.origin.x = frame.origin.x + ((size.width - frame.size.width) / 2)
        }
        
        self._svgAtualViewFrame = frame
    }
    
    private func setSvgColor(node: Node) {
        if let group = node as? Group {
            for child in group.contents {
                setSvgColor(node: child)
            }
        }
        else if let shape = node as? Shape {
            let strongSelf = self
            strongSelf.replaceColors(node: shape, color: strongSelf._penColor)
        }
    }
    
}

// MARK: Business
extension SVGView {
    private func replaceColors(node: Node, color: Fill?){ // -> Bool {
        if let shape = node as? Shape {
            shape.fill = color
        }
    }
    
    public func uicolorToMacawColorWith(color: UIColor) -> Color {
        let hexColor = self.hexStringFromUIColor(color)
        return Color(val: Int(hexColor, radix: 16)!)
    }
    
    public func macawColorWithToUIColor(color: Color) -> UIColor {
        return UIColor(red: CGFloat(color.r()), green: CGFloat(color.g()), blue: CGFloat(color.b()), alpha: CGFloat(color.a()))
    }
    
    public func hexStringFromUIColor(_ color: UIColor ) -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"%02X", rgb)
    }
    
    public func macawRectToCGRect(_ rect: Rect) -> CGRect {
        return CGRect (x: rect.x, y: rect.y, width: rect.w, height: rect.h)
    }
    
    public var calculateSVGActualSize: CGSize {
        get {
            return self._svgAtualViewFrame.size
        }
    }
    
    public var calculateSVGActualFrame: CGRect {
        get {
            return self._svgAtualViewFrame
        }
    }
    
    public func setPenColor(_ color: UIColor) {//throws -> Node {
        guard self._node != nil else {
            return
            //            throw self._node
        }
        self._penUIColor = color
        let macawColor = self.uicolorToMacawColorWith(color: color)
        self._penColor = macawColor
        
        self.setSvgColor(node: self._node!)
    }
    
    public func penColor() -> UIColor {
//        guard self._node != nil else {
//            return UIColor.clear
//        }
        return self._penUIColor
    }
}




