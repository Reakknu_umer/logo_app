//
//  UIColorExtension.swift
//
//  Created by Shafi Ahmed on 24/07/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

extension UIColor {
    var coreImageColor: CIColor {
        return CIColor(color: self)
    }
    var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        let coreImageColor = self.coreImageColor
        return (coreImageColor.red, coreImageColor.green, coreImageColor.blue, coreImageColor.alpha)
    }
}

public extension UIColor {
    public func colorWithBrightness(brightness: CGFloat) -> UIColor {
        var H: CGFloat = 0, S: CGFloat = 0, B: CGFloat = 0, A: CGFloat = 0
        
        if getHue(&H, saturation: &S, brightness: &B, alpha: &A) {
            B += (brightness - 1.0)
            B = max(min(B, 1.0), 0.0)
            
            return UIColor(hue: H, saturation: S, brightness: B, alpha: A)
        }
        
        return self
    }
}

public extension UIColor {
    public func colorWithSaturation(saturation: CGFloat) -> UIColor {
        var H: CGFloat = 0, S: CGFloat = 0, B: CGFloat = 0, A: CGFloat = 0
        
        if getHue(&H, saturation: &S, brightness: &B, alpha: &A) {
            S += (saturation - 1.0)
            S = max(min(B, 1.0), 0.0)
            
            return UIColor(hue: H, saturation: S, brightness: B, alpha: A)
        }
        
        return self
    }
}

public extension UIColor {
    
    public func lighter(by percentage: CGFloat) -> UIColor {
        return self.adjustBrightness(by: abs(percentage))
    }

    public func darker(by percentage: CGFloat) -> UIColor {
        return self.adjustDarkess(by: -abs(percentage))
    }
    
    public func getBrightness() -> CGFloat {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            if b < 1.0 {
                return b //-- return brightness
            } else {
                return abs (s - 0)//-- return saturation
            }
        }
        return 0
    }
    
    public func getDarkness() -> CGFloat {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            if b < 1.0 {
                return (0 - b) //-- return brightness
            } else {
                return (0 - s) //-- return saturation
            }
        }
        return 0
    }
    
    //-- decrease saturation to increase brightness
    public func adjustBrightness(by percentage: CGFloat = 30.0) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            if b < 1.0 {
                let newB: CGFloat
                if b == 0.0 {
                    newB = max(min(b + percentage/100, 1.0), 0.0)
                } else {
                    newB = max(min(b + (percentage/100.0)*b, 1.0), 0,0)
                }
                print ("brightness set for \(newB)")
                return UIColor(hue: h, saturation: s, brightness: newB, alpha: a)
            } else {
                let newS: CGFloat = min(max(s - (percentage/100.0)*s, 0.0), 1.0)
                print ("saturation set for \(newS - 1.0)")
                return UIColor(hue: h, saturation: newS, brightness: b, alpha: a)
            }
        }
        return self
    }
    
    //-- increase saturation to decrease brightness
    public func adjustDarkess(by percentage: CGFloat = 30.0) -> UIColor {
        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
            if b <= 1.0 {
                let newB: CGFloat
                if b == 0.0 {
                    newB = max(min(b + percentage/100, 1.0), 0.0)
                } else {
                    newB = max(min(b + (percentage/100.0)*b, 1.0), 0,0)
                }
                print ("brightness set for \(newB)")
                return UIColor(hue: h, saturation: s, brightness: newB, alpha: a)
            } else {
                let newS: CGFloat = min(max(s - (percentage/100.0)*s, 0.0), 1.0)
                print ("saturation set for \(newS)")
                return UIColor(hue: h, saturation: newS, brightness: b, alpha: a)
            }
        }
        return self
    }
    
//    public func calculateBrightness(by percentage: CGFloat) -> CGFloat {
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            if b < 1.0 {
//                let newB: CGFloat
//                if b == 0.0 {
//                    newB = max(min(b + percentage/100, 1.0), 0.0)
//                } else {
//                    newB = max(min(b + (percentage/100.0)*b, 1.0), 0,0)
//                }
//                return newB
//            } else {
//                let newS: CGFloat = min(max(s - (percentage/100.0)*s, 0.0), 1.0)
//                return newS
//            }
//        }
//        return 0.0
//    }
//
//    //-- increase saturation to decrease brightness
//    public func calculateDarkess(by percentage: CGFloat) -> CGFloat {
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            if b <= 1.0 {
//                let newB: CGFloat
//                if b == 0.0 {
//                    newB = max(min(b + percentage/100, 1.0), 0.0)
//                } else {
//                    newB = max(min(b + (percentage/100.0)*b, 1.0), 0,0)
//                }
//                return newB
//            } else {
//                let newS: CGFloat = min(max(s - (percentage/100.0)*s, 0.0), 1.0)
//                return newS
//            }
//        }
//        return 0.0
//    }
//
//    //-- decrease saturation to increase brightness
//    public func adjustBrightness(by percentage: CGFloat = 30.0) -> UIColor {
//        var newBOrS: CGFloat //-- Brightness Or Saturation
//        newBOrS = calculateBrightness(by: percentage) //-- returns either brightness or saturation
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//
//        print ("Brightness or Saturation is: \(newBOrS)")
//
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            if b < 1.0 {
//                return UIColor(hue: h, saturation: s, brightness: newBOrS, alpha: a)
//            } else {
//                return UIColor(hue: h, saturation: newBOrS, brightness: b, alpha: a)
//            }
//        }
//        return self
//    }
//
//    //-- increase saturation to decrease brightness
//    public func adjustDarkess(by percentage: CGFloat = 30.0) -> UIColor {
//        var newBOrS: CGFloat //-- Brightness Or Saturation
//        newBOrS = calculateBrightness(by: percentage) //-- returns either brightness or saturation
//        var h: CGFloat = 0, s: CGFloat = 0, b: CGFloat = 0, a: CGFloat = 0
//
//        print ("Brightness or Saturation is: \(newBOrS)")
//
//        if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
//            if b <= 1.0 {
//                return UIColor(hue: h, saturation: s, brightness: newBOrS, alpha: a)
//            } else {
//                return UIColor(hue: h, saturation: newBOrS, brightness: b, alpha: a)
//            }
//        }
//        return self
//    }
    
}

extension UIColor {
    public func changeBrightness(b: CGFloat, saturation s: CGFloat = 1.0, hue h: CGFloat = 1.0) -> UIColor? {
        var hue : CGFloat = 0.0
        var saturation : CGFloat = 0.0
        var brightness : CGFloat = 0.0
        var alpha : CGFloat = 0.0
        
        if self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            brightness *= b
            brightness = max(min(brightness, 1.0), 0.0)
            saturation *= s
            saturation = max(min(saturation, 1.0), 0.0)
            hue *= h
            hue = max(min(hue, 1.0), 0.0)
            
            return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        }
        
        var white: CGFloat = 0.0
        if self.getWhite(&white, alpha: &alpha) {
            white += (b - 1.0)
            white = max(min(b, 1.0), 0.0)
            
            return UIColor(white: white, alpha: alpha)
        }
        
        return nil
    }
    
    public func sRGB2Linear(x:CGFloat) -> CGFloat {
        let a: CGFloat = 0.055;
        if(x <= 0.04045){
            return x * (1.0 / 12.92);
        }else{
            return pow((x + a) * (1.0 / (1 + a)), 2.4);
        }
    }
    
    public func linear2sRGB(x: CGFloat) -> CGFloat {
        let a:CGFloat  = 0.055;
        if(x <= 0.0031308){
            return x * 12.92;
        }else{
            return (1 + a) * pow(x, 1 / 2.4) - a;
        }
    }
    
    public func lighten(value: CGFloat) ->UIColor{
        let components = cgColor.components
        var newR = sRGB2Linear(x: components![0]+1) * value
        
        var newG = sRGB2Linear(x: components![1]+1) * value
        
        var newB = sRGB2Linear(x: components![2]+1) * value
        newR = max(0, min(1, linear2sRGB(x: newR)) )
        newG = max(0, min(1, linear2sRGB(x: newG)))
        newB = max(0, min(1, linear2sRGB(x: newB)))
        
        newR = (newR * 299) / 1000
        newG = (newG * 587) / 1000
        newB = (newB * 114) / 1000
//        (((red * 299) + (green * 587) + (blue * 114)) * 255) / 1000)
        return UIColor (red: newR, green: newG, blue: newB, alpha: components![3])
    }
}







