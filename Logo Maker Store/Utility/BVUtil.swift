//
//  BVResizabbleTextView.swift
//  BVTextView
//
//  Created by Shafi Ahmed on 19/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit

@objc class Utility : NSObject {
    @objc public class func testUtility(withParameter parameter : String) {
        print(parameter)
    }
    
    
    @objc public class func testArray(withArray array : NSMutableArray) -> NSMutableArray {
        let swiftArray : [String] = [String]()
        let objArray : NSMutableArray = NSMutableArray()
        for object in swiftArray {
            objArray.add(object)
        }
        return objArray
    }
    
    
    
    
    
    struct HSBA {
        var h:CGFloat
        var s:CGFloat
        var b:CGFloat
        var a:CGFloat
    }
    
    struct RGBA {
        var r:CGFloat
        var g:CGFloat
        var b:CGFloat
        var a:CGFloat
    }
    
    static func CGRectGetCenter(_ rect: CGRect) -> CGPoint{
        return CGPoint(x: rect.midX, y: rect.midY)
    }
    
    static func CGRectReduceBy(_ offset: CGFloat, rect: CGRect) -> CGRect {
        var frame: CGRect = rect
        frame.origin.x = rect.origin.x + offset/2
        frame.origin.y = rect.origin.y + offset/2
        frame.size.width = rect.size.width - offset
        frame.size.height = rect.size.height - offset
        return frame
    }
    
    static func CGRectScale(_ rect: CGRect, wScale: CGFloat, hScale: CGFloat) -> CGRect {
        return CGRect(x: rect.origin.x * wScale, y: rect.origin.y * hScale, width: rect.size.width * wScale, height: rect.size.height * hScale)
    }
    
    static func CGpointGetDistance(_ point1: CGPoint, point2: CGPoint) -> CGFloat {
        let fx = point2.x - point1.x
        let fy = point2.y - point1.y
        
        return sqrt((fx * fx + fy * fy))
    }
    
    static func CGAffineTrasformGetAngle(_ t: CGAffineTransform) -> CGFloat{
        return atan2(t.b, t.a)
    }
    
    static func CGAffineTransformGetScale(_ t: CGAffineTransform) -> CGSize {
        return CGSize(width: sqrt(t.a * t.a + t.c + t.c), height: sqrt(t.b * t.b + t.d * t.d))
    }
    
    static func rgbaToHsba(_ color: UIColor) -> HSBA {
        var hsba = HSBA(h:0, s:0, b:0, a:0)
        color.getHue(&hsba.h, saturation: &hsba.s, brightness: &hsba.b, alpha: &hsba.a)
        
        return hsba
    }
    
    static func hsbaToRgba(_ color: UIColor) -> RGBA {
        var rgba = RGBA(r:0, g:0, b:0, a:0)
        color.getRed(&rgba.r, green: &rgba.g, blue: &rgba.b, alpha: &rgba.a)
        
        return rgba
    }
    
    static func changeColorBrightness(_ brightness:CGFloat, color:UIColor) -> UIColor {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        if (color.getHue(&h, saturation: &s, brightness: &b, alpha: &a)) {
            b = (brightness * 1.0)
            print(" bright in change: \(s)")
//            b = max(min(b, 1.0), 0.0)
            return UIColor (hue: h, saturation: s, brightness: b, alpha: a)
        }
        
        var white:CGFloat = 0
        color .getWhite(&white, alpha:&a)
        white += (brightness - 1.0)
        white = max(min(white, 1.0), 0.0)
        return UIColor(white: white, alpha: a)
    }
    
    static func changeColorSaturation(_ saturation:CGFloat, color:UIColor) -> UIColor {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        if (color.getHue(&h, saturation: &s, brightness: &b, alpha: &a)) {
            s = (saturation * 1.0)
//            s = max(min(s, 1.0), 0.0)
            print(" sat in change: \(s)")
            return UIColor (hue: h, saturation: s, brightness: b, alpha: a)
        }
        
        var white:CGFloat = 0
        color .getWhite(&white, alpha:&a)
        white += (saturation - 1.0)
        white = max(min(white, 1.0), 0.0)
        return UIColor(white: white, alpha: a)
    }
    
    static func calculateBrightness(color:UIColor) -> CGFloat {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        if (color.getHue(&h, saturation: &s, brightness: &b, alpha: &a)) {
            b = (b * 1.0)
//            b = max(min(b, 1.0), 0.0)
            return b
        }
        
        var white:CGFloat = 0
        color .getWhite(&white, alpha:&a)
        white = abs(white - 1.0)
//        white = max(min(white, 1.0), 0.0)
        return white
    }
    
    static func calculateSaturation(color:UIColor) -> CGFloat {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        if (color.getHue(&h, saturation: &s, brightness: &b, alpha: &a)) {
            s = abs(s - 1.0)
//            s = max(min(s, 1.0), 0.0)
            
            return s
        }
        
        var white:CGFloat = 0
        color .getWhite(&white, alpha:&a)
        white = abs(white - 1.0)
        white = max(min(white, 1.0), 0.0)
        return white
    }
    
    static func hueFrom(color:UIColor) -> CGFloat {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        color.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
        return h;
    }
    
    /*-------------------------------------------------------------------------*/
    
    static func fileHasExtension(_ file: String) -> Bool {
        guard file.contains(".") else {
            return false
        }
        return true
    }
    
    static func extensionFrom(file: String) -> String {
        guard fileHasExtension(file) else {
            return ""
        }
        let indexOfDotChar = file.index(of: ".")
        let positionOFDotChar = file.distance(from: file.startIndex, to: indexOfDotChar!)
        let extn = String(file.suffix(file.count - positionOFDotChar))
        return extn
    }
    
    static func deleteExtensionFrom(file: String, extnsionWithDotPrefix: String) -> String {
        guard file.hasSuffix(extnsionWithDotPrefix) else {
            return file
        }
        return String(file.dropLast(extnsionWithDotPrefix.count))
    }
    
    static func addExtensionTo(file: String, fileExtension: String) -> String {
        guard !fileHasExtension(file) else {
            return file
        }
        guard !file.hasSuffix(fileExtension) else {
            return file
        }
        return String("\(file)\(fileExtension)")
    }
    
    //--MARK: load contents(files, folders) in a given directory and returned in a SORTED array
    static func contentFrom(path: String, byDeletingExtn: Bool? = false, ignoreStringSeparatedByComma: String = "") -> [String] {
        do {
            var pngFileList = [String]()
            var ignoreList = [Substring] ()
            
            let fileManager = FileManager.default
            let currentPath: String? = Bundle.main.path(forResource: path, ofType: nil)
            guard currentPath != nil else { return [] }
            
            if ignoreStringSeparatedByComma.count > 0 {
                ignoreList = ignoreStringSeparatedByComma.split(separator: ",")
            }
            
            let filelist = try fileManager.contentsOfDirectory(atPath: currentPath!)
            for fileName:String in filelist {
                //-- deletingPathExtension wouldn't work if file has white space in its name.
//                var fileWithoutExtn:String = (NSURL(string: fileName)?.deletingPathExtension?.path)!
                var fileWithoutExtn:String = Utility.deleteExtensionFrom(file: fileName, extnsionWithDotPrefix:SYMBOL_THUMBNAIL_FILE_EXTN)
                
                //-- remove @2x, @3x if there are files for different scales for retina display
                if fileName.contains("@"){
                    fileWithoutExtn.removeLast(3)
                }
                
                if !byDeletingExtn! { //-- add the extension back to the file
                    //-- aggain pathExtension or a return by deletingLastPathComponent wouldn't work if file has white space in its name.
                    //let extn: String = (NSURL(string: fileName)?.pathExtension)!
                    let extn:String = Utility.extensionFrom(file: fileName)
                    if (extn.count > 0) {
                        fileWithoutExtn = "\(fileWithoutExtn)\(String(describing: extn))"
                    }
                }

                if ignoreList.contains(Substring(fileWithoutExtn)) {
                    continue; //-- if ignored file found, force loop to return
                }
                
                if !pngFileList.contains(fileWithoutExtn) { //-- check if the file is already added
//                    if !byDeletingExtn! { //-- add the extension back to the file
//                        //-- aggain pathExtension or a return by deletingLastPathComponent wouldn't work if file has white space in its name.
//                        //let extn: String = (NSURL(string: fileName)?.pathExtension)!
//                        let extn:String = Utility.extensionFrom(file: fileName)
//                        if (extn.count > 0) {
//                            fileWithoutExtn = "\(fileWithoutExtn)\(String(describing: extn))"
//                        }
//                    }
                    pngFileList.append(fileWithoutExtn)
//                    print("\n \(#function) **** file added to array \(fileWithoutExtn)")
                }
            }
            pngFileList.sort()
            return pngFileList
            
        }catch let error {
            print("Error: \(error.localizedDescription)")
            return []
        }
    }
    
    static func relativePathFor(file: String, type:String, directory: String) -> String {
        guard let path = Bundle.main.path(forResource: file, ofType: type, inDirectory: directory) else {
            return file
        }
        return path
    }
    
    static func symbolPathFor(_ category: String, fileType: Constants.SymbolType) -> String {
        guard category.count > 0 else {
            return ""
        }
        let path  = String("\(SYMBOL_CATEGORY_PATH)/\(category)/\(fileType)")
        return path
    }
    
    static func galleryMenuPathFor(_ category: String) -> String {
        guard category.count > 0 else {
            return ""
        }
        let path  = String("\(GALLERY_CATEGORY_MENU_PATH)/\(category)")
        return path
    }
    
    static func galleryPathFor(_ category: String) -> String {
        guard category.count > 0 else {
            return ""
        }
        let path  = String("\(GALLERY_CATEGORY_PATH)/\(category)")
        return path
    }
    static func galleryPathForThumb(_ category: String) -> String {
        guard category.count > 0 else {
            return ""
        }
        let path  = String("\(GALLERY_CATEGORY_PATH)/\(category)")
        return path
    }
    
    
    
    
   
    
//    static func uniqueFilenameWithPrefix(_ prefix: String, fileExtension: String) -> String {
//        guard !DataManager.logoExistsWith(prefix: "") else {
//            return self.addExtensionTo(file:prefix, fileExtension:fileExtension)
//        }
//        var uniqueName: String = ""
//
//        return uniqueName
//    }
//
//    static func uniqueFileName() -> String {
//        let uniqueName: String = self.uniqueFilenameWithPrefix("LogoStore", fileExtension:"Default logo name prefix")
//
//        return uniqueName
//    }
}

extension UIColor {
    public convenience init?(hexString: String) {
        let r, g, b, a: CGFloat
        
        if hexString.hasPrefix("#") {
            let start = hexString.index(hexString.startIndex, offsetBy: 1)
            let hexColor = String(hexString[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
            if hexColor.count == 6 {
                let cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
                var rgbValue:UInt32 = 0
                Scanner(string: cString).scanHexInt32(&rgbValue)
                self.init(
                    red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                    green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                    blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                    alpha: CGFloat(1.0)
                )
            }
        }
        
        return nil
    }
}








