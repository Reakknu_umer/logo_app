//
//  CategoryViewCell.swift
//  Logo Maker Store
//
//  Created by Md Shafi Ahmed on 05/09/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

private let kBorderWidthDefault: CGFloat = 3
private let kCornerRadius: CGFloat = 6
private var kBorderColorDefault: UIColor = UIColor(red: 87/255, green: 163/255, blue: 192/255, alpha: 1)

class CategoryViewCell: UICollectionViewCell {
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var mainImage: UIImageView!
    @IBOutlet var mainTitle: UILabel!
    
    private var border: CAShapeLayer?
    private var _borderWidth: CGFloat =  kBorderWidthDefault
    private var _borderColor: UIColor = kBorderColorDefault
    private var _cornerRadius: CGFloat = kCornerRadius
    
    public override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainImage.image = nil
    }
    
    override func prepareForReuse() {
        self.mainImage.image = nil
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.mainImage.layer.cornerRadius = self.cornerRadius
        self.setupBorder()
    }
    
    //    override var isHighlighted: Bool {
    //        willSet {
    //            print("\n isHighlighted \(newValue)")
    //        }
    //    }
    
    internal var borderderWidth: CGFloat {
        
        set {
            self._borderWidth = newValue
//            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._borderWidth
        }
    }
    
    internal var borderderColor: UIColor {
        set {
            self._borderColor = newValue
//            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._borderColor
        }
    }
    
    internal var cornerRadius: CGFloat {
        set {
            self._cornerRadius = newValue
//            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._cornerRadius
        }
    }
    
    override var isSelected: Bool {
        willSet {
//            print("isSelected \(newValue)")
            if newValue == true {
                self.addBorder()
            }
            else {
                self.removeBorder()
            }
        }
    }
    
    func setupBorder() {
        border = CAShapeLayer(layer: layer)
        border?.strokeColor = self.borderderColor.cgColor
        border?.fillColor = nil
        border?.lineWidth = self.borderderWidth
        border?.cornerRadius = self.cornerRadius
    }
    
    func removeBorder() {
        border?.strokeColor = UIColor.clear.cgColor
        border?.removeFromSuperlayer()
    }
    
    func addBorder() {
        border?.strokeColor = self.borderderColor.cgColor
        self.layer.insertSublayer(border!, at: 1)
    }
    
    override open func layoutSubviews() {
//        border?.path = UIBezierPath(rect: self.mainImage.frame).cgPath
        border?.path = UIBezierPath(roundedRect: self.mainImage.frame, cornerRadius: self.cornerRadius).cgPath
        let frame: CGRect = self.mainImage.bounds
        border?.frame = frame
        
        self.mainImage.layer.cornerRadius = self.cornerRadius
    }
}










