//
//  MenuTableViewCell.m
//  Logo Maker Store
//
//  Created by iBuildX on 22/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "MenuTableViewCell.h"

@implementation MenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
