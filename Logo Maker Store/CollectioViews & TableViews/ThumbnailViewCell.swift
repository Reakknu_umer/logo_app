//
//  ThumbnailViewCell.swift
//  Logo Maker Store
//
//  Created by Md Shafi Ahmed on 05/09/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

private let kCornerRadius: CGFloat = 6
private let kBorderWidthDefault: CGFloat = 2
private var kBorderColorDefault: UIColor = UIColor(red: 87/255, green: 163/255, blue: 192/255, alpha: 1)
private let kFontSizeMax: CGFloat = 15

class ThumbnailViewCell: UICollectionViewCell {
    
    @IBOutlet var thumbImage: UIImageView!
    @IBOutlet var thumbLabel: UILabel!
    
    private var border: CAShapeLayer?
    private var _borderWidth: CGFloat =  kBorderWidthDefault
    private var _borderColor: UIColor = kBorderColorDefault
    private var _cornerRadius: CGFloat = kCornerRadius
    
    public override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        self.layer.cornerRadius = self.cornerRadius
        self.clipsToBounds = true
        self.setupBorder()
    }
    
    internal var borderderWidth: CGFloat {

        set {
            self._borderWidth = newValue
            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._borderWidth
        }
    }
    
    internal var borderderColor: UIColor {
        set {
            self._borderColor = newValue
            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._borderColor
        }
    }
    
    internal var cornerRadius: CGFloat {
        set {
            self._cornerRadius = newValue
            self.setupBorder()
            self.layoutSubviews()
        }
        get {
            return self._cornerRadius
        }
    }
    
    
    //    override var isHighlighted: Bool {
    //        willSet {
    //            print("\n isHighlighted \(newValue)")
    //        }
    //    }
    
    internal override var isSelected: Bool {
        willSet {
            //            print("isSelected \(newValue)")
            if newValue == true {
                self.addBorder()
//                self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }
            else {
                self.removeBorder()
            }
        }
    }
    
    private func setupBorder() {
        border = CAShapeLayer(layer: layer)
        border?.strokeColor = self.borderderColor.cgColor
        border?.fillColor = nil
        border?.lineWidth = self.borderderWidth
        border?.cornerRadius = self.cornerRadius
    }
    
    private func removeBorder() {
        border?.strokeColor = UIColor.clear.cgColor
        border?.removeFromSuperlayer()
    }
    
    private func addBorder() {
        border?.strokeColor = self.borderderColor.cgColor
        self.thumbImage?.layer.insertSublayer(border!, at: 0)
        border?.cornerRadius = self.cornerRadius
    }
    
    internal override func layoutSubviews() {
        var frame = self.bounds
        
        //-- align imageview to its cell
        if thumbLabel != nil {
            frame.size.height = self.frame.width
        }
        self.thumbImage?.frame = frame
        self.thumbImage?.layer.cornerRadius = self.cornerRadius
        
        //-- border
        frame = (self.thumbImage?.bounds)!
        frame = Utility.CGRectReduceBy(self.borderderWidth/2, rect: frame)
        border?.path = UIBezierPath(roundedRect: frame, cornerRadius: self.cornerRadius).cgPath
        
        guard thumbLabel != nil else {
            return
        }
        
//        //-- thumb label
        let padding: CGFloat = thumbImage.frame.size.height / 50
        let delta: CGFloat = thumbImage!.frame.height / 11.0   //7.20
        var fontSize: CGFloat  = thumbImage!.frame.height / delta
        if fontSize > kFontSizeMax {
            fontSize = kFontSizeMax
        }
        thumbLabel.font = thumbLabel.font.withSize(fontSize)
        
        frame = thumbLabel.frame
        frame.size.width = thumbImage.frame.width
        thumbLabel?.center = thumbImage.center
        frame.origin.x = thumbImage.frame.origin.x
        frame.origin.y = thumbImage.frame.size.height + thumbImage.frame.origin.y + padding
        thumbLabel?.frame = frame
        
    }
}

class CategoryFlowLayout: UICollectionViewFlowLayout {
    
    private var firstSetupDone = false
    
    override func prepare() {
        super.prepare()
        if !firstSetupDone {
            setup()
            firstSetupDone = true
        }
    }
    
    private func setup() {
        scrollDirection = .horizontal
        let padding: CGFloat = (collectionView!.frame.height * 20) / 100
        minimumLineSpacing = padding
//        minimumInteritemSpacing = 10
        let height: CGFloat = collectionView!.frame.size.height
        itemSize = CGSize(width: height - padding, height: height)
        
        collectionView!.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        collectionView!.decelerationRate = UIScrollViewDecelerationRateFast
    }
    
}






