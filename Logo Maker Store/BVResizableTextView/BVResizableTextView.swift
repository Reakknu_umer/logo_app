//
//  BVResizableTextView.swift
//  BVTextView
//
//  Created by Shafi Ahmed on 19/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit

@objc public protocol BVResizableTextViewDelegate: NSObjectProtocol {
    //-- Occurs when a touch gesture event occurs on close button.
    @objc optional func labelViewDidClose(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was shown.
    @objc optional func labelViewDidShowEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was hidden.
    @objc optional func labelViewDidHideEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when label become first responder.
    @objc optional func labelViewDidBeginEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label lose being first responder.
    @objc optional func labelViewDidEndEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label starts move or rotate.
    @objc optional func labelViewDidBeginMoving(_ label: BVLabelView) -> Void
    
    //--Occurs when label continues move or rotate.
    @objc optional func labelViewDidMoving(_ label: BVLabelView) -> Void
    
    //-- Occurs when label ends move or rotate.
    @objc optional func labelViewDidSelected(_ label: BVLabelView) -> Void
}

@objcMembers public class BVResizableTextView: UIImageView, UIGestureRecognizerDelegate {
    
    private let kAllowBeginUndoGrouping: Bool = true
    private let kLabelSizeDefault: CGSize = CGSize(width: 60, height: 40)
    private let kRnadomBaseValueForLabelPosition: CGFloat = 100
    
    //TODO: currentlyEditingLabel should be private, ad a getter for this to be acces from outside this class
    public var currentlyEditingLabel: BVLabelView!
    
    public var labelsStack: NSMutableArray!
    private var renderedView: UIView!
    var delegate: BVResizableTextViewDelegate?
    
    var beginUndoGroupingFlag: Bool = false
    var undoer = UndoManager()
    override public var undoManager : UndoManager? {
        return self.undoer
    }
    
    fileprivate lazy var tapOutsideGestureRecognizer: UITapGestureRecognizer! = {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BVResizableTextView.tapOutside))
        tapGesture.delegate = self
        return tapGesture
        
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        isUserInteractionEnabled = true
        labelsStack = []
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        labelsStack = []
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isUserInteractionEnabled = true
        labelsStack = []
    }
}

extension BVResizableTextView: BVLabelViewDelegate {
    
    public func labelViewDidClose(_ label: BVLabelView) {
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidClose(_:))) {
                delegate.labelViewDidClose!(label)
                self.removeLabel(label)
            }
        }
    }
    
    public func labelViewDidShowEditingHandles(_ label: BVLabelView) {
        currentlyEditingLabel = label
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidShowEditingHandles(_:))) {
                delegate.labelViewDidShowEditingHandles!(label)
            }
        }
    }
    
    public func labelViewDidHideEditingHandles(_ label: BVLabelView) {
        currentlyEditingLabel = nil
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidHideEditingHandles(_:))) {
                delegate.labelViewDidHideEditingHandles!(label)
            }
        }
    }
    
    public func labelViewDidStartEditing(_ label: BVLabelView) {
        currentlyEditingLabel = label
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidBeginEditing(_:))) {
                delegate.labelViewDidBeginEditing!(label)
            }
        }
    }
    
    public func labelViewDidEndEditing(_ label: BVLabelView) {
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidEndEditing(_:))) {
                delegate.labelViewDidEndEditing!(label)
            }
        }
    }
    
    public func labelViewDidSelected(_ label: BVLabelView) {
        for labelItem in labelsStack {
            if let label: BVLabelView = labelItem as? BVLabelView {
                label.hideEditingHandlers()
            }
        }
        label.showEditingHandles()
        
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidSelected(_:))) {
                delegate.labelViewDidSelected!(label)
            }
        }
    }
    
    public func labelViewDidBeginEditing(_ label: BVLabelView) {    //-- begin moving or rotating
        /* labels.removeObject(label) */
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidBeginMoving(_:))) {
                delegate.labelViewDidBeginMoving!(label)
            }
        }
    }
    
    public func labelViewDidChangeEditing(_ label: BVLabelView) { //-- label is still moving or rotating
        if let delegate: BVResizableTextViewDelegate = delegate {
            if delegate.responds(to: #selector(BVResizableTextViewDelegate.labelViewDidMoving(_:))) {
                delegate.labelViewDidMoving!(label)
            }
        }
    }
}

extension BVResizableTextView {
    
    func randomPontOnScreenWith(baseVale: CGFloat) -> CGPoint {
        let kLabelRandomPosition: CGPoint = CGPoint(x: CGFloat(arc4random()).truncatingRemainder(dividingBy: baseVale),
                                                            y: CGFloat(arc4random()).truncatingRemainder(dividingBy: baseVale))
        return kLabelRandomPosition
    }
    
    //-- MARK: Add label
    public func addNewLabel(_ newlable: BVLabelView) {
        if self.undoer.isUndoing {
            removeLabel(newlable)
        }
        (self.undoer.prepare(withInvocationTarget: self) as AnyObject).addNewLabel(newlable)
        self.undoer.setActionName("addLabel")
        labelsStack.add(newlable)
    }
    
    func removeLabel(_ label: BVLabelView) {
        label.removeFromSuperview()
        labelsStack.remove(label)
        print("labels in the stack are: \(labelsStack.count)")
    }
    
    public func addLabel() {
        if let label: BVLabelView = currentlyEditingLabel {
            label.hideEditingHandlers()
        }
        
//        (self.undoer.prepare(withInvocationTarget: self) as AnyObject).addLabel(currentlyEditingLabel.labelTextView.fontName)
        
        let randomPoint:CGPoint = self.randomPontOnScreenWith(baseVale: kRnadomBaseValueForLabelPosition)
        let frame = CGRect(x: bounds.midX - randomPoint.x, y: bounds.midY - randomPoint.y,
                                width: kLabelSizeDefault.width, height: kLabelSizeDefault.height)
        
        //labelView.enableMoveRestriction = false
        let labelView = BVLabelView(frame: frame)
        labelView.delegate = self
        labelView.showsContentShadow = false
        labelView.labelTextView.fontName = kFontNameDefault
        labelView.borderColor = kBorderColorDefault
        addSubview(labelView)
        currentlyEditingLabel = labelView
        adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
//        labels.add(labelView)
        self.addNewLabel(labelView)
        print("labels in the stack are: \(labelsStack.count)")
        
        addGestureRecognizer(tapOutsideGestureRecognizer) //-- now labelView will lose its focus if tapped outside the labelView
    }
    
    public func renderTextOnView(_ view: UIView) -> UIImage? {
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0)
        
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        return img
    }
    
    public func limitImageViewToSuperView() {
        if superview == nil {
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = true
        let imageSize = self.image?.size
        let aspectRatio = imageSize!.width / imageSize!.height
        
        if let width = imageSize?.width, let height = imageSize?.height {
            if width > height {
                bounds.size.width = superview!.bounds.size.width
                bounds.size.height = superview!.bounds.size.width / aspectRatio
            }else {
                bounds.size.height = superview!.bounds.size.height
                bounds.size.width = superview!.bounds.size.height * aspectRatio
            }
        }
    }
}

extension BVResizableTextView {
    
    func tapOutside() {
        if let _: BVLabelView = currentlyEditingLabel {
            currentlyEditingLabel.hideEditingHandlers()
        }
    }
}

extension BVResizableTextView: adjustFontSizeToFillRectProtocol {
    
    public enum textShadowPropterties {
        case offSet(CGSize)
        case color(UIColor)
        case blurRadius(CGFloat)
    }
    
    public func setFontName(_ name: String) {
        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setFontName(currentlyEditingLabel.labelTextView.fontName)
            self.undoer.setActionName("setFontName")
            currentlyEditingLabel.labelTextView.fontName = name
            adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        }
    }
    
    public func fontName()-> String {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.fontName
        }
        return kFontNameDefault
    }
    
    public var fontSize: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.fontSize = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
                
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.fontSize
            }
            return kFontSizeDefault
        }
    }
    
    /**
     It calculates and returns the outcome of the division of the two parameters.
     
     ## Important Notes ##
     1. Both parameters are **double** numbers.
     2. For a proper result the second parameter *must be other than 0*.
     3. If the second parameter is 0 then the function will return nil.
     
     */
    func performDivision(number1: Double, number2: Double) -> Double! {
        if number2 != 0 {
            return number1 / number2
        }
        else {
            return nil
        }
    }
    
   @objc public var textColor: UIColor! {

///        undoManager.setActionName(NSLocalizedString("actions.update", comment: "Update Score"))
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVResizableTextView.textColor), object: currentlyEditingLabel.labelTextView.foregroundColor)
                currentlyEditingLabel.labelTextView.foregroundColor = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.foregroundColor
            }
            return UIColor.clear
        }
    }
    
    public var backgroundColour: UIColor! {
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVResizableTextView.backgroundColour), object: currentlyEditingLabel.labelTextView.backgroundColour)
                currentlyEditingLabel.labelTextView.backgroundColour = newValue
            }
        }
        
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.backgroundColour
            }
            return UIColor.clear
        }
    }
    
    @objc public func setTextOpacity(_ newValue: CGFloat) {
        
        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setTextOpacity(currentlyEditingLabel.labelTextView.textOpacity)
            self.undoer.setActionName("textOpacity")
            currentlyEditingLabel.labelTextView.textOpacity = newValue
        }
    }
    func textOpacity() ->CGFloat {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.textOpacity
        }
        return 1.0
    }
    
    @objc public var backgroundOpacity: CGFloat {
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVResizableTextView.backgroundOpacity), object: currentlyEditingLabel.labelTextView.backgroundOpacity)
                currentlyEditingLabel.labelTextView.backgroundOpacity = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVResizableTextView.backgroundOpacity), object: currentlyEditingLabel.labelTextView.backgroundOpacity)
                return currentlyEditingLabel.labelTextView.backgroundOpacity!
            }
            return 1.0
        }
    }
    
    public var textAlignment: NSTextAlignment! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.alignment = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.alignment
            }
            return NSTextAlignment.center
        }
    }
    
    @objc public func setTextSpacing(_ newValue: CGFloat) {

        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setTextSpacing(currentlyEditingLabel.labelTextView.textSpacing)
            self.undoer.setActionName("textSpacing")
            currentlyEditingLabel.labelTextView.textSpacing = newValue
            adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        }
    }
    
    public func textSpacing() -> CGFloat {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.textSpacing
        }
        return kTextSpacingDefault
    }
    
    public var lineSpacing: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.lineSpacing = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.lineSpacing
            }
            return kLineSpacingDefault
        }
    }
    
    public var textBrightness: CGFloat {
        
        //TODO: ideally brightness value should be added as NSAttributedStringKey in BVTextView instead of storing it in BVLabelView
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.brightnessValue = newValue
                currentlyEditingLabel.labelTextView.brightness = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
        
//                return currentlyEditingLabel.labelTextView.brightness
                return currentlyEditingLabel.brightnessValue
            }
            return 0
            
        }
    }
    
    //TODO: ideally saturation value should be added as NSAttributedStringKey in BVTextView instead of storing it in BVLabelView
    public var textSaturation: CGFloat {
        
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.saturationValue = newValue
                currentlyEditingLabel.labelTextView.saturation = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
//                return currentlyEditingLabel.labelTextView.saturation
                return currentlyEditingLabel.saturationValue
            }
            return 0
        }
    }
    
    public var textShadowOffset: CGSize! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowOffset = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.shadow?.shadowOffset
            }
            return CGSize.zero
        }
    }
    
    public var textShadowColor: UIColor! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowColor = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return (currentlyEditingLabel.labelTextView.shadow?.shadowColor) as? UIColor
            }
            return UIColor.clear
        }
    }
    
    public var textShadowBlur: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowBlur = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.shadow?.shadowBlurRadius
            }
            return 0.0
        }
    }
}

extension BVResizableTextView {
    public func showEditingHandlke() {
        if currentlyEditingLabel != nil {
            currentlyEditingLabel.showEditingHandles()
        }
    }
}

extension BVResizableTextView {
    
    public func beginUndoGrouping() {
        if beginUndoGroupingFlag != kAllowBeginUndoGrouping {
            beginUndoGroupingFlag = kAllowBeginUndoGrouping
            print("beginUndoGrouping# is called \n")
            self.undoer.beginUndoGrouping()
        }
    }
    
    public func endUndoGrouping() { //sender: Any) {
        
        if beginUndoGroupingFlag == kAllowBeginUndoGrouping {
            print("endUndoGrouping##### is called\n")
            self.undoer.endUndoGrouping()
            beginUndoGroupingFlag = !kAllowBeginUndoGrouping
        }
        if currentlyEditingLabel != nil {
            currentlyEditingLabel.becomeFirstResponder()
        }
    }

    func undo()  {
        self.undoer.undo()
    }

    func redo()  {
        self.undoer.redo()
    }
}









