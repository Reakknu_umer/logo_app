//
//  BVCanvas+SVG.swift
//  DemoBVCanvas
//
//  Created by Md Shafi Ahmed on 14/08/18.
//  Copyright © 2018 Md Shafi Ahmed. All rights reserved.
//

import UIKit

fileprivate let kGlobalInset: CGFloat = 18.0;
fileprivate let kBorderWidthDefault: CGFloat = 3
fileprivate let kBorderColorDefault = UIColor.red
fileprivate let kSVGColorDefault = UIColor.lightGray
fileprivate let kMinimumFrameWidth: CGFloat = 25;
fileprivate let kMinimumFrameHeight: CGFloat = 25;

//@objc public protocol BVCanvasDelegateForSvg: NSObjectProtocol {
//    //-- Occurs when a touch gesture event occurs on close button.
//    @objc optional func svgViewDidClose(_ label: BVLabelView) -> Void
//
//    //-- Occurs when border and control buttons was shown.
//    @objc optional func labelViewDidShowEditingHandles(_ label: BVLabelView) -> Void
//
//    //-- Occurs when border and control buttons was hidden.
//    @objc optional func labelViewDidHideEditingHandles(_ label: BVLabelView) -> Void
//
//    //-- Occurs when label become first responder.
//    @objc optional func labelViewDidBeginEditing(_ label: BVLabelView) -> Void
//
//    //-- Occurs when label lose being first responder.
//    @objc optional func labelViewDidEndEditing(_ label: BVLabelView) -> Void
//
//    //-- Occurs when label starts move or rotate.
//    @objc optional func labelViewDidBeginMoving(_ label: BVLabelView) -> Void
//
//    //--Occurs when label continues move or rotate.
//    @objc optional func labelViewDidMoving(_ label: BVLabelView) -> Void
//
//    //-- Occurs when label ends move or rotate.
//    @objc optional func labelViewDidSelected(_ label: BVLabelView) -> Void
//}

extension BVCanvas {
    
//    private lazy var tapOutsideGestureRecognizer: UITapGestureRecognizer! = {
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BVCanvas.tapOutside))
//        tapGesture.delegate = self
//        return tapGesture
//
//    }()
    
    //-- MARK: Add BVSvg required a svg file name
    public func addSvgView(_ named: String) {
        
        if let svgView: BVSvgView = currentlyEditingSvg {
            svgView.hideEditingHandles()
        }
        guard named.count > 0 else {
            return
        }
        let randomPoint:CGPoint = self.randomPontOnScreenWith(baseVale: kRnadomBaseValueForLabelPosition)
        let frame = CGRect(x: bounds.midX - randomPoint.x, y: bounds.midY - randomPoint.y,
                           width: kSvgSizeDefault.width, height: kSvgSizeDefault.height)
        
        let newSvgView: BVSvgView = BVSvgView.init(fileName: named, frame: frame)
        newSvgView.delegate = self
        newSvgView.borderColor = UIColor.red
        self.addSubview(newSvgView)
        currentlyEditingSvg = newSvgView
        self.addSvgToTheStack(newSvgView)
        addGestureRecognizer(tapOutsideGestureRecognizer) //-- now svgView will lose its focus if tapped outside the labelView
    }
    
    public func addSvgView(_ named: String, andFrame frame : CGRect) {
        
        if let svgView: BVSvgView = currentlyEditingSvg {
            svgView.hideEditingHandles()
        }
        guard named.count > 0 else {
            return
        }
        let newSvgView: BVSvgView = BVSvgView.init(fileName: named, frame: frame)
        newSvgView.delegate = self
        newSvgView.borderColor = UIColor.red
        self.addSubview(newSvgView)
        currentlyEditingSvg = newSvgView
        self.addSvgToTheStack(newSvgView)
        addGestureRecognizer(tapOutsideGestureRecognizer) //-- now svgView will lose its focus if tapped outside the labelView
    }
    
    
    
    private func addSvgToTheStack(_ newSvgView: BVSvgView) {
        if self.undoer.isUndoing {
            removeSvgFromTheStack(newSvgView)
        }
//        (self.undoer.prepare(withInvocationTarget: self) as AnyObject).addNewLabel(newSvg)
        self.undoer.setActionName("addLabel")
        svgStack.add(newSvgView)
    }
    
    func removeSvgFromTheStack(_ svgView: BVSvgView) {
        svgView.removeFromSuperview()
        svgStack.remove(svgView)
        print("svg in the stack are: \(labelStack.count)")
    }
    
//    public func renderTextOnView(_ view: UIView) -> UIImage? {
//
//        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0)
//
//        view.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let img = UIGraphicsGetImageFromCurrentImageContext()
//
//        UIGraphicsEndImageContext()
//        return img
//    }
    
//    public func limitImageViewToSuperView() {
//        if superview == nil {
//            return
//        }
//        self.translatesAutoresizingMaskIntoConstraints = true
//        let imageSize = self.image?.size
//        let aspectRatio = imageSize!.width / imageSize!.height
//
//        if let width = imageSize?.width, let height = imageSize?.height {
//            if width > height {
//                bounds.size.width = superview!.bounds.size.width
//                bounds.size.height = superview!.bounds.size.width / aspectRatio
//            }else {
//                bounds.size.height = superview!.bounds.size.height
//                bounds.size.width = superview!.bounds.size.height * aspectRatio
//            }
//        }
//    }
}

extension BVCanvas {
    
//    public enum textShadowPropterties {
//        case offSet(CGSize)
//        case color(UIColor)
//        case blurRadius(CGFloat)
//    }
//
//    public func setFontName(_ name: String) {
//        if currentlyEditingSvg != nil {
//            (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setFontName(currentlyEditingSvg.labelTextView.fontName)
//            self.undoer.setActionName("setFontName")
//            currentlyEditingSvg.labelTextView.fontName = name
//            adjustsWidthToFillItsContens(currentlyEditingSvg, labelView: currentlyEditingSvg.labelTextView)
//        }
//    }
//
//    public func fontName()-> String {
//        if currentlyEditingSvg != nil {
//            return currentlyEditingSvg.labelTextView.fontName
//        }
//        return kFontNameDefault
//    }
//
//    public var fontSize: CGFloat! {
//        set {
//            if currentlyEditingSvg != nil {
//                currentlyEditingSvg.labelTextView.fontSize = newValue
//                adjustsWidthToFillItsContens(currentlyEditingSvg, labelView: currentlyEditingSvg.labelTextView)
//
//            }
//        }
//        get {
//            if currentlyEditingSvg != nil {
//                return currentlyEditingSvg.labelTextView.fontSize
//            }
//            return kFontSizeDefault
//        }
//    }
    
    /**
     It calculates and returns the outcome of the division of the two parameters.
     
     ## Important Notes ##
     1. Both parameters are **double** numbers.
     2. For a proper result the second parameter *must be other than 0*.
     3. If the second parameter is 0 then the function will return nil.
     */
    
//    @objc public var svgFillColor: UIColor! {
//        set {
//            if currentlyEditingSvg != nil {
//                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.svgFillColor),
//                                          object: currentlyEditingSvg.svgView?.penColor())
//
//                currentlyEditingSvg.brightColor = newValue
//
//                if (currentlyEditingSvg.brightnessValue > 0.0) {
//                    currentlyEditingSvg.svgBrightness = currentlyEditingSvg.brightnessValue
//                }
//                else {
//                    currentlyEditingSvg.svgView?.setPenColor(newValue)
//                }
//            }
//        }
//        get {
//            if currentlyEditingSvg != nil {
//                return currentlyEditingSvg.brightColor
//            }
//            return kBorderColorDefault
//        }
//    }
    @objc public func setSVGRotationAngle(_ newValue: CGFloat) {
        
        if currentlyEditingSvg != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setSVGRotationAngle(currentlyEditingSvg.rotationAngle)
            self.undoer.setActionName("svgRotationAngle")
            currentlyEditingSvg.transform = CGAffineTransform(rotationAngle: -newValue)
            currentlyEditingSvg.rotationAngle = newValue
        }
    }
    
    func svgRotationAngle() ->CGFloat {
        if currentlyEditingSvg != nil {
            return currentlyEditingSvg.rotationAngle
        }
        return 1.0
    }
    
    @objc public var svgFillColor: UIColor! {
        set {
            if currentlyEditingSvg != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.svgFillColor),
                                          object: currentlyEditingSvg.svgView?.penColor())
                currentlyEditingSvg.fillColor = newValue
                currentlyEditingSvg.svgFillColor = newValue
            }
        }
        
        get {
            if currentlyEditingSvg != nil {
                return currentlyEditingSvg.svgFillColor
            }
            return UIColor.clear
        }
    }
    
    
    
    internal var svgBackgroundColor: UIColor! {
        set {
            if currentlyEditingSvg != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.svgBackgroundColor),
                                          object: currentlyEditingSvg.svgView?.backgroundColor)
                currentlyEditingSvg.svgView?.backgroundColor = newValue
            }
        }
        
        get {
            if currentlyEditingSvg != nil {
                return currentlyEditingSvg.svgView?.backgroundColor
            }
            return UIColor.clear
        }
    }
    
    @objc public func setSvgOpacity(_ newValue: CGFloat) {
        
        if currentlyEditingSvg != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setSvgOpacity(CGFloat((currentlyEditingSvg.svgView?.alpha)!))
            self.undoer.setActionName("textOpacity")
            currentlyEditingSvg.svgView?.alpha = newValue
        }
    }
    
    func svgOpacity() -> CGFloat {
        if currentlyEditingSvg != nil {
            return CGFloat((currentlyEditingSvg.svgView?.alpha)!)
        }
        return 1.0
    }
    
    @objc public var svgBackgroundOpacity: CGFloat {
        set {
            if currentlyEditingSvg != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.svgBackgroundOpacity),
                                          object: currentlyEditingSvg.alpha)
                currentlyEditingSvg.alpha = newValue
            }
        }
        get {
            if currentlyEditingSvg != nil {
                return currentlyEditingSvg.alpha
            }
            return 1.0
        }
    }
    
   @objc public func setSvgBrightness(_ newValue: CGFloat) {
        if currentlyEditingSvg != nil {
            (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setSvgBrightness(currentlyEditingSvg.svgBrightness)
            currentlyEditingSvg.brightnessValue = newValue
            currentlyEditingSvg.svgBrightness = newValue
        }
    }
    
    public func svgBrightness() -> CGFloat {
        if currentlyEditingSvg != nil {
            return currentlyEditingSvg.brightnessValue
        }
        return 0.0
    }
    
//    public var svgBrightness: CGFloat {
//        set {
//            if currentlyEditingSvg != nil {
//                //                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.svgBrightness), object: currentlyEditingSvg.svgBrightness)
//                (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setSVG (currentlyEditingLabel.labelTextView.fontName)
//                currentlyEditingSvg.brightnessValue = newValue
//                currentlyEditingSvg.svgBrightness = newValue
//            }
//        }
//        get {
//            if currentlyEditingSvg != nil {
//                return currentlyEditingSvg.brightnessValue
//            }
//            return 0.0
//            
//        }
//    }
    
    @objc public func setSvgSaturation (_ newValue: CGFloat) {
        if currentlyEditingSvg != nil {
            (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setSvgSaturation(currentlyEditingSvg.svgSaturation)
            currentlyEditingSvg.saturationValue = newValue
            currentlyEditingSvg.svgSaturation = newValue
        }
    }
    
    public func svgSaturation() -> CGFloat {
            if currentlyEditingSvg != nil {
                return currentlyEditingSvg.saturationValue
            }
            return 0.0
    }
  
//    public var svgSaturation: CGFloat {
//
//        set {
//            if currentlyEditingSvg != nil {
//                currentlyEditingSvg.saturationValue = newValue
//                currentlyEditingSvg.svgSaturation = newValue
//            }
//        }
//        get {
//            if currentlyEditingSvg != nil {
//                return currentlyEditingSvg.saturationValue
//            }
//            return 0.0
//        }
//    }
    
}
