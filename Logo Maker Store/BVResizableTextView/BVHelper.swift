//
//  BVHelper.swift
//  
//
//  Created by Shafi Ahmed on 24/07/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

//private let kFontNameDefault:String = "HelveticaNeue"

protocol adjustFontSizeToFillRectProtocol {
    
    func adjustFontSizeToFillRect(_ newBounds: CGRect, view: BVLabelView, labelView: BVTextView) -> Void
    func adjustsWidthToFillItsContens(_ view: BVLabelView, labelView: BVTextView) -> Void
    func calculateBoudsToFillItsContens(_ view: BVLabelView, labelView: BVTextView) -> CGRect
}

extension adjustFontSizeToFillRectProtocol {
    func adjustFontSizeToFillRect(_ newBounds: CGRect, view: BVLabelView, labelView: BVTextView) {
        var mid: CGFloat = 0.0
        var stickerMaximumFontSize: CGFloat = 200.0
        var stickerMinimumFontSize: CGFloat = 15.0
//        let offset = newBounds.size.width - labelView.frame.size.width
        let bounds: CGRect = newBounds// CGRect(x: newBounds.origin.x, y: newBounds.origin.y,
                                    //width: newBounds.size.width - offset, height: newBounds.size.height - offset)
        var difference: CGFloat = 0.0
        
        var tempFont = UIFont(name: view.labelTextView.fontName, size: view.labelTextView.fontSize)
        var copyTextAttributes = labelView.attributes
        copyTextAttributes[NSAttributedStringKey.font] = tempFont
        var attributedText = NSAttributedString(string: view.labelTextView.text, attributes: copyTextAttributes)
        
        while stickerMinimumFontSize <= stickerMaximumFontSize {
            mid =  stickerMinimumFontSize + (stickerMaximumFontSize - stickerMinimumFontSize) / 2
            tempFont? = UIFont(name: view.labelTextView.fontName, size: CGFloat(mid))!
            if (tempFont == nil) {
                print("\n Invalid Font Name! Set to default font: \(kFontNameDefault)")
                view.labelTextView.fontName = kFontNameDefault
                tempFont? = UIFont(name: view.labelTextView.fontName, size: CGFloat(mid))!
            }
            copyTextAttributes[NSAttributedStringKey.font] = tempFont
            attributedText = NSAttributedString(string: view.labelTextView.text, attributes: copyTextAttributes)
            
            difference = bounds.height - attributedText.boundingRect(with: CGSize(width: bounds.width - 24,
                                                                                  height: CGFloat.greatestFiniteMagnitude),
                                                                     options:[.usesLineFragmentOrigin, .usesFontLeading], context: nil).height
            
            if (mid == stickerMinimumFontSize || mid == stickerMaximumFontSize) {
                if (difference < 0) {
                    view.labelTextView.fontSize = mid - 1
                    return
                }
                
                view.labelTextView.fontSize = mid
                return
            }
            
            if (difference < 0) {
                stickerMaximumFontSize = mid - 1
            }else if (difference > 0) {
                stickerMinimumFontSize = mid + 1
            }else {
                view.labelTextView.fontSize = mid
                return
            }
        }
        
        view.labelTextView.fontSize = mid
        return
    }
    
//    func adjustsWidthToFillItsContens(_ view: BVLabelView, labelView: BVTextView) {
//        let attributedText = labelView.attributedText
//
//        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//
//        let w1 = (ceilf(Float((recSize?.size.width)!)) + 24 < 50) ? view.labelTextView.bounds.size.width : CGFloat(ceilf(Float((recSize?.size.width)!)) + 24)
//        let h1 = (ceilf(Float((recSize?.size.height)!)) + 24 < 50) ? 50 : CGFloat(ceilf(Float((recSize?.size.height)!)) + 24)
//
//        var viewFrame = view.bounds
//        viewFrame.size.width = w1 + 24
//        viewFrame.size.height = h1 + 18
//        view.bounds = viewFrame
//
////        print("Text Frame x: \(recSize?.size.width)");
//    }
    
    func adjustsWidthToFillItsContens(_ view: BVLabelView, labelView: BVTextView) {
        let attributedText = labelView.attributedText
        
        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        var w1: CGFloat = 0.0
        var h1: CGFloat = 0.0
            
        if ceilf(Float((recSize?.size.width)!)) + 24 < 50 {
            w1 = view.labelTextView.bounds.size.width
        }
        else {
            w1 = CGFloat(ceilf(Float((recSize?.size.width)!)) + 24)
        }
        
        if ceilf(Float((recSize?.size.height)!)) + 24 < 50 {
            h1 = 50
        }
        else {
            h1 = CGFloat(ceilf(Float((recSize?.size.height)!)) + 24)
        }
        
        var viewFrame = view.bounds
        viewFrame.size.width = w1 + 24
        viewFrame.size.height = h1 + 18
        view.bounds = viewFrame
        
//        print("Text Frame x: \(recSize?.size.width)");
    }
    
    func calculateBoudsToFillItsContens(_ view: BVLabelView, labelView: BVTextView) -> CGRect {
        let attributedText = labelView.attributedText

        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        
        var w1: CGFloat = 0.0
        var h1: CGFloat = 0.0
        
        if ceilf(Float((recSize?.size.width)!)) + 24 < 50 {
            w1 = view.labelTextView.bounds.size.width
        }
        else {
            w1 = CGFloat(ceilf(Float((recSize?.size.width)!)) + 24)
        }
        
        if ceilf(Float((recSize?.size.height)!)) + 24 < 50 {
            h1 = 50
        }
        else {
            h1 = CGFloat(ceilf(Float((recSize?.size.height)!)) + 24)
        }
        
        var viewFrame = view.bounds
        viewFrame.size.width = w1 + 24
        viewFrame.size.height = h1 + 18
//        view.bounds = viewFrame

        return viewFrame
    }
}







