//
//  BVTextView.swift
// 
//
//  Created by Shafi Ahmed on 19/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit

private let kFontAlphaDefault:CGFloat = 1.0
public let kFontSizeDefault:CGFloat = 20
public let kLineSpacingDefault:CGFloat = 20
public let kTextSpacingDefault:CGFloat = 20
public let kFontNameDefault:String = "HelveticaNeue"
public let kParagraphStyleDefault = NSMutableParagraphStyle()


@objcMembers public class BVTextView: UITextView {
    
    var _brightnessValue:CGFloat = 0
    var _saturationValue:CGFloat = 0
//    let attributedString = NSMutableAttributedString(string: "TRUETRUE")

    
    public private(set) var attributes: [NSAttributedStringKey: AnyObject] = [:] //-- array to store arrtribute
    
    //TODO: ideally these 2 properties should be added as NSAttributedStringKey in BVTextView instead of storing it in BVLabelView
    public var brightnessValue: CGFloat {
        get {
            return self._brightnessValue
        }
        set {
            self._brightnessValue = newValue
        }
    }
    
    private var saturationValue: CGFloat {
        get {
            return self._saturationValue
        }
        set {
            self._saturationValue = newValue
        }
    }
    //END TODO
    
    //-- to avoid conflict with UITextView font property, give this property a different name
    public var fontName: String = kFontNameDefault {
        didSet {
            let fontFaceName = UIFont(name: fontName, size: fontSize)
            attributes[NSAttributedStringKey.font] = fontFaceName
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
            
            self.font = fontFaceName
        }
    }
    
    public var fontSize: CGFloat = kFontSizeDefault {
        didSet {
            let fontFaceName = UIFont(name: fontName, size: fontSize)
            attributes[NSAttributedStringKey.font] = fontFaceName
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
            
            self.font = fontFaceName
        }
    }
    
    public var foregroundColor: UIColor? {
        didSet {
            attributes[NSAttributedStringKey.foregroundColor] = foregroundColor
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    //-- to avoid conflict with UITextView backgroundColor property, give this property a different name, backgroundColour (with 'u').
    public var backgroundColour: UIColor? {
        didSet {
            self.layer.backgroundColor = backgroundColour?.cgColor
        }
    }
    
    public var textOpacity: CGFloat = kFontAlphaDefault { //-- alpha value for the font/text
        didSet {
            attributes[NSAttributedStringKey.foregroundColor] = foregroundColor?.withAlphaComponent(textOpacity)
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var brightness: CGFloat {
        get {
            let lightness:CGFloat = self.brightnessValue
            return lightness
        }
        set {
            self.brightnessValue = newValue
            let previousColor: UIColor = self.foregroundColor!
            let colorHsl = previousColor.lighten(newValue)
            attributes[NSAttributedStringKey.foregroundColor] = colorHsl
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
            
        }
    }
    
    public var saturation: CGFloat {
        get {
            return self.saturationValue
        }
        set {
            var h:CGFloat = 0
            var s:CGFloat = 0
            var l:CGFloat = 0
            var a:CGFloat = 0
            self.saturationValue = newValue
            let minusOneValue = (newValue*2) + (-1)
            
            let previousColor: UIColor = self.foregroundColor!
            previousColor.getHue(&h, saturation:&s, brightness:&l, alpha:&a)
            let colorHsl = previousColor.offset(withHue:1, saturation: minusOneValue, lightness:self.brightnessValue, alpha:a)
            attributes[NSAttributedStringKey.foregroundColor] = colorHsl
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
            
        }
    }
    
    public var backgroundOpacity: CGFloat? {
        didSet {
            layer.backgroundColor = backgroundColour?.withAlphaComponent(backgroundOpacity!).cgColor
        }
    }
    
    public var alignment: NSTextAlignment {
        get {
            return paragraphStyle.alignment
        }
        set {
            paragraphStyle.alignment = newValue
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
            
        }
    }
    
    public var paragraphStyle: NSMutableParagraphStyle = kParagraphStyleDefault {
        didSet {
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
        }
    }
    
    public var paragraphSpacing: CGFloat {
        get {
            return paragraphStyle.paragraphSpacing
        }
        
        set {
            paragraphStyle.paragraphSpacing = newValue
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var textSpacing: CGFloat {
        get {
            let kern: Any? = attributes[NSAttributedStringKey.kern]
            if (kern == nil) {
                return 0;
            }
            return attributes[NSAttributedStringKey.kern] as! CGFloat
//            return CGFloat(truncating: attributes[NSAttributedStringKey.kern] as! NSNumber)
        }
         set {
            attributes[NSAttributedStringKey.kern] = NSNumber(value: Float(newValue))
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var kern: CGFloat = 0.0 {
        didSet {
            attributes[NSAttributedStringKey.kern] = NSNumber(value: Float(kern))
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var lineSpacing: CGFloat {
        get {
            return paragraphStyle.lineSpacing
        }
        
        set {
            paragraphStyle.lineSpacing = newValue
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
#if os(iOS) || os(tvOS)

    public var shadow: NSShadow? = NSShadow() {
        didSet {
            attributes[NSAttributedStringKey.shadow] = shadow
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            self.attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var textShadowOffset: CGSize! {
        didSet {
            shadow?.shadowOffset = textShadowOffset
            attributes[NSAttributedStringKey.shadow] = shadow
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var textShadowColor: UIColor! {
        didSet {
            shadow?.shadowColor = textShadowColor
            attributes[NSAttributedStringKey.shadow] = shadow
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
    public var textShadowBlur: CGFloat! {
        didSet {
            shadow?.shadowBlurRadius = textShadowBlur
            attributes[NSAttributedStringKey.shadow] = shadow
            attributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            attributedText = NSAttributedString(string: self.text, attributes: attributes)
        }
    }
    
#endif
}
    
extension BVTextView {
        override public func caretRect(for position: UITextPosition) -> CGRect {
            var rect = super.caretRect(for: position)
            rect.size.height = self.font!.pointSize - self.font!.descender
            
            return rect
        }
}

extension NSAttributedStringKey { //-- custom attributes
//    static let saturation = NSAttributedStringKey("saturation")
//    static var brightness = NSAttributedStringKey("br")

}




