//
//  BVLabelView.swift
//  
//
//  Created by Shafi Ahmed on 19/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit

fileprivate let kGlobalInset: CGFloat = 18.0;
fileprivate let kHandleRadius: CGFloat = 18.0;
fileprivate let kBorderWidthDefault: CGFloat = 3
fileprivate let kBorderColorDefault = UIColor.red
fileprivate let kMinimumFrameWidth: CGFloat = 90;
fileprivate let kMinimumFrameHeight: CGFloat = 60;
fileprivate let kMinimumFontSize: CGFloat = 5;


//-- MARK: Delegate
@objc public protocol BVLabelViewDelegate: NSObjectProtocol {
    //-- Occurs when a touch gesture event occurs on close button.
    @objc optional func labelViewDidClose(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was shown.
    @objc optional func labelViewDidShowEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was hidden.
    @objc optional func labelViewDidHideEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when label become first responder.
    @objc optional func labelViewDidStartEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label lose being first responder.
    @objc optional func labelViewDidEndEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label starts move or rotate.
    @objc optional func labelViewDidBeginEditing(_ label: BVLabelView) -> Void
    
    //--Occurs when label continues move or rotate.
    @objc optional func labelViewDidChangeEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label ends move or rotate.
    @objc optional func labelViewDidSelected(_ label: BVLabelView) -> Void
}

//-- MARK: Gesture Recognizer
@objcMembers public class BVLabelView: UIView {

    fileprivate lazy var singleTapShowHide: UITapGestureRecognizer! = {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(BVLabelView.contentTapped(_:)))
        tapRecognizer.delegate = self
        return tapRecognizer
    }()
    private lazy var moveGestureRecognizer: UIPanGestureRecognizer! = {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BVLabelView.moveGesture(_:)))
        panRecognizer.delegate = self
        return panRecognizer
    }()
    
    private lazy var closeTap: UITapGestureRecognizer! = {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: (#selector(BVLabelView.closeTap(_:))))
        tapRecognizer.delegate = self
        return tapRecognizer
    }()
    
    private lazy var panRotateGesture: UIPanGestureRecognizer! = {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BVLabelView.rotateViewPanGesture(_:)))
        panRecognizer.delegate = self
        return panRecognizer
    }()
    
    //--MARK: Properties
    fileprivate var lastTouchedView: BVLabelView?
    var delegate: BVLabelViewDelegate?

    fileprivate var initialBounds: CGRect?
    fileprivate var initialDistance: CGFloat?
    fileprivate var beginningPoint: CGPoint?
    fileprivate var beginningCenter: CGPoint?
    fileprivate var touchLocation: CGPoint?
    fileprivate var deltaAngle: CGFloat?
    fileprivate var beginBounds: CGRect?
    
    public var border: CAShapeLayer?
    public var labelTextView: BVTextView!
    public var rotateView: UIImageView?
    public var closeView: UIImageView?
//    fileprivate var backgroundImageView : UIImageView?
    
    fileprivate var isShowingEditingHandles = true
    
    //TODO: ideally these two properties should be added as NSAttributedStringKey in BVTextView
    var brightnessValue: CGFloat = -1.0
    var saturationValue: CGFloat = -1.0
    private var currentRotationAngle: CGFloat = 0.00
    private var currentLabelBounds: CGRect = CGRect.zero
    private var currentScaleRect: CGRect = CGRect.zero
    
    //--MARK: Initialization
    init() {
        super.init(frame: CGRect.zero)
        setup()
        adjustsWidthToFillItsContens(self, labelView: labelTextView)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if frame.size.width < kMinimumFrameWidth {
            bounds.size.width = kMinimumFrameWidth
        }
        
        if frame.size.height < kMinimumFrameHeight {
            bounds.size.height = kMinimumFrameHeight
        }
        
        setup()
        adjustsWidthToFillItsContens(self, labelView: labelTextView)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
        adjustsWidthToFillItsContens(self, labelView: labelTextView)
    }
    
    //MARK: Set Control Buttons
    
    public var enableClose: Bool = true {
        didSet {
            closeView?.isHidden = enableClose
            closeView?.isUserInteractionEnabled = enableClose
        }
    }
    
    public var enableRotate: Bool = true {
        didSet {
            rotateView?.isHidden = enableRotate
            rotateView?.isUserInteractionEnabled = enableRotate
        }
    }
    
    public var rotationAngle: CGFloat {
        set {
            self.currentRotationAngle = newValue
//            self.rotateLabelWith(angle: rotationAngle)
        }
        get {
            return self.currentRotationAngle
        }
    }
    public var scaleRect: CGRect {
        set {
            self.currentScaleRect = newValue
        }
        get {
            return self.currentScaleRect
        }
    }
    
    public var currentBounds: CGRect {
        set {
            self.currentLabelBounds = newValue
        }
        get {
            return self.currentLabelBounds
        }
    }
    
    public func adjustBoundsAndFontSize() {
        adjustFontSizeToFillRect(scaleRect, view: self, labelView: labelTextView)
        bounds = scaleRect
        self.currentBounds = calculateBoudsToFillItsContens(self, labelView: labelTextView)
        self.bounds = self.currentBounds
        refresh()
    }
    
    public var enableMoveRestriction: Bool = true {
        didSet {
            
        }
    }
    
    //-- MARK: Border, Color, Shadow
    public var borderColor: UIColor? {
        didSet {
            border?.strokeColor = borderColor?.cgColor
        }
    }
    
//    public var backgroundImage: UIImage? {
//        didSet {
//            backgroundImageView = UIImageView(frame: bounds)
//            backgroundImageView?.bounds = bounds
//            backgroundImageView?.image = backgroundImage
//            //            backgroundImageView?.contentMode = .scaleAspectFit
//            backgroundImageView?.clipsToBounds = true
//            backgroundColor = UIColor.clear
//            if let view = backgroundImageView {
//                addSubview(view)
//                sendSubview(toBack: view)
//            }
//        }
//    }
    
    public var showsContentShadow: Bool = false {
        didSet {
            if showsContentShadow {
                layer.shadowColor = UIColor.black.cgColor
                layer.shadowOffset = CGSize(width: 0, height: 5)
                layer.shadowOpacity = 1.0
                layer.shadowRadius = 4.0
            }else {
//                layer.shadowColor = UIColor.clear.cgColor
                layer.shadowOffset = CGSize.zero
                layer.shadowOpacity = 0.0
                layer.shadowRadius = 0.0
            }
        }
    }
    
    override public func didMoveToSuperview() {
        super.didMoveToSuperview()
        if self.superview != nil {
            showEditingHandles()
            refresh()
        }
    }
    
    public override func layoutSubviews() {
        if ((labelTextView) != nil) {
            border?.path = UIBezierPath(rect: labelTextView.bounds).cgPath
            border?.frame = labelTextView.bounds
        }
    }
    
    func setup() {
        backgroundColor = UIColor.clear
        autoresizingMask = [.flexibleHeight, .flexibleWidth]
        border?.strokeColor = UIColor(red: 33, green: 45, blue: 59, alpha: 1).cgColor
        
        setupLabelTextView()
        setupBorder()
        
        insertSubview(labelTextView!, at: 0)
        //        self.backgroundColor = UIColor.yellow
        setupCloseAndRotateView()
        addGestureRecognizer(moveGestureRecognizer)
        addGestureRecognizer(singleTapShowHide)
        moveGestureRecognizer.require(toFail: closeTap)
        
        closeView!.addGestureRecognizer(closeTap)
        rotateView!.addGestureRecognizer(panRotateGesture)
        
        enableMoveRestriction = false
        enableClose = true
        enableRotate = true
        showsContentShadow = true
        
        showEditingHandles()
        labelTextView?.becomeFirstResponder()
//        self.backgroundColor = UIColor.lightGray
//        self.labelTextView.backgroundColor = UIColor.green
    }
}

extension BVLabelView: UITextViewDelegate {
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (isShowingEditingHandles) {
            return true
        }
        return false
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if let delegate: BVLabelViewDelegate = delegate {
            if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidStartEditing(_:))) {
                delegate.labelViewDidStartEditing!(self)
            }
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if let delegate: BVLabelViewDelegate = delegate {
            if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidEndEditing(_:))) {
                delegate.labelViewDidEndEditing!(self)
                
                if (isShowingEditingHandles) {
                    hideEditingHandles()
                }
            }
        }
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (!isShowingEditingHandles) {
            showEditingHandles()
        }
        //if textView.text != "" {
        //adjustsWidthToFillItsContens(self, labelView: labelTextView)
        //}
        
        return true
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        if textView.text != "" {
            adjustsWidthToFillItsContens(self, labelView: labelTextView)
            labelTextView.attributedText = NSAttributedString(string: labelTextView.text, attributes: labelTextView.attributes as [NSAttributedStringKey : Any])
            
        }
    }
}

extension BVLabelView: UIGestureRecognizerDelegate, adjustFontSizeToFillRectProtocol {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == singleTapShowHide {
            return true
        }
        return false
    }
    
    
     func contentTapped(_ recognizer: UITapGestureRecognizer) {
        if !isShowingEditingHandles {
            self.showEditingHandles()
            
            if let delegate: BVLabelViewDelegate = delegate {
                delegate.labelViewDidSelected!(self)
            }
        }
        
    }
    
     func closeTap(_ recognizer: UITapGestureRecognizer) {
//        self.removeFromSuperview()      //FIXME: ideally removing label from its super view is part of its super view not this labbel view
        
        if let delegate: BVLabelViewDelegate = delegate {
            if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidClose(_:))) {
                delegate.labelViewDidClose!(self)
            }
        }
    }
    
     func moveGesture(_ recognizer: UIPanGestureRecognizer) {
        if !isShowingEditingHandles {
            self.showEditingHandles()
            
            if let delegate: BVLabelViewDelegate = delegate {
                delegate.labelViewDidSelected!(self)
            }
        }
        
        touchLocation = recognizer.location(in: superview)
        
        switch recognizer.state {
        case .began:
            beginningPoint = touchLocation
            beginningCenter = center
            
            center = estimatedCenter()
            beginBounds = bounds
            
            if let delegate: BVLabelViewDelegate = delegate {
                delegate.labelViewDidBeginEditing!(self)
            }
            
        case .changed:
            center = estimatedCenter()
            
            
            if let delegate: BVLabelViewDelegate = delegate {
                delegate.labelViewDidChangeEditing!(self)
            }
            
        case .ended:
            center = estimatedCenter()
            
            
            if let delegate: BVLabelViewDelegate = delegate {
                delegate.labelViewDidEndEditing!(self)
            }
            
        default:break
            
        }
    }
    
    func rotateViewPanGesture(_ recognizer: UIPanGestureRecognizer) {
        touchLocation = recognizer.location(in: superview)
        let center = Utility.CGRectGetCenter(self.frame)
        
        switch recognizer.state {
        case .began:
            deltaAngle = atan2(touchLocation!.y - center.y, touchLocation!.x - center.x) - Utility.CGAffineTrasformGetAngle(transform)
            initialBounds = bounds
            initialDistance = Utility.CGpointGetDistance(center, point2: touchLocation!)
           
            if let delegate: BVLabelViewDelegate = delegate {
                if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidBeginEditing(_:))) {
                    delegate.labelViewDidBeginEditing!(self)
                }
            }
            
        case .changed:
            let ang = atan2(touchLocation!.y - center.y, touchLocation!.x - center.x)
            
            let angleDiff = deltaAngle! - ang
//            transform = CGAffineTransform(rotationAngle: -angleDiff)
            self.rotationAngle =  angleDiff
            layoutIfNeeded()
            
            let scale = sqrtf(Float(Utility.CGpointGetDistance(center, point2: touchLocation!)) / Float(initialDistance!))
            let scaleRect = Utility.CGRectScale(initialBounds!, wScale: CGFloat(scale), hScale: CGFloat(scale))
            self.scaleRect = scaleRect
            
            if scaleRect.size.width >= (1 + kGlobalInset * 2) && scaleRect.size.height >= (1 + kGlobalInset * 2) && self.labelTextView.text != "" {
//                print("\(labelTextView.fontSize)")
                if scale < 1 && labelTextView.fontSize <= kMinimumFontSize {
                    print("reached to minimum font size")
                }else {
//                    adjustFontSizeToFillRect(scaleRect, view: self, labelView: labelTextView)
//                    bounds = scaleRect
//                    self.currentBounds = calculateBoudsToFillItsContens(self, labelView: labelTextView)
//                    self.bounds = self.currentBounds
//                    refresh()
                    
                    self.adjustBoundsAndFontSize()
                }
            }
            
            if let delegate: BVLabelViewDelegate = delegate {
                if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidChangeEditing(_:))) {
                    delegate.labelViewDidChangeEditing!(self)
                }
            }
        case .ended:
            if let delegate: BVLabelViewDelegate = delegate {
                if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidEndEditing(_:))) {
                    delegate.labelViewDidEndEditing!(self)
                }
            }
            
            self.refresh()
            
        self.adjustsWidthToFillItsContens(self, labelView: labelTextView)
        default:break
            
        }
    }
}

//-- MARK: Editing handles and all
extension BVLabelView {
    
    func setupLabelTextView() {
        labelTextView = BVTextView(frame: self.bounds.insetBy(dx: kGlobalInset, dy: kGlobalInset))
        labelTextView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        labelTextView?.clipsToBounds = true
        labelTextView?.delegate = self
        labelTextView?.backgroundColor = UIColor.clear
        labelTextView?.isScrollEnabled = false
        labelTextView.isSelectable = true
        labelTextView.textContainerInset = UIEdgeInsetsMake(0, 0, 0, 0)
        labelTextView?.text = "Edit"
        labelTextView?.font = UIFont (name: "HelveticaNeue", size: kFontSizeDefault)
        
        labelTextView.foregroundColor = UIColor.black
        labelTextView.kern = 0.0
        //--change the cursor color
        labelTextView?.tintColor = UIColor.black
    }
    
    func setupBorder() {
        border = CAShapeLayer(layer: layer)
        border?.strokeColor = borderColor?.cgColor
        border?.fillColor = nil
//        border?.lineDashPattern = [10, 2]
        border?.lineWidth = 4

    }
    
    func createCircleImage (rect: CGRect, radius: CGFloat, backgroundColor:UIColor, borderColor:CGColor, borderWidth:CGFloat = kBorderWidthDefault) -> UIImageView {
        
        let circleImage = UIImageView (frame: rect)
        circleImage.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        circleImage.layer.borderColor = borderColor
        circleImage.layer.borderWidth = borderWidth
        circleImage.contentMode = .scaleAspectFill
        circleImage.clipsToBounds = true
        circleImage.backgroundColor = backgroundColor
        circleImage.layer.cornerRadius = kHandleRadius
        circleImage.alpha = 0.9
        
        return circleImage
    }
    
    func drawXOnImage (view: UIImageView, color:UIColor, borderWidth:CGFloat = kBorderWidthDefault) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        
        var maxWidth: CGFloat = view.frame.size.width
        var maxHeight: CGFloat = view.frame.size.height
        
        let inset: CGFloat = (maxWidth + maxHeight) * 0.13
        maxWidth = maxWidth - inset
        maxHeight = maxHeight - inset
        
        var startPoint: CGPoint = CGPoint.zero //view.frame.origin
        var endPoint = CGPoint(x: maxWidth, y: maxHeight)
        startPoint = CGPoint(x: startPoint.x + inset, y:startPoint.y + inset)
        
        let contextSize = CGSize(width: maxWidth, height: maxHeight)
        
        UIGraphicsBeginImageContextWithOptions(contextSize, false, 0)
        let path = UIBezierPath()
        path.lineWidth = borderWidth
        
        color.setStroke()
        //draw the path and make visible
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        path.stroke()
        
        startPoint = CGPoint(x: maxWidth, y: inset)
        endPoint = CGPoint(x: inset, y: maxHeight)
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        path.stroke()
        
        //create image from path and add to subview
        let image = UIGraphicsGetImageFromCurrentImageContext()
        let imageView = UIImageView(image: image)
        view.addSubview(imageView)
        UIGraphicsEndImageContext()
    }
    
    func setupCloseAndRotateView() {
        
        //-- add Close view
        let dia: CGFloat = kHandleRadius * 2
        var rect = CGRect(x: self.bounds.size.width - dia, y: self.bounds.origin.y, width: dia, height: dia)
        
        closeView = createCircleImage(rect: rect, radius: kHandleRadius, backgroundColor: UIColor.orange,
                                      borderColor: UIColor.red.cgColor, borderWidth: 1)
        
        closeView?.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        closeView?.isUserInteractionEnabled = true
        
        drawXOnImage(view: closeView!, color: UIColor.white)
        addSubview(closeView!)
        
        
        //-- add Rotate view
        rect = CGRect.zero
        rect = CGRect(x: self.bounds.size.width - dia, y: self.bounds.size.height - dia, width: dia, height: dia)
        
        rotateView = createCircleImage(rect: rect, radius: kHandleRadius, backgroundColor: UIColor.orange,
                                       borderColor: UIColor.white.cgColor, borderWidth: 6)

        rotateView?.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin]
        rotateView?.isUserInteractionEnabled = true
        addSubview(rotateView!)
    }
}

extension BVLabelView {
    
    fileprivate func refresh() {
        if let superView: UIView = superview {
            if let transform: CGAffineTransform = superView.transform {
                let scale = Utility.CGAffineTransformGetScale(transform)
                let t = CGAffineTransform(scaleX: scale.width, y: scale.height)
                closeView?.transform = t.inverted()
                rotateView?.transform = t.inverted()
                
                if (isShowingEditingHandles) {
                    if let border: CALayer = border {
                        labelTextView?.layer.addSublayer(border)
                    }
                }else {
                    border?.removeFromSuperlayer()
                }
            }
        }
    }
    
    public func hideEditingHandles() {
        lastTouchedView = nil
        
        isShowingEditingHandles = false
        
        if enableClose {
            closeView?.isHidden = true
        }
        if enableRotate {
            rotateView?.isHidden = true
        }
        
        labelTextView.resignFirstResponder()
        
        refresh()
        
        if let delegate : BVLabelViewDelegate = delegate {
            if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidHideEditingHandles(_:))) {
                delegate.labelViewDidHideEditingHandles!(self)
            }
        }
    }
    
    public var isEditingHandlerHidden: Bool {
        get {
            return !isShowingEditingHandles
        }
    }
    
    public func showEditingHandles() {
        lastTouchedView?.hideEditingHandles()
        
        isShowingEditingHandles = true
        
        lastTouchedView = self
        
        if enableClose {
            closeView?.isHidden = false
        }
        
        if enableRotate {
            rotateView?.isHidden = false
        }
        
        self.refresh()
        
        if let delegate: BVLabelViewDelegate = delegate {
            if delegate.responds(to: #selector(BVLabelViewDelegate.labelViewDidShowEditingHandles(_:))) {
                delegate.labelViewDidShowEditingHandles!(self)
            }
        }
    }
    
    fileprivate func estimatedCenter() -> CGPoint{
        let newCenter: CGPoint!
        var newCenterX = beginningCenter!.x + (touchLocation!.x - beginningPoint!.x)
        var newCenterY = beginningCenter!.y + (touchLocation!.y - beginningPoint!.y)
        
        if (enableMoveRestriction) {
            if (!(newCenterX - 0.5 * frame.width > 0 &&
                newCenterX + 0.5 * frame.width < superview!.bounds.width)) {
                newCenterX = center.x;
            }
            if (!(newCenterY - 0.5 * frame.height > 0 &&
                newCenterY + 0.5 * frame.height < superview!.bounds.height)) {
                newCenterY = center.y;
            }
            newCenter = CGPoint(x: newCenterX, y: newCenterY)
        }else {
            newCenter = CGPoint(x: newCenterX, y: newCenterY)
        }
        return newCenter
    }
}






