//
//  BVSvgView.swift
//  
//
//  Created by Md Shafi Ahmed on 14/08/18.
//  Copyright © 2018 Md Shafi Ahmed. All rights reserved.
//

import Foundation
import UIKit
import Macaw

//open struct Constants {
    fileprivate let kGlobalInset: CGFloat = 18.0;
    fileprivate let kHandleRadius: CGFloat = 18.0;
    fileprivate let kBorderWidthDefault: CGFloat = 3
    fileprivate let kBorderColorDefault = UIColor.red
    fileprivate let kSVGColorDefault = UIColor.lightGray
    fileprivate let kMinimumFrameWidth: CGFloat = 25;
    fileprivate let kMinimumFrameHeight: CGFloat = 25;
//}

//-- MARK: Delegate
@objc public protocol BVSvgViewDelegate: NSObjectProtocol {
    //-- Occurs when a touch gesture event occurs on close button.
    @objc optional func svgViewDidClose(_ label: BVSvgView) -> Void
    
    //-- Occurs when border and control buttons was shown.
    @objc optional func svgViewDidShowEditingHandles(_ label: BVSvgView) -> Void
    
    //-- Occurs when border and control buttons was hidden.
    @objc optional func svgViewDidHideEditingHandles(_ label: BVSvgView) -> Void
    
    //-- Occurs when label become first responder.
    @objc optional func svgViewDidStartEditing(_ label: BVSvgView) -> Void
    
    //-- Occurs when label lose being first responder.
    @objc optional func svgViewDidEndEditing(_ label: BVSvgView) -> Void
    
    //-- Occurs when label starts move or rotate.
    @objc optional func svgViewDidBeginEditing(_ label: BVSvgView) -> Void
    
    //--Occurs when label continues move or rotate.
    @objc optional func svgViewDidChangeEditing(_ label: BVSvgView) -> Void
    
    //-- Occurs when label ends move or rotate.
    @objc optional func svgViewDidSelected(_ label: BVSvgView) -> Void
}

//-- MARK: Gesture Recognizer
@objcMembers open class BVSvgView: UIView, UIGestureRecognizerDelegate {
    
    fileprivate lazy var singleTapShowHide: UITapGestureRecognizer! = {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(BVSvgView.contentTapped(_:)))
        tapRecognizer.delegate = self
        return tapRecognizer
    }()
    private lazy var moveGestureRecognizer: UIPanGestureRecognizer! = {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BVSvgView.moveGesture(_:)))
        panRecognizer.delegate = self
        return panRecognizer
    }()

    private lazy var closeTap: UITapGestureRecognizer! = {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: (#selector(BVSvgView.closeTap(_:))))
        tapRecognizer.delegate = self
        return tapRecognizer
    }()

    private lazy var panRotateGesture: UIPanGestureRecognizer! = {
        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BVSvgView.rotateViewPanGesture(_:)))
        panRecognizer.delegate = self
        return panRecognizer
    }()
    
    //--MARK: Properties
    fileprivate var lastTouchedView: BVSvgView?
    var delegate: BVSvgViewDelegate?
    
    fileprivate var initialBounds: CGRect?
    fileprivate var initialDistance: CGFloat?
    fileprivate var beginningPoint: CGPoint?
    fileprivate var beginningCenter: CGPoint?
    fileprivate var touchLocation: CGPoint?
    fileprivate var deltaAngle: CGFloat?
    fileprivate var beginBounds: CGRect?
    
    public var border: CAShapeLayer?
    public var svgView: SVGView?
    
    public var rotateView: UIImageView?
    public var closeView: UIImageView?
    fileprivate var isShowingEditingHandles = true
    
    //TODO: ideally these two properties should be added as NSAttributedStringKey in BVTextView
    internal var fillColor: UIColor = UIColor.lightGray
    internal var brightnessValue: CGFloat = -1.0
    internal var brightColor: UIColor = UIColor.clear
    var saturationValue: CGFloat = -1.0
    var saturatedColor: UIColor = UIColor.clear
    private var lastRotationAngle: CGFloat = 0.00

    //--MARK: Macaw Initialization
    public init(fileName: String, frame: CGRect, backgroundColor: UIColor = UIColor.clear) {
        super.init(frame: frame)
        guard fileName.count > 0 else {
            return
        }
        self.initialSetup(fileName: fileName, frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(frame: CGRect.zero)
//        self.initialSetup(fileName: "", backgroundColor: UIColor.clear)
    }
    
    //MARK: Set Control Buttons
    public var enableClose: Bool = true {
        didSet {
            closeView?.isHidden = enableClose
            closeView?.isUserInteractionEnabled = enableClose
        }
    }
    public var enableRotate: Bool = true {
        didSet {
            rotateView?.isHidden = enableRotate
            rotateView?.isUserInteractionEnabled = enableRotate
        }
    }
    
    public var rotationAngle: CGFloat {
        set {
            self.lastRotationAngle = newValue
        }
        get {
            return self.lastRotationAngle
        }
    }
    
    public var enableMoveRestriction: Bool = true {
        didSet {
            
        }
    }
    
    //-- MARK: Border, Color, Shadow
    public var borderColor: UIColor? {
        didSet {
            border?.strokeColor = borderColor?.cgColor
        }
    }
    
    private func calculateBrightness (color: UIColor, value: CGFloat) -> UIColor {
        return color.lighten(value)
    }
    
    private func calculateSatuartion (color: UIColor, value: CGFloat) -> UIColor {
        
        var h:CGFloat = 0
        var s:CGFloat = 0
        var l:CGFloat = 0
        var a:CGFloat = 0
        let minusOneValue = (value*2) + (-1)
        
        color.getHue(&h, saturation:&s, brightness:&l, alpha:&a)
        return color.offset(withHue:1, saturation: minusOneValue, lightness:0, alpha:a)
    }
    
    @objc public var svgFillColor: UIColor! {
        set {
            self.fillColor = newValue
            let currentColor: UIColor = newValue
            var newColor: UIColor = newValue
            
            if (self.saturationValue > 0.01) {
                newColor = self.calculateSatuartion(color: currentColor, value: self.saturationValue)
                self.saturatedColor = newColor
            }
            
            if (self.brightnessValue > 0.01) {
                newColor = currentColor.lighten(self.brightnessValue)
                self.brightColor = newColor
            }
            self.svgView?.setPenColor(newColor)
        }
        get {
            return self.fillColor
        }
    }
    
    public var svgBrightness: CGFloat {
        set {
            self.brightnessValue = newValue
            let currentColor: UIColor = self.fillColor// (self.svgView?.penColor())!
            
            var newColor: UIColor = currentColor.lighten(newValue)
            if self.saturationValue > 0.01 {
                newColor = self.calculateSatuartion(color: newColor, value: self.saturationValue)
                self.saturatedColor = newColor
            }
            self.svgView!.setPenColor(newColor)
        }
        get {
            return self.brightnessValue
        }
    }
    
    public var svgSaturation: CGFloat {
        set {
            self.saturationValue = newValue
            let currentColor: UIColor = self.fillColor // (self.svgView?.penColor())!

            var newColor: UIColor = calculateSatuartion(color: currentColor, value: newValue)
            if self.brightnessValue > 0.01 {
                newColor = self.calculateBrightness(color: newColor, value: self.brightnessValue)
                self.brightColor = newColor
            }
            self.svgView?.setPenColor(newColor)
        }
        get {
            return self.saturationValue
        }
    }

    override open func didMoveToSuperview() {
        super.didMoveToSuperview()
        if self.superview != nil {
            showEditingHandles()
            refresh()
        }
    }

    func setupBorder() {
        border = CAShapeLayer(layer: layer)
        border?.strokeColor = kBorderColorDefault.cgColor
        border?.fillColor = nil
        border?.lineWidth = kBorderWidthDefault
    }
    
    override open func layoutSubviews() {
        border?.path = UIBezierPath(rect: (self.svgView?.frame)!).cgPath
        border?.frame = CGRect(x: 0, y: 0, width: (self.svgView?.frame.size.width)! - 36.0, height: (self.svgView?.frame.size.height)! - 36.0)
        
        
        //(self.svgView?.bounds)!
//        borderLayer?.path = UIBezierPath(rect: (self.svgView?.frame)!).cgPath //, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: 20, height: 20)).cgPath
    }
    
    func initialSetup(fileName: String, frame: CGRect) {
        self.svgView = SVGView.init(template: fileName, frame: self.bounds.insetBy(dx: kGlobalInset, dy: kGlobalInset))
        self.svgView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.svgView?.clipsToBounds = true
        insertSubview(self.svgView!, at: 0)
        
        setupBorder()
        setupCloseAndRotateView()
        addGestureRecognizer(moveGestureRecognizer)
        addGestureRecognizer(singleTapShowHide)
        moveGestureRecognizer.require(toFail: closeTap)
        closeView!.addGestureRecognizer(closeTap)
        rotateView!.addGestureRecognizer(panRotateGesture)
        enableMoveRestriction = false
        enableClose = true
        enableRotate = true
        showEditingHandles()
        self.svgView?.becomeFirstResponder()
        
//        self.backgroundColor = UIColor.yellow
//        self.labelTextView.backgroundColor = UIColor.green
    }
}

extension BVSvgView: UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if (isShowingEditingHandles) {
            return true
        }
        return false
    }

    public func textViewDidBeginEditing(_ textView: UITextView) {
        if let delegate: BVSvgViewDelegate = delegate {
            if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidStartEditing(_:))) {
                delegate.svgViewDidStartEditing!(self)
            }
        }
    }

    public func textViewDidEndEditing(_ textView: UITextView) {
        if let delegate: BVSvgViewDelegate = delegate {
            if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidEndEditing(_:))) {
                delegate.svgViewDidEndEditing!(self)

                if (isShowingEditingHandles) {
                    hideEditingHandles()
                }
            }
        }
    }
}

//extension BVSvgView: UIGestureRecognizerDelegate, adjustFontSizeToFillRectProtocol {
//
//    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
//        if gestureRecognizer == singleTapShowHide {
//            return true
//        }
//        return false
//    }

extension BVSvgView  {
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == singleTapShowHide {
            return true
        }
        return false
    }
    
    func contentTapped(_ recognizer: UITapGestureRecognizer) {
        if !isShowingEditingHandles {
            self.showEditingHandles()
        
            if let delegate: BVSvgViewDelegate = delegate {
                delegate.svgViewDidSelected!(self)
            }
        }
    }
    
    func closeTap(_ recognizer: UITapGestureRecognizer) {
        //        self.removeFromSuperview()      //FIXME: ideally removing svg from its super view is part of its super view not this labbel view
        
        if let delegate: BVSvgViewDelegate = delegate {
            if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidClose(_:))) {
                delegate.svgViewDidClose!(self)
            }
        }
    }
    
    func moveGesture(_ recognizer: UIPanGestureRecognizer) {
        if !isShowingEditingHandles {
            self.showEditingHandles()
        
            if let delegate: BVSvgViewDelegate = delegate {
                delegate.svgViewDidSelected!(self)
            }
        }
    
        touchLocation = recognizer.location(in: superview)
        
        switch recognizer.state {
        case .began:
            beginningPoint = touchLocation
            beginningCenter = center
            
            center = estimatedCenter()
            beginBounds = bounds
            
            if let delegate: BVSvgViewDelegate = delegate {
                delegate.svgViewDidBeginEditing!(self)
            }
            
        case .changed:
            center = estimatedCenter()
            
            if let delegate: BVSvgViewDelegate = delegate {
                delegate.svgViewDidChangeEditing!(self)
            }
            
        case .ended:
            center = estimatedCenter()
            
            if let delegate: BVSvgViewDelegate = delegate {
                delegate.svgViewDidEndEditing!(self)
            }
            
        default:break
            
        }
    }
    
    private func rotateLabelWith(angle: CGFloat) {
        transform = CGAffineTransform(rotationAngle: -angle)
    }
    
    func rotateViewPanGesture(_ recognizer: UIPanGestureRecognizer) {

        touchLocation = recognizer.location(in: superview)
        let center = Utility.CGRectGetCenter(self.frame)
        
        switch recognizer.state {
        case .began:
//            self.tappedAt = self.frame
//            self.tappedAt.size = (superview?.frame.size)!
            
            deltaAngle = atan2(touchLocation!.y - center.y, touchLocation!.x - center.x) - Utility.CGAffineTrasformGetAngle(transform)
            initialBounds = bounds
            initialDistance = Utility.CGpointGetDistance(center, point2: touchLocation!)
            
            if let delegate: BVSvgViewDelegate = delegate {
                if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidBeginEditing(_:))) {
                    delegate.svgViewDidBeginEditing!(self)
                }
            }
            
        case .changed:
            let ang = atan2(touchLocation!.y - center.y, touchLocation!.x - center.x)
            
            let angleDiff = deltaAngle! - ang
//            transform = CGAffineTransform(rotationAngle: -angleDiff)
            self.rotationAngle =  angleDiff
            layoutIfNeeded()
            
            //Finding scale between current touchPoint and previous touchPoint
            
//            print("touc location x:\(touchLocation!.x) y:\(touchLocation!.y)");
            
            let scale = sqrtf(Float(Utility.CGpointGetDistance(center, point2: touchLocation!)) / Float(initialDistance!))
            let scaleRect = Utility.CGRectScale(initialBounds!, wScale: CGFloat(scale), hScale: CGFloat(scale))
            let kGlobalInset : CGFloat = 18.0
            if scaleRect.size.width >= (1 + kGlobalInset * 2) && scaleRect.size.height >= (1 + kGlobalInset * 2) {
                //  if fontSize < 100 || CGRectGetWidth(scaleRect) < CGRectGetWidth(self.bounds) {
//                if scale < 1 && labelTextView.fontSize <= 9 {
//
//                }else {
                    bounds = scaleRect
//                updateSizeToFillContensWith(self, touchPoint: touchLocation!, previous: tappedAt.origin)
                updateSizeToFillContensWith(frame: scaleRect)
//                    refresh()
//                }
            }
            
            if let delegate: BVSvgViewDelegate = delegate {
                if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidChangeEditing(_:))) {
                    delegate.svgViewDidChangeEditing!(self)
                }
            }
        case .ended:
            if let delegate: BVSvgViewDelegate = delegate {
                if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidEndEditing(_:))) {
                    delegate.svgViewDidEndEditing!(self)
                }
            }
            
            self.refresh()
            
        //self.adjustsWidthToFillItsContens(self, labelView: labelTextView)
        default:break
            
        }
    }
}

extension BVSvgView {
    
    func createCircleImage (rect: CGRect, radius: CGFloat, backgroundColor:UIColor, borderColor:CGColor, borderWidth:CGFloat = kBorderWidthDefault) -> UIImageView {
        
        let inset: CGFloat = 36.0//kGlobalInset * 2.2
        let circleImage = UIImageView (frame: rect)
        circleImage.autoresizingMask = [.flexibleRightMargin, .flexibleBottomMargin]
        circleImage.layer.borderColor = borderColor
        circleImage.layer.borderWidth = borderWidth
        circleImage.contentMode = .scaleAspectFill
        circleImage.clipsToBounds = true
        circleImage.backgroundColor = backgroundColor
        circleImage.layer.cornerRadius = inset * 0.5
        circleImage.alpha = 0.9
        
        return circleImage
    }
    
    func drawXOnImage (view: UIImageView, color:UIColor, borderWidth:CGFloat = kBorderWidthDefault) {
        
        UIGraphicsBeginImageContext(view.frame.size)
        
        var maxWidth: CGFloat = view.frame.size.width
        var maxHeight: CGFloat = view.frame.size.height
        
        let inset: CGFloat = (maxWidth + maxHeight) * 0.13
        maxWidth = maxWidth - inset
        maxHeight = maxHeight - inset
        
        var startPoint: CGPoint = CGPoint.zero //view.frame.origin
        var endPoint = CGPoint(x: maxWidth, y: maxHeight)
        startPoint = CGPoint(x: startPoint.x + inset, y:startPoint.y + inset)
        
        let contextSize = CGSize(width: maxWidth, height: maxHeight)
        
        UIGraphicsBeginImageContextWithOptions(contextSize, false, 0)
        let path = UIBezierPath()
        path.lineWidth = borderWidth
        
        color.setStroke()
        //draw the path and make visible
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        path.stroke()
        
        startPoint = CGPoint(x: maxWidth, y: inset)
        endPoint = CGPoint(x: inset, y: maxHeight)
        path.move(to: startPoint)
        path.addLine(to: endPoint)
        path.stroke()
        
        //create image from path and add to subview
        let image = UIGraphicsGetImageFromCurrentImageContext()
        let imageView = UIImageView(image: image)
        view.addSubview(imageView)
        UIGraphicsEndImageContext()
    }
    
//    func setupCloseAndRotateView() {
//        //-- add Close view
//        let inset: CGFloat = 36.0//kGlobalInset * 2.2
//        var rect = CGRect(x: self.bounds.size.width - inset, y: 0, width: inset, height: inset)
//        closeView = createCircleImage(rect: rect, radius: inset * 0.5, backgroundColor: UIColor.orange,
//                                      borderColor: UIColor.red.cgColor, borderWidth: 1)
//        closeView?.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
//        closeView?.isUserInteractionEnabled = true
//
//        drawXOnImage(view: closeView!, color: UIColor.white)
//        addSubview(closeView!)
//
//        //-- add Rotate view
//        rect = CGRect.zero
//        rect = CGRect(x: self.bounds.size.width - inset, y: self.bounds.size.height - inset, width: inset, height: inset)
//        rotateView = createCircleImage(rect: rect, radius: inset * 0.5, backgroundColor: UIColor.orange,
//                                       borderColor: UIColor.white.cgColor, borderWidth: 6)
//        rotateView?.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin]
//        rotateView?.isUserInteractionEnabled = true
//        addSubview(rotateView!)
//    }
    
    func setupCloseAndRotateView() {
        
        //-- add Close view
        let dia: CGFloat = kHandleRadius * 2
        var rect = CGRect(x: self.bounds.size.width - dia, y: self.bounds.origin.y, width: dia, height: dia)
        
        closeView = createCircleImage(rect: rect, radius: kHandleRadius, backgroundColor: UIColor.orange,
                                      borderColor: UIColor.red.cgColor, borderWidth: 1)
        
        closeView?.autoresizingMask = [.flexibleLeftMargin, .flexibleBottomMargin]
        closeView?.isUserInteractionEnabled = true
        
        drawXOnImage(view: closeView!, color: UIColor.white)
        addSubview(closeView!)
        
        
        //-- add Rotate view
        rect = CGRect.zero
        rect = CGRect(x: self.bounds.size.width - dia, y: self.bounds.size.height - dia, width: dia, height: dia)
        
        rotateView = createCircleImage(rect: rect, radius: kHandleRadius, backgroundColor: UIColor.orange,
                                       borderColor: UIColor.white.cgColor, borderWidth: 6)
        
        rotateView?.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin]
        rotateView?.isUserInteractionEnabled = true
        addSubview(rotateView!)
    }
}

extension BVSvgView {
    
    fileprivate func refresh() {
        if let superView: UIView = superview {
            if let transform: CGAffineTransform = superView.transform {
                let scale = Utility.CGAffineTransformGetScale(transform)
                let t = CGAffineTransform(scaleX: scale.width, y: scale.height)
                closeView?.transform = t.inverted()
                rotateView?.transform = t.inverted()
                
                if (isShowingEditingHandles) {
                    if let border: CALayer = border {
                        self.layer.insertSublayer(border, at: 1)
                    }
                }else {
                    border?.removeFromSuperlayer()
                }
            }
        }
    }
    
    public var isEditingHandlerHidden: Bool {
        get {
            return !isShowingEditingHandles
        }
    }
    
    public func hideEditingHandles() {
        lastTouchedView = nil
        
        isShowingEditingHandles = false
        
        if enableClose {
            closeView?.isHidden = true
//            borderLayer?.isHidden = true
        }
        if enableRotate {
            rotateView?.isHidden = true
        }
        
//        labelTextView.resignFirstResponder()
        
        refresh()
        
        if let delegate : BVSvgViewDelegate = delegate {
            if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidHideEditingHandles(_:))) {
                delegate.svgViewDidHideEditingHandles!(self)
            }
        }
    }
    
    public func showEditingHandles() {
        lastTouchedView?.hideEditingHandles()
        
        isShowingEditingHandles = true
        
        lastTouchedView = self
        
        if enableClose {
            closeView?.isHidden = false
//            borderLayer?.isHidden = false
        }
        
        if enableRotate {
            rotateView?.isHidden = false
        }
        
        self.refresh()
        
        if let delegate: BVSvgViewDelegate = delegate {
            if delegate.responds(to: #selector(BVSvgViewDelegate.svgViewDidShowEditingHandles(_:))) {
                delegate.svgViewDidShowEditingHandles!(self)
            }
        }
    }
    
    fileprivate func estimatedCenter() -> CGPoint{
        let newCenter: CGPoint!
        var newCenterX = beginningCenter!.x + (touchLocation!.x - beginningPoint!.x)
        var newCenterY = beginningCenter!.y + (touchLocation!.y - beginningPoint!.y)
        
        if (enableMoveRestriction) {
            if (!(newCenterX - 0.5 * frame.width > 0 &&
                newCenterX + 0.5 * frame.width < superview!.bounds.width)) {
                newCenterX = center.x;
            }
            if (!(newCenterY - 0.5 * frame.height > 0 &&
                newCenterY + 0.5 * frame.height < superview!.bounds.height)) {
                newCenterY = center.y;
            }
            newCenter = CGPoint(x: newCenterX, y: newCenterY)
        }else {
            newCenter = CGPoint(x: newCenterX, y: newCenterY)
        }
        return newCenter
    }
    
    private func updateSizeToFillContensWith(frame: CGRect) {
        
        self.bounds = frame
//        let attributedText = labelView.attributedText
//
//        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//
//        let w1 = (ceilf(Float((recSize?.size.width)!)) + 24 < 50) ? view.labelTextView.bounds.size.width : CGFloat(ceilf(Float((recSize?.size.width)!)) + 24)
//        let h1 = (ceilf(Float((recSize?.size.height)!)) + 24 < 50) ? 50 : CGFloat(ceilf(Float((recSize?.size.height)!)) + 24)
//
//        var viewFrame = view.bounds
//        viewFrame.size.width = w1 + 24
//        viewFrame.size.height = h1 + 18
//        view.bounds = viewFrame
//
//        //        print("Text Frame x: \(recSize?.size.width)");
    }
    
    
//    func adjustsWidthToFillItsContens(_ view: BVLabelView, labelView: BVTextView) {
    func updateSizeToFillContensWith(_ view: BVSvgView, touchPoint: CGPoint, previous: CGPoint) {
        
//        let touchPoint = touches.first!.location(in: self)
//        let previous = touches.first!.previousLocation(in: self)
        
        let deltaWidth =  previous.x - touchPoint.x
        let deltaHeight = previous.y - touchPoint.y
        
        let width = view.frame.size.width;
        let height = view.frame.size.height;
        let originFrame = view.frame
        var finalFrame: CGRect = originFrame
        
        let distance = CGPoint(x: 1.0 - (deltaWidth / width),
                               y: 1.0 - (deltaHeight / height))
        
        let scale = (distance.x + distance.y) * 0.5
        finalFrame.size.width = width * scale
        finalFrame.size.height = height * scale
        
//        let attributedText = labelView.attributedText
//
//        let recSize = attributedText?.boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
//
//        let w1 = (ceilf(Float((recSize?.size.width)!)) + 24 < 50) ? view.labelTextView.bounds.size.width : CGFloat(ceilf(Float((recSize?.size.width)!)) + 24)
//        let h1 = (ceilf(Float((recSize?.size.height)!)) + 24 < 50) ? 50 : CGFloat(ceilf(Float((recSize?.size.height)!)) + 24)
//
//        var viewFrame = view.bounds
//        viewFrame.size.width = w1 + 24
//        viewFrame.size.height = h1 + 18
//        view.bounds = viewFrame
//

    }
}











