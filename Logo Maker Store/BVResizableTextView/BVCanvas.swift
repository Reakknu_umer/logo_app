//
//  BVCanvas.swift
//  
//
//  Created by Shafi Ahmed on 19/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit
//import Macaw

fileprivate let kGlobalInset: CGFloat = 18.0;
fileprivate let kBorderWidthDefault: CGFloat = 3
fileprivate let kBorderColorDefault = UIColor.red
fileprivate let kMinimumFrameWidth: CGFloat = 25;
fileprivate let kMinimumFrameHeight: CGFloat = 25;
public let CANVAS_DEFAULT_COLOR = UIColor(red: 233/256, green: 233/256, blue: 233/256, alpha: 1.0)
private let INVALID_ANGLE: CGFloat = 361.0
private let IGNORE_ANGLE: CGFloat = 362.0

@objc public protocol BVCanvasDelegate: NSObjectProtocol {

    @objc optional func svgViewDidShowEditingHandles(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidHideEditingHandles(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidEndEditing(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidBeginMoving(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidMoving(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidSelected(_ label: BVSvgView) -> Void
    @objc optional func svgViewDidClose(_ label: BVSvgView) -> Void
  
    
    //-- Occurs when a touch gesture event occurs on close button.
    @objc optional func labelViewDidClose(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was shown.
    @objc optional func labelViewDidShowEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when border and control buttons was hidden.
    @objc optional func labelViewDidHideEditingHandles(_ label: BVLabelView) -> Void
    
    //-- Occurs when label lose being first responder.
    @objc optional func labelViewDidEndEditing(_ label: BVLabelView) -> Void
    
    //-- Occurs when label starts move or rotate.
    @objc optional func labelViewDidBeginMoving(_ label: BVLabelView) -> Void
    
    //--Occurs when label continues move or rotate.
    @objc optional func labelViewDidMoving(_ label: BVLabelView) -> Void
    
    //-- Occurs when label ends move or rotate.
    @objc optional func labelViewDidSelected(_ label: BVLabelView) -> Void
    
    //-- Occurs when label become first responder.
    @objc optional func labelViewDidBeginEditing(_ label: BVLabelView) -> Void
}

@objcMembers public class BVCanvas: UIImageView, UIGestureRecognizerDelegate {
    
    let kAllowBeginUndoGrouping: Bool = true
    private let kLabelSizeDefault: CGSize = CGSize(width: 60, height: 40)
    let kSvgSizeDefault: CGSize = CGSize(width: 150, height: 150)
    let kRnadomBaseValueForLabelPosition: CGFloat = 100
    
    private var hueFromDefaultyBackgroundColor: CGFloat = -1
    //TODO: currentlyEditingLabel should be private, ad a getter for this to be acces from outside this class
    public var currentlyEditingLabel: BVLabelView!
    public var currentlyEditingSvg: BVSvgView!
    
    public var labelStack: NSMutableArray!
    public var svgStack: NSMutableArray!
    private var lastSavedAngle: CGFloat = 0
    private var currentAngle: CGFloat = 0
    
    private var renderedView: UIView!
    var delegate: BVCanvasDelegate?
    
    var beginUndoGroupingFlag: Bool = false
    var undoer = UndoManager()
    
    override public var undoManager : UndoManager? {
        return self.undoer
    }
    
    internal lazy var tapOutsideGestureRecognizer: UITapGestureRecognizer! = {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(BVCanvas.tapOutside))
        tapGesture.delegate = self
        return tapGesture
        
    }()
    
    init() {
        super.init(frame: CGRect.zero)
        isUserInteractionEnabled = true
        labelStack = []
        svgStack = []
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        labelStack = []
        svgStack = []
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isUserInteractionEnabled = true
        labelStack = []
        svgStack = []
    }
}

extension BVCanvas: BVLabelViewDelegate, BVSvgViewDelegate {
    
    public func svgViewDidClose(_ label: BVSvgView) {
        if let delegate: BVCanvasDelegate = delegate {
            self.removeSvgFromTheStack(label)
            
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidClose(_:))) {
                delegate.svgViewDidClose!(label)
            }
        }
    }
    
    public func svgViewDidSelected(_ label: BVSvgView) {
        for svgItem in svgStack {
            if let label: BVSvgView = svgItem as? BVSvgView {
                label.hideEditingHandles()
            }
        }
        label.showEditingHandles()
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidSelected(_:))) {
                delegate.svgViewDidSelected!(label)
            }
        }
    }
    
    public func svgViewDidShowEditingHandles(_ label: BVSvgView) {
        currentlyEditingSvg = label
        
        if currentlyEditingLabel != nil {
            currentlyEditingLabel.hideEditingHandles()
            currentlyEditingLabel = nil
        }
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidShowEditingHandles(_:))) {
                delegate.svgViewDidShowEditingHandles!(label)
            }
        }
    }
    
    public func svgViewDidHideEditingHandles(_ label: BVSvgView) {
        currentlyEditingSvg = nil
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidHideEditingHandles(_:))) {
                delegate.svgViewDidHideEditingHandles!(label)
            }
        }
//        label.hideEditingHandlers()
    }
    
    public func svgViewDidBeginEditing(_ label: BVSvgView) {    //-- begin moving or rotating
        //-- end undo grouping for Rotational angle
        self.beginUndoGrouping()
        self.setSVGRotationAngle(label.rotationAngle)
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidBeginMoving(_:))) {
                delegate.svgViewDidBeginMoving!(label)
            }
        }
    }
    
    public func svgViewDidChangeEditing(_ label: BVSvgView) { //-- label is still moving or rotating
        //-- set rotation angle
        self.setSVGRotationAngle(label.rotationAngle)
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidMoving(_:))) {
                delegate.svgViewDidMoving!(label)
            }
        }
    }
    
    public func svgViewDidEndEditing(_ label: BVSvgView) {
        //-- end undo grouping for Rotational angle
        self.endUndoGrouping()
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.svgViewDidEndEditing(_:))) {
                delegate.svgViewDidEndEditing!(label)
            }
        }
    }

    public func labelViewDidClose(_ label: BVLabelView) {
        if let delegate: BVCanvasDelegate = delegate {
            self.removeLabelFromTheStack(label)
            
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidClose(_:))) {
                delegate.labelViewDidClose!(label)
            }
        }
    }
    
    public func labelViewDidShowEditingHandles(_ label: BVLabelView) {
        if currentlyEditingSvg != nil {
            currentlyEditingSvg.hideEditingHandles()
        }
        
        currentlyEditingLabel = label
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidShowEditingHandles(_:))) {
                delegate.labelViewDidShowEditingHandles!(label)
            }
        }
    }
    
    public func labelViewDidHideEditingHandles(_ label: BVLabelView) {
        currentlyEditingLabel = nil
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidHideEditingHandles(_:))) {
                delegate.labelViewDidHideEditingHandles!(label)
            }
        }
    }
    
    public func labelViewDidStartEditing(_ label: BVLabelView) {
        currentlyEditingLabel = label
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidBeginEditing(_:))) {
                delegate.labelViewDidBeginEditing!(label)
            }
        }
    }
    
    public func labelViewDidSelected(_ label: BVLabelView) {
        if currentlyEditingSvg != nil {
            currentlyEditingSvg.hideEditingHandles()
            currentlyEditingSvg = nil
        }
        
        for labelItem in labelStack {
            if let label: BVLabelView = labelItem as? BVLabelView {
                label.hideEditingHandles()
            }
        }
        label.showEditingHandles()
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidSelected(_:))) {
                delegate.labelViewDidSelected!(label)
            }
        }
    }
    
    public func labelViewDidBeginEditing(_ label: BVLabelView) {    //-- begin moving or rotating
        //-- end undo grouping for Rotational angle
        self.beginUndoGrouping()
        self.setLabelRotationAngle(label.rotationAngle)
        
        /* labels.removeObject(label) */
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidBeginMoving(_:))) {
                delegate.labelViewDidBeginMoving!(label)
            }
        }
//        if currentlyEditingSvg != nil {
//            currentlyEditingSvg.hideEditingHandlers()
//        }
    }
    
    public func labelViewDidChangeEditing(_ label: BVLabelView) { //-- label is still moving or rotating
        //-- set rotation angle
        self.setLabelRotationAngle(label.rotationAngle)
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidMoving(_:))) {
                delegate.labelViewDidMoving!(label)
            }
        }
    }
    
    public func labelViewDidEndEditing(_ label: BVLabelView) {
        //-- end undo grouping for Rotational angle
        self.endUndoGrouping()
        
        if let delegate: BVCanvasDelegate = delegate {
            if delegate.responds(to: #selector(BVCanvasDelegate.labelViewDidEndEditing(_:))) {
                delegate.labelViewDidEndEditing!(label)
            }
        }
    }
}

extension BVCanvas {
    
    func randomPontOnScreenWith(baseVale: CGFloat) -> CGPoint {
        let kLabelRandomPosition: CGPoint = CGPoint(x: CGFloat(arc4random()).truncatingRemainder(dividingBy: baseVale),
                                                            y: CGFloat(arc4random()).truncatingRemainder(dividingBy: baseVale))
        return kLabelRandomPosition
    }
    
    //-- MARK: Add label
    func addNewLabelToTheStack(_ newlable: BVLabelView) {
        if self.undoer.isUndoing {
            removeLabelFromTheStack(newlable)
        }
        (self.undoer.prepare(withInvocationTarget: self) as AnyObject).addNewLabelToTheStack(newlable)
        self.undoer.setActionName("addLabel")
        labelStack.add(newlable)
    }
    
    func removeLabelFromTheStack(_ label: BVLabelView) {
        label.removeFromSuperview()
        labelStack.remove(label)
        print("labels in the stack are: \(labelStack.count)")
    }
    
    public func addLabel() {
        if let label: BVLabelView = currentlyEditingLabel {
            label.hideEditingHandles()
        }
        
        let randomPoint:CGPoint = self.randomPontOnScreenWith(baseVale: kRnadomBaseValueForLabelPosition)
        let frame = CGRect(x: bounds.midX - randomPoint.x, y: bounds.midY - randomPoint.y,
                                width: kLabelSizeDefault.width, height: kLabelSizeDefault.height)
        
        let labelView = BVLabelView(frame: frame)
        labelView.delegate = self
        labelView.showsContentShadow = false
        labelView.labelTextView.fontName = kFontNameDefault
        labelView.borderColor = kBorderColorDefault
        addSubview(labelView)
        currentlyEditingLabel = labelView
        adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        self.addNewLabelToTheStack(labelView)

        addGestureRecognizer(tapOutsideGestureRecognizer) //-- now labelView will lose its focus if tapped outside the labelView
    }
    
    public func addLabel(withFrame frame : CGRect ,andText text : String) {
        if let label: BVLabelView = currentlyEditingLabel {
            label.hideEditingHandles()
        }
        let labelView = BVLabelView(frame: frame)
        labelView.delegate = self
        labelView.showsContentShadow = false
        labelView.labelTextView.fontName = kFontNameDefault
        labelView.borderColor = kBorderColorDefault
        addSubview(labelView)
        currentlyEditingLabel = labelView
        adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        self.addNewLabelToTheStack(labelView)
        
        addGestureRecognizer(tapOutsideGestureRecognizer)
    }
    
    public func limitImageViewToSuperView() {
        if superview == nil {
            return
        }
        self.translatesAutoresizingMaskIntoConstraints = true
        let imageSize = self.image?.size
        let aspectRatio = imageSize!.width / imageSize!.height
        
        if let width = imageSize?.width, let height = imageSize?.height {
            if width > height {
                bounds.size.width = superview!.bounds.size.width
                bounds.size.height = superview!.bounds.size.width / aspectRatio
            }else {
                bounds.size.height = superview!.bounds.size.height
                bounds.size.width = superview!.bounds.size.height * aspectRatio
            }
        }
    }
}

extension BVCanvas {
    
    func tapOutside() {
        if let _: BVLabelView = currentlyEditingLabel {
            currentlyEditingLabel.hideEditingHandles()
        }
        
        if let _: BVSvgView = currentlyEditingSvg {
            currentlyEditingSvg.hideEditingHandles()
        }
    }
}

extension BVCanvas: adjustFontSizeToFillRectProtocol {
    
    public enum textShadowPropterties {
        case offSet(CGSize)
        case color(UIColor)
        case blurRadius(CGFloat)
    }
    
    public func setFontName(_ name: String) {
        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget: self) as AnyObject).setFontName(currentlyEditingLabel.labelTextView.fontName)
            self.undoer.setActionName("setFontName")
            currentlyEditingLabel.labelTextView.fontName = name
            adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        }
    }
    
    public func fontName()-> String {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.fontName
        }
        return kFontNameDefault
    }
    
    public var fontSize: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.fontSize = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
                
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.fontSize
            }
            return kFontSizeDefault
        }
    }
    
    /**
     It calculates and returns the outcome of the division of the two parameters.
     
     ## Important Notes ##
     1. Both parameters are **double** numbers.
     2. For a proper result the second parameter *must be other than 0*.
     3. If the second parameter is 0 then the function will return nil.
     
     */
    func performDivision(number1: Double, number2: Double) -> Double! {
        if number2 != 0 {
            return number1 / number2
        }
        else {
            return nil
        }
    }
    
    //-- MARK: Canvass attriute - This is the only attribute for Canvass
    public var canvasBackgroundColor: CGFloat {
        set {
            undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.canvasBackgroundColor), object: self.hueFromDefaultyBackgroundColor )
            
            let hsba = Utility.rgbaToHsba(self.backgroundColor!)
            let rgba:UIColor = UIColor(hue: newValue, saturation: 1.0, brightness: hsba.b, alpha: hsba.a)
            self.backgroundColor = rgba
            self.hueFromDefaultyBackgroundColor = newValue
        }
        
        get {
            if self.hueFromDefaultyBackgroundColor < 0 {
                let hsba = Utility.rgbaToHsba(self.backgroundColor!)
                return hsba.h
            }
            return self.hueFromDefaultyBackgroundColor
        }
    }
    
    //-- MARK: TextField attriutes: angle, text color, bg color, font, text space, saturation, brightness, opacity etc.
    @objc public func setLabelRotationAngle(_ newValue: CGFloat) {

        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setLabelRotationAngle(currentlyEditingLabel.rotationAngle)
            self.undoer.setActionName("rotationAngle")
            currentlyEditingLabel.transform = CGAffineTransform(rotationAngle: -newValue)
            currentlyEditingLabel.rotationAngle = newValue
        }
    }
    
    func labelRotationAngle() ->CGFloat {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.rotationAngle
        }
        return 1.0
    }
    
    @objc public func setLabelBounds(_ newValue: CGRect) {
        
        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setLabelBounds(currentlyEditingLabel.currentBounds)
            self.undoer.setActionName("labelBounds")
//            currentlyEditingLabel.transform = CGAffineTransform(rotationAngle: -newValue)
//            currentlyEditingLabel.rotationAngle = newValue
        }
    }
    
    func labelBounds() ->CGRect {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.currentBounds
        }
        return CGRect.zero
    }
    
   @objc public var textColor: UIColor! {
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.textColor), object: currentlyEditingLabel.labelTextView.foregroundColor)
                currentlyEditingLabel.labelTextView.foregroundColor = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.foregroundColor
            }
            return UIColor.clear
        }
    }
    
    public var textBackgroundColour: UIColor! {
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.textBackgroundColour), object: currentlyEditingLabel.labelTextView.backgroundColour)
                currentlyEditingLabel.labelTextView.backgroundColour = newValue
            }
        }
        
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.backgroundColour
            }
            return UIColor.clear
        }
    }
    
    @objc public func setTextOpacity(_ newValue: CGFloat) {
        
        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setTextOpacity(currentlyEditingLabel.labelTextView.textOpacity)
            self.undoer.setActionName("textOpacity")
            currentlyEditingLabel.labelTextView.textOpacity = newValue
        }
    }
    func textOpacity() ->CGFloat {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.textOpacity
        }
        return 1.0
    }
    
    @objc public var backgroundOpacity: CGFloat {
        set {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.backgroundOpacity), object: currentlyEditingLabel.labelTextView.backgroundOpacity)
                currentlyEditingLabel.labelTextView.backgroundOpacity = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                undoManager?.registerUndo(withTarget: self, selector: #selector(setter: BVCanvas.backgroundOpacity), object: currentlyEditingLabel.labelTextView.backgroundOpacity)
                return currentlyEditingLabel.labelTextView.backgroundOpacity!
            }
            return 1.0
        }
    }
    
    public var textAlignment: NSTextAlignment! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.alignment = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.alignment
            }
            return NSTextAlignment.center
        }
    }
    
    @objc public func setTextSpacing(_ newValue: CGFloat) {

        if currentlyEditingLabel != nil {
            (self.undoer.prepare(withInvocationTarget:self) as AnyObject).setTextSpacing(currentlyEditingLabel.labelTextView.textSpacing)
            self.undoer.setActionName("textSpacing")
            currentlyEditingLabel.labelTextView.textSpacing = newValue
            adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
        }
    }
    
    public func textSpacing() -> CGFloat {
        if currentlyEditingLabel != nil {
            return currentlyEditingLabel.labelTextView.textSpacing
        }
        return kTextSpacingDefault
    }
    
    public var lineSpacing: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.lineSpacing = newValue
                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.lineSpacing
            }
            return kLineSpacingDefault
        }
    }
    
    public var textBrightness: CGFloat {
        
        //TODO: ideally brightness value should be added as NSAttributedStringKey in BVTextView instead of storing it in BVLabelView
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.brightnessValue = newValue
                currentlyEditingLabel.labelTextView.brightness = newValue
//                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
        
//                return currentlyEditingLabel.labelTextView.brightness
                return currentlyEditingLabel.brightnessValue
            }
            return 0
            
        }
    }
    
    //TODO: ideally saturation value should be added as NSAttributedStringKey in BVTextView instead of storing it in BVLabelView
    public var textSaturation: CGFloat {
        
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.saturationValue = newValue
                currentlyEditingLabel.labelTextView.saturation = newValue
//                adjustsWidthToFillItsContens(currentlyEditingLabel, labelView: currentlyEditingLabel.labelTextView)
            }
        }
        get {
            if currentlyEditingLabel != nil {
//                return currentlyEditingLabel.labelTextView.saturation
                return currentlyEditingLabel.saturationValue
            }
            return 0
        }
    }
    
    public var textShadowOffset: CGSize! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowOffset = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.shadow?.shadowOffset
            }
            return CGSize.zero
        }
    }
    
    public var textShadowColor: UIColor! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowColor = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return (currentlyEditingLabel.labelTextView.shadow?.shadowColor) as? UIColor
            }
            return UIColor.clear
        }
    }
    
    public var textShadowBlur: CGFloat! {
        set {
            if currentlyEditingLabel != nil {
                currentlyEditingLabel.labelTextView.textShadowBlur = newValue
            }
        }
        get {
            if currentlyEditingLabel != nil {
                return currentlyEditingLabel.labelTextView.shadow?.shadowBlurRadius
            }
            return 0.0
        }
    }
}

extension BVCanvas {
    public func showEditingHandle() {
        if currentlyEditingLabel != nil {
            currentlyEditingLabel.showEditingHandles()
        }
    }
}

extension BVCanvas {
    
    public func beginUndoGrouping() {
        if beginUndoGroupingFlag != kAllowBeginUndoGrouping {
            beginUndoGroupingFlag = kAllowBeginUndoGrouping
            print("beginUndoGrouping# is called \n")
            self.undoer.beginUndoGrouping()
        }
    }
    
    public func endUndoGrouping() { //sender: Any) {
        
        if beginUndoGroupingFlag == kAllowBeginUndoGrouping {
            print("endUndoGrouping##### is called\n")
            self.undoer.endUndoGrouping()
            beginUndoGroupingFlag = !kAllowBeginUndoGrouping
        }
        if currentlyEditingLabel != nil {
            currentlyEditingLabel.becomeFirstResponder()
        }
    }

    func undo()  {
        self.undoer.undo()
    }

    func redo()  {
        self.undoer.redo()
    }
}


//func handlePan(pan: UIPanGestureRecognizer) {
//    switch pan.state {
//    case .Began:
//        if CGRectContainsPoint(self.pannableView.frame, pan.locationInView(self.pannableView)) {
//            // Gesture started inside the pannable view. Do your thing.
//        }
//}








