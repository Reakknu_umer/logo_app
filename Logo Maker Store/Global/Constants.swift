//
//  Constants.swift
//  Logo Maker Store
//
//  Created by Md Shafi Ahmed on 31/07/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import Foundation

let DEFAULT_PADDING: CGFloat = 16
let SYMBOL_CATEGORY_PATH = "Resource/Symbol"
let SYMBOL_PNG_FOLDERNAME = "png"
let SYMBOL_SVG_FOLDERNAME = "svg"
let SYMBOL_CATEGORY_FOLDERNAME = "category"
let SYMBOL_THUMBNAIL_FILE_EXTN = ".png"
let SYMBOL_SVG_FILE_EXTN = ".svg"

let GALLERY_FOLDER_INITIAL_FOR_FREE_IMAGE: String = "Free"
let GALLERY_FOLDER_NAME_FOR_CLEAR_CATEGORY: String = "CLEAR"
let GALLERY_FOLDER_NAME_FOR_COLOR_CATEGORY: String = "COLOR"

let GALLERY_CATEGORY_MENU_PATH = "Resource/Gallery/Menu"
let GALLERY_CATEGORY_PATH = "Resource/Gallery"
let GALLERY_THUMBNAIL_FILE_NAME = "thumb"
let GALLERY_THUMBNAIL_FILE_EXTN = ".png"
let GALLERY_FILE_EXTN = ".png"

struct Constants {
    struct Defaults {

    }
    
    struct Tabs {
        static let TabOne: Int = 1
        static let TabTwo: Int = 2
        static let TabThree: Int = 3
    }
    
    struct SliderAction {
        static let touchDragEnter: Int = 0
        static let touchUpInside: Int = 1
        static let touchUpOutside: Int = 2
        static let valueChanged: Int = 3
    }
    
    //-- fonts list diplay related
    struct FontList {
        static let frameSize: CGSize = CGSize(width: 44, height: 44)
        static let frameSizeForSelectedFont: CGSize = CGSize(width: 60, height: 60)
    }
    
    //-- expanding menu related
    struct Menu {
        static let width: CGFloat = 34
        static let height: CGFloat = 34
        static let xInset: CGFloat = 44
        static let yInset: CGFloat = 10
    }
    
    struct MenuButtonIndex {
        static let undo: Int = 0
        static let redo: Int = 1
        static let angle: Int = 2
        static let copy: Int = 3
        static let layer: Int = 4
    }
    
    public struct ImageGallery {
        var category: String = ""
        var thumb: String = "thumb"
        var images = [String] ()
        var isFree = true
    }
    
    enum SymbolType: String, CustomStringConvertible {
        case CATEGORY = "category"
        case THUMBNAIL = "png"
        case SVG = "svg"
        
        var description : String {
            get {
                return self.rawValue
            }
        }
    }
    
//    struct SymbolPath {
//        public func forPngFor(category: String) -> String {
//            guard category.count > 0 else {
//                return ""
//                let path  = String("\(SYMBOL_CATEGORY_PATH)'/'\(category) '/'\(SYMBOL_PNG_FOLDERNAME)")
//                return path
//            }
//        }
//    }
//        func translated(by offset: Point) -> Rectangle {
//            var copy = self
//            copy.translate(by: offset)
//            return copy
//        }
    }
//}
