//
//  AppDelegate.h
//  Logo Maker Store
//
//  Created by iBuildX on 21/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

