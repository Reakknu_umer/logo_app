//
//  MainVC.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 21/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

class NewLogoViewC: MenuViewContrioller, MenuViewContriollerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, BVResizableTextViewDelegate{

    private var popUpViewController: PopUpViewController?
    
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var canvas: BVResizableTextView!
    @IBOutlet weak var contraintHeightOfCollectionView: NSLayoutConstraint!
    @IBOutlet var bottomTabButtons: [UIButton]!

    private var currentlyEditingLabel:BVLabelView!
    public var objects = [UIView]()
    private var selectedTab:Int = 1
    private let tab3Titles = ["Camera","Mobile Gallery","App Gallery"]
    private let tab2Titles = ["Symbols","Saturation","Opacity","Color","Brightness"]
    public var bottomSheetType:BottomSheetType = .OPACITY
    
    private let imagePicker = UIImagePickerController()
    private var idealHeightOfCollectionView:CGFloat  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.imagePicker.delegate = self
        self.idealHeightOfCollectionView = self.contraintHeightOfCollectionView.constant
    }
    
    override func menuFrame()-> CGRect {
        return canvas.frame
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("section num: \(section)")
        if selectedTab == 3{
            return self.tab3Titles.count
        }else if selectedTab == 2{
            return self.tab2Titles.count
        }else{
            return super.collectionView(collectionView, numberOfItemsInSection: section)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        print("row num: \(indexPath.row)")
        
        if selectedTab == 3{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! NewLogoCollectionViewCell
            
            let title = self.tab3Titles[indexPath.row]
            
            cell.mainTitle.text = title
            cell.mainImage.image = UIImage(named: title)
            return cell
            
        }else{
         
            let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! NewLogoCollectionViewCell
            
            if selectedTab == 2{
                
                let title = self.tab2Titles[indexPath.row]
                cell.mainTitle.text = title
                cell.mainImage.image = UIImage(named: title)
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let row = indexPath.row
        
        if self.selectedTab == 1{
            self.actionTab1(row)
        }else if self.selectedTab == 2{
            self.actionTab2(row)
        }else{
            self.actionTab3(row)
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? PopUpViewController {
            vc.newLogoVC = self
            vc.sheetType = self.bottomSheetType
            vc.heightOftheSheet = self.viewBottomContainer.frame.height
            popUpViewController = vc
        }
    }
    
    @IBAction func actionBottomTabItems(_ sender: UIButton) {
        
        let isLayoutNeed = self.selectedTab == 3
        
        if sender.isSelected{
            return
        }else{
            self.bottomTabButtons.forEach { (button) in
              
                button.isSelected = false
            }
            
            sender.isSelected = true
        }
        
        self.selectedTab = sender.tag
        
        UIView.transition(with:self.buttonTextCollectionView!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            
            self.contraintHeightOfCollectionView.constant = sender.tag == 3 ? self.viewBottomContainer.frame.height-16 : self.idealHeightOfCollectionView
            
            self.buttonTextCollectionView?.reloadData()

            if self.selectedTab == 3{
                
                let width = (self.viewBottomContainer.frame.width/3)-20
                let cellSize = CGSize(width:width, height:width)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
                layout.minimumLineSpacing = 30
                layout.minimumInteritemSpacing = 30
                self.buttonTextCollectionView?.isScrollEnabled = false
                self.buttonTextCollectionView?.setCollectionViewLayout(layout, animated: false)
                
            }else if isLayoutNeed{
                
                let cellSize = CGSize(width:60 , height:60)
                
                let layout = UICollectionViewFlowLayout()
                layout.scrollDirection = .horizontal
                layout.itemSize = cellSize
                layout.sectionInset = UIEdgeInsets(top: 1, left: 1.0, bottom: 1, right: 1.0)
                layout.minimumLineSpacing = 4
                layout.minimumInteritemSpacing = 8
                 self.buttonTextCollectionView?.isScrollEnabled = true
                self.buttonTextCollectionView?.setCollectionViewLayout(layout, animated: false)
            }
            
        })
    }
    
    public func deselectAllItemsInCollectionView(){
        
        self.buttonTextCollectionView?.selectItem(at: nil, animated: true, scrollPosition: [])

    }
    
    private func actionTab3(_ row:Int){
        
        if row == 0{
            self.askCameraPermission()

        }else if row == 1{
            self.checkPhotoLibraryPermission()

        }else{
            
        }
    }
    
    private func askCameraPermission(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == true{
            
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                self.openCameraPicker()
            } else if AVCaptureDevice.authorizationStatus(for:AVMediaType.video) ==  AVAuthorizationStatus.denied{
                self.showCameraPermissionDialog()
            }else {
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        
                        self.openCameraPicker()
                        
                    } else {
                        
                        self.showCameraPermissionDialog()
                    }
                })
            }
        }else{
            
            self.alert("Your device does not have camera!")
        }
    }
    
    // MARK: -  For Photos
    private func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.openPhotoPicker()
            break
            
        case .denied, .restricted :
            self.showPhotoPermissionDialog()
            break
            
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                switch status {
                case .authorized:
                    self.openPhotoPicker()
                    break
                // as above
                case .denied, .restricted:
                    self.showPhotoPermissionDialog()
                    break
                // as above
                case .notDetermined:
                    self.alert("Unexpected error occured for accessing photo library")
                    break
                    
                }
            }
        }
    }
    
    private func alert(_ message:String){
        
        let controller = UIAlertController.init(title: "Choose Image", message: message, preferredStyle: .alert)
        let actionCancel = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        controller.addAction(actionCancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    private func showPhotoPermissionDialog(){
        
        self.alert("Go to settings and grant permission for photos access")
    }
    
    private func openPhotoPicker(){
        
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func openCameraPicker(){
        
        self.imagePicker.sourceType = .camera
       
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func showCameraPermissionDialog(){
        
        self.alert("Go to settings and grant permission for camera access")
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let choosenImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    private func actionTab2(_ row:Int){
        
        if row == 0{
            
            self.performSegue(withIdentifier: "SymbolsViewController", sender: self)
        }else if row == 1{
            self.bottomSheetType = .SATURATION
        }else if row == 2{
            self.bottomSheetType = .OPACITY
        }else if row == 3{
            self.bottomSheetType = .COLOR
        }else if row == 4{
            self.bottomSheetType = .BRIGHTNESS
        }
        self.performSegue(withIdentifier: "PopUpViewController", sender: self)
        
    }
    
    private func actionTab1(_ row:Int){
        
        if row == 0{
            self.createNewText()
            return
        }
        
        switch row {
        case 1:
            self.bottomSheetType = .FONT
        case 2:
            self.bottomSheetType = .OPACITY
            break
        case 3:
            self.bottomSheetType = .COLOR
        case 4:
            self.bottomSheetType = .TEXTSPACING
        case 5:
            self.bottomSheetType = .SATURATION
        case 6:
            self.bottomSheetType = .BRIGHTNESS
        default:
            break
        }
        
        if self.currentlyEditingLabel != nil {
            currentlyEditingLabel.showEditingHandles()
            currentlyEditingLabel.becomeFirstResponder //puts cursor on text field
        }
        self.performSegue(withIdentifier: "PopUpViewController", sender: self)
    }
    
    //-- MARK: MenuViewControlled Delegates
    func onMenuButtonTapped(_ index: Int) {
        switch index {
        case Constants.MenuButtonIndex.undo:
            self.canvas.undo()
            break
        case Constants.MenuButtonIndex.redo:
            self.canvas.redo()
            break
        case Constants.MenuButtonIndex.angle:
            break
        case Constants.MenuButtonIndex.copy:
            break
        case Constants.MenuButtonIndex.layer:
            break
        default:
            break
        }
    }
    
    private func createNewText(){
        
        // Src5254691_C code220718_started_ByteVite
        // Src5254690_A code220718_started_ByteVite
        canvas.delegate = self
        canvas.addLabel()
//        canvas.textColor =   UIColor.init(hue: 0.33, saturation: 0.5, brightness: 0.5, alpha: 1)
//        viewDrawingBoard.textColor = UIColor (red: 0.3, green: 0.8, blue: 0.3, alpha: 1) // [UIColor colorWithRed:0.02f green:1.0f blue:0.0f alpha:0.8f];
        canvas.textOpacity = 1
        canvas.currentlyEditingLabel.border?.strokeColor = UIColor.red.cgColor
        canvas.fontName = "HelveticaNeue"
        canvas.fontSize = 50

        // Src5254690_A code220718_ended_ByteVite
        
//        let image: UIImage = viewDrawingBoard.renderTextOnView(stickerView)!
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
//        //-- save image in PNG format
//        let imageData:Data = UIImagePNGRepresentation(image)! // get png representation
//        let pngImage: UIImage = UIImage(data: imageData as Data)!           // rewrap
//        UIImageWriteToSavedPhotosAlbum(pngImage, nil, nil, nil)
    }
    
    //-- MARK: BVLabelView Delegates
    public func labelViewDidClose(_ label: BVLabelView) {
        print("Delegate called in NewLogoVC for LABEL IS CLOSED")
    }
    
    public func labelViewDidShowEditingHandles(_ label: BVLabelView) {
        print("Delegate called in NewLogoVC for labelViewDidShowEditingHandles")
    }
    
    public func labelViewDidHideEditingHandles(_ label: BVLabelView) {
        print("Delegate called in NewLogoVC for labelViewDidHideEditingHandles")
    }
    
    public func labelViewDidBeginEditing(_ label: BVLabelView) {    //-- begin moving or rotating
        print("Delegate called in NewLogoVC for LABEL BECOME FIRST RESPONDER")
    }
    
    
    public func labelViewDidEndEditing(_ label: BVLabelView) {
        currentlyEditingLabel = label
        print("Delegate called in NewLogoVC for labelViewDidEndEditing")
    }
    
    public func labelViewDidBeginMoving(_ label: BVLabelView) {
        currentlyEditingLabel = label
        print("Delegate called in NewLogoVC for LABEL IS MOVING OR ROTATING")
    }
    
    public func labelViewDidMoving(_ label: BVLabelView) {
        print("Delegate called in NewLogoVC for LABEL IS STILL MOVING OR ROTATING")
    }
    
    public func labelViewDidSelected(_ label: BVLabelView) {
        currentlyEditingLabel = label
        print("Delegate called in NewLogoVC for LABEL IS SELECTED")
    }

}










