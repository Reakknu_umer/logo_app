//
//  PopUpViewController.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 22/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

private let kFontSizeMax: CGFloat = 15

public enum BottomSheetType{
    case FONT, TEXTSPACING, COLOR, BACKGROUNDCOLOR, OPACITY, SATURATION, BRIGHTNESS, THUMBNAIL
}

class PopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource{
    
    private let kFontDefault: String = "Arial"
    private let kFontSizeDefault: CGFloat = 14.0
    private let kFontSpaceDefault: CGFloat = 50.0
    
    @IBOutlet weak var viewSheet: UIView!
    @IBOutlet weak var viewPanel: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var constraintHeightOfTheSheet: NSLayoutConstraint!
   
    ///Slider
    @IBOutlet weak var slider: ASValueTrackingSlider!
    @IBOutlet weak var viewSlider: UIView!
    
    //Fonts display
    @IBOutlet weak var tableViewFonts: UITableView!
    
    //Color Chooser
    @IBOutlet weak var collectionViewColor: UICollectionView!
    @IBOutlet weak var thumbCollectionView: UICollectionView!
    
    //MARK:- Public properties
    public var newLogoVC: NewLogoViewController!
    private var galleryCategory = [String] ()
    private var imageAndThumbnailFromGallery = [Constants.ImageGallery] ()
    private var isTimeForShowColoAction: Bool = false
    
    public var heightOftheSheet:CGFloat = 0
    public var sheetType:BottomSheetType!
    public var selectedTab: Int = TAB_ONE
    public var color:UIColor = .cyan
    
    private var fonts = [String]()
    private var colors:[UIColor] = [.red,.green,.cyan,.yellow,.gray,.red,.green,.cyan,.yellow,.gray]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableViewFonts.allowsMultipleSelection = false
        tableViewFonts.allowsSelection = true
        self.constraintHeightOfTheSheet.constant = self.heightOftheSheet
        self.alignView()
        
        self.slider.popUpViewCornerRadius = 20.0;
        self.slider.setMaxFractionDigitsDisplayed (0); //-- set slider not to display decimal value
        self.slider.popUpViewColor = UIColor.init(hue: 0.52, saturation: 0.90, brightness: 0.80, alpha: 0.8)
        self.slider.font = UIFont.init(name: kFontDefault, size: kFontSizeDefault)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // self.openAnimation()
    }
    
    //--MARK:- Diaplay font list in Tableview
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fonts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = self.tableViewFonts.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let fontName = self.fonts[indexPath.row]
        
        cell.textLabel?.text = fontName
        cell.textLabel?.textColor = .white
        cell.textLabel?.font = UIFont(name: fontName, size: 12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fontName: String = self.fonts[indexPath.row]
        newLogoVC.canvas.setFontName (fontName)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 18
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 18
    }
    
    //--MARK:- Diaplay clors and image gallery (for Tab3) list in Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == thumbCollectionView) { //-- tab3 thumbnail
            self.initializeThumbnailCollectionView()
            return self.numberOfItemsInSectionForThumbnail()
        }
        return self.colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 
        if (collectionView == thumbCollectionView) { //-- tab3 thumbnail
            return self.customizeCellForThumbnailView(collectionView: collectionView, indexPath: indexPath)
        }
        
        let color = self.colors[indexPath.row]
        let cell = self.collectionViewColor.dequeueReusableCell(withReuseIdentifier: color == self.color ? "ColorCell" : "SelectedColorCell", for: indexPath)
    
        if color == newLogoVC.canvas.textColor{
            var point: CGPoint = cell.frame.origin
            let xOffset = (abs(Constants.FontList.frameSizeForSelectedFont.width - Constants.FontList.frameSize.width))/2
            let yOffset = (abs(Constants.FontList.frameSizeForSelectedFont.height - Constants.FontList.frameSize.height)) / 2
            point.x = point.x - xOffset
            point.y = point.y - yOffset
            cell.frame.origin = point
            cell.frame.size = Constants.FontList.frameSizeForSelectedFont
        }else{
            cell.frame.size = Constants.FontList.frameSize
        }
        cell.backgroundColor = color
        
        return cell
    }
    
    //-- MARK: Set color, background color obects
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == thumbCollectionView) { //-- tab3 thumbnail
            return self.didSelectItemAt(indexPath: indexPath)
        }
        
        let color = self.colors[indexPath.row]
        self.color = color
        if (self.selectedTab == TAB_ONE) {
            self.newLogoVC.canvas.textColor = color
        }
        else if (self.selectedTab == TAB_TWO) {
            self.newLogoVC.canvas.svgFillColor = color
        }
        else if (self.selectedTab == TAB_THREE) {
            
        }
        self.collectionViewColor.reloadData()
    }
    
    private func alignView(){
        let width  = self.view.frame.width-16
        let positionX = self.view.center.x
        let positionY = self.viewPanel.frame.height/2
        
        switch self.sheetType {
            
        case .TEXTSPACING?, .OPACITY?, .SATURATION?, .BRIGHTNESS?, .BACKGROUNDCOLOR?:
            
            if self.sheetType == .TEXTSPACING {
//                self.slider.maximumValue = Float(kFontSpaceDefault)
                self.labelTitle.text = "Space"
                let currentValue: CGFloat = self.newLogoVC.canvas.textSpacing()
                slider.value = Float(currentValue * 2)
                
            }else if self.sheetType == .OPACITY {
                self.labelTitle.text = "Opacity"
                slider.value = Float(self.newLogoVC.canvas.textOpacity() * 100)
                
            }else if self.sheetType == .SATURATION  {
                self.labelTitle.text = "Saturation"
                if (self.selectedTab == TAB_ONE) {
                    let savedValue = Float(self.newLogoVC.canvas.textSaturation * 100)
                    slider.value = savedValue
                    if (savedValue < 0) {//-- value yet not saved
                        slider.value = 50 //-- set initial value
                    }
                }
                else if (self.selectedTab == TAB_TWO) {
                    let savedValue = Float(self.newLogoVC.canvas.svgSaturation() * 100)
                    slider.value = savedValue
                    if (savedValue < 0) {//-- value yet not saved
                        slider.value = 50 //-- set initial value
                    }
                }
            }
            else if self.sheetType == .BRIGHTNESS {
                self.labelTitle.text = "Brightness"
                if (self.selectedTab == TAB_ONE) {
                    let savedValue = Float(self.newLogoVC.canvas.textBrightness * 100)
                    slider.value = savedValue
                }
                else if (self.selectedTab == TAB_TWO) {
                    let savedValue = Float(self.newLogoVC.canvas.svgBrightness() * 100)
                    slider.value = savedValue
                }
                else if (self.selectedTab == TAB_THREE) {

                }
            }
            else if self.sheetType == .BACKGROUNDCOLOR {
                if self.selectedTab == TAB_THREE {
                    self.labelTitle.text = "Background Color"
                    let savedValue = Float(self.newLogoVC.canvas.canvasBackgroundColor * 100)
                    slider.value = savedValue
                }
            }
            
            self.viewSlider.isHidden = false
            self.viewPanel.addSubview(self.viewSlider)
            self.viewSlider.frame.size.width = width
            self.viewSlider.layer.position.x = positionX
            self.viewSlider.layer.position.y = positionY
            self.slider.setThumbImage(UIImage(named: "Slider Icon"), for: .normal)

            
        case .FONT?:
            self.labelTitle.text = "Fonts"
            
            self.tableViewFonts.isHidden = false
            self.tableViewFonts.delegate = self
            self.tableViewFonts.dataSource = self
            self.viewPanel.addSubview(self.tableViewFonts)
            
            self.tableViewFonts.frame.size.width = width+16
            self.tableViewFonts.frame.size.height = self.viewPanel.frame.height-self.labelTitle.frame.height*3
            self.tableViewFonts.layer.position.x = positionX
            self.tableViewFonts.layer.position.y = positionY
            self.fonts = UIFont.familyNames
            self.tableViewFonts.reloadData()
            
        case .COLOR?:
            self.labelTitle.text = "Colors"
            
            self.collectionViewColor.isHidden = false
            self.collectionViewColor.delegate = self
            self.collectionViewColor.dataSource = self
            
            self.viewPanel.addSubview(self.collectionViewColor)
            
            self.collectionViewColor.frame.size.width = width+16
            self.collectionViewColor.frame.size.height = self.viewPanel.frame.height-self.labelTitle.frame.height*3
            self.collectionViewColor.layer.position.x = positionX
            self.collectionViewColor.layer.position.y = positionY
            
        case .THUMBNAIL?:
            self.labelTitle.text = "Image Gallery"
            self.loadGalleryCategory()
            self.loadThumbnailAndImageFromGallery()
            self.initializeThumbnailCollectionView()
            
        default:
            break
        }
    }
    
    @IBAction func onSliderTouchUpInsideAndOutside(_ sender: UISlider) {
        performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        if self.selectedTab == TAB_ONE {
            performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
        else if self.selectedTab == TAB_TWO{
            performActionWithSliderValueForSVGTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
        else if self.selectedTab == TAB_THREE{
            performActionWithSliderValueForImageTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
    }
    
    @IBAction func onSliderValueChanged(_ sender: UISlider) {
        if self.selectedTab == TAB_ONE {
            performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
        else if self.selectedTab == TAB_TWO{
            performActionWithSliderValueForSVGTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
        else if self.selectedTab == TAB_THREE{
            performActionWithSliderValueForImageTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
    }
    
    //-- Tab1
    func performActionWithSliderValueForTexTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
        case .OPACITY?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setTextOpacity(abs(value * 0.01))
            }
            break
            
        case .TEXTSPACING?:
            if action == Constants.SliderAction.valueChanged {
                let newValue: CGFloat = value * 0.5
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setTextSpacing(newValue)
            }
            break
            
        case .SATURATION?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.textSaturation = (value * 0.01)
            }
            break
            
        case .BRIGHTNESS?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.textBrightness = (value * 0.01)
            }
        default:
            break
        }
    }
    
    //-- Tab2
    func performActionWithSliderValueForSVGTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
        case .COLOR?:
            if action == Constants.SliderAction.valueChanged {
//                self.newLogoVC.canvas.beginUndoGrouping()
            }
            break
            
        case .OPACITY?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setSvgOpacity(abs(value * 0.01))
            }
            break
            
        case .SATURATION?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setSvgSaturation(value * 0.01)
            }
            break
            
        case .BRIGHTNESS?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setSvgBrightness(value * 0.01)
            }
        default:
            break
        }
    }
    
    //-- Tab3
    func performActionWithSliderValueForImageTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
//        case .COLOR:
//            if action == Constants.SliderAction.valueChanged {
//                self.newLogoVC.canvas.beginUndoGrouping()
//                self.newLogoVC.canvas.setTextOpacity(abs(value * 0.01))
//            }
//            break
//
//        case .OPACITY:
//            if action == Constants.SliderAction.valueChanged {
//                self.newLogoVC.canvas.beginUndoGrouping()
//                self.newLogoVC.canvas.setTextOpacity(abs(value * 0.01))
//            }
//            break
//
//        case .SATURATION:
//            if action == Constants.SliderAction.valueChanged {
//                self.newLogoVC.canvas.textSaturation = (value * 0.01)
//            }
//            break
//
        case .BACKGROUNDCOLOR?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.canvasBackgroundColor = (value * 0.01)
            }
        default:
            break
        }
    }
    
    private func openAnimation(){
        UIView.animate(withDuration: 0.3) {
            self.newLogoVC.buttonTextCollectionView?.alpha = 0
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any?) {
        
        UIView.animate(withDuration: 0.9, animations: {
           // self.newLogoVC.textCollectionView.alpha = 1.0
            self.newLogoVC.deselectAllItemsInCollectionView()
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }) { (bool) in
           
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let popupViewController2 = segue.destination as? PopUpViewController2 {
            popupViewController2.selectedTab = self.selectedTab
        }
        
        if let vc = segue.destination as? PopUpViewController2 {
            vc.newLogoVC = self.newLogoVC
            vc.sheetType = BottomSheetType.BACKGROUNDCOLOR
            vc.heightOftheSheet = self.heightOftheSheet
        }
    }
    
    @IBAction func unwindFromPopupViewController2(segue: UIStoryboardSegue) {
        self.view.isHidden = true
        self.actionDismiss(nil)
    }
    
}

//-- MARK: Tab3 - Big Thumb diplay in collection to display image gallery
extension PopUpViewController {
    
    private func numberOfItemsInSectionForThumbnail() -> Int {
        
        return galleryCategory.count;
    }
    
    private func customizeCellForThumbnailView(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ThumbnailViewCell = collectionView.dequeueReusableCell (withReuseIdentifier: "ThumbnailViewCell",
                                                                         for: indexPath) as! ThumbnailViewCell
        let category = self.galleryCategory[indexPath.row]
        let imageGallery: Constants.ImageGallery = self.ImageGalleryFro(category: category)!
        
        print(imageGallery.category.lowercased())
        if imageGallery.category.lowercased() == "color" || imageGallery.category.lowercased() == "clear" {
            cell.thumbLabel.text = imageGallery.category
        }
        else if imageGallery.isFree {
            cell.thumbLabel.text = GALLERY_FOLDER_INITIAL_FOR_FREE_IMAGE
        }
        else {
            cell.thumbLabel.text = imageGallery.category
        }
        
        let galleryPath = Utility.galleryPathFor(category)
        let thumbRelativePath = Utility.relativePathFor(file: imageGallery.thumb, type: GALLERY_THUMBNAIL_FILE_EXTN, directory: galleryPath)
        let thumbImage = UIImage(contentsOfFile: thumbRelativePath)
        cell.thumbImage?.image = thumbImage
        
        //-- set label font size
        let delta: CGFloat = cell.thumbImage!.frame.height / 12   //7.20
        var fontSize: CGFloat  = cell.thumbImage!.frame.height / delta
        if fontSize > kFontSizeMax {
            fontSize = kFontSizeMax
        }
        self.labelTitle.font = self.labelTitle.font.withSize(fontSize)
        
        cell.borderderWidth = 3
        cell.cornerRadius = 6
        return cell
    }
    
    private func didSelectItemAt(indexPath: IndexPath) {
        let category = self.galleryCategory[indexPath.row]
        let imageGallery: Constants.ImageGallery = self.ImageGalleryFro(category: category)!
        
        if imageGallery.category.lowercased() == "clear" {
            self.newLogoVC.makeCanvasBackgroundClear()
        }
        else if imageGallery.category.lowercased() == "color" {
            self.newLogoVC.bottomSheetType = .BACKGROUNDCOLOR
            self.selectedTab = TAB_THREE
            self.performSegue(withIdentifier: "PopUpViewController2", sender: self)
            self.newLogoVC.removeCanvasImage()
        }
        else {
            self.newLogoVC.setCanvasImageWith(name: imageGallery.images[0], gallery: imageGallery.category)
        }
    }
    
    private func initializeThumbnailCollectionView() {
        self.thumbCollectionView.isHidden = false
        self.thumbCollectionView.delegate = self
        self.thumbCollectionView.dataSource = self
        self.viewPanel.addSubview(self.thumbCollectionView)
        
        let padding: CGFloat = (self.viewPanel.frame.height * 20) / 100
        self.thumbCollectionView.frame.size.width = self.viewPanel.frame.width - padding
        self.thumbCollectionView.frame.size.height = self.viewPanel.frame.height - padding
        self.thumbCollectionView.layer.position.x = self.viewPanel.center.x //+ padding * 0.5
        self.thumbCollectionView.frame.origin.y = self.viewPanel.bounds.origin.y + (padding * 0.90)
    }
    
    private func loadGalleryCategory() {
        let ignoreString = "Menu"
        self.galleryCategory.removeAll()
        
        //-- load content a sorted array of categories.
        self.galleryCategory = Utility.contentFrom(path: GALLERY_CATEGORY_PATH, byDeletingExtn: true,
                                              ignoreStringSeparatedByComma: ignoreString)
    }
    
    private func loadThumbnailAndImageFromGallery(){
        self.imageAndThumbnailFromGallery.removeAll()
        
        for category:String in self.galleryCategory {
            let ignoreString: String = "thumb"
            let galleryPathForThumbnail = Utility.galleryPathFor(category)
            var imageGallery = Constants.ImageGallery()
            imageGallery.category = category
            //-- imageGallery.thumb //-- default = 'thumb' is already set
            imageGallery.images = Utility.contentFrom(path: galleryPathForThumbnail, byDeletingExtn: true, ignoreStringSeparatedByComma: ignoreString)
            
            //-- MARK: Check which gallery (floder/directory) is assigned for free images.
            /* Any folder inside Resource/Gallery, has 'Free or free' as suffix (i.e first 4 chars = 'Free')
               in its name, is treated as the directory image to be used freely or no puchase is required. */
            
            if category.lowercased().prefix(4) == GALLERY_FOLDER_INITIAL_FOR_FREE_IMAGE.lowercased() {
                imageGallery.isFree = true
            }
            else {
                imageGallery.isFree = false
            }
            self.imageAndThumbnailFromGallery.append(imageGallery)
        }
        
        //--MARK: Re-arrange 'Clear' and "Color' Catories and Galleries(thumnails) to first and second pos (resp) in the array
        if let index:Int = self.galleryCategory.index(of: "Clear") {
            let clearGallery: Constants.ImageGallery = self.imageAndThumbnailFromGallery.remove(at: index)
            self.imageAndThumbnailFromGallery.insert(clearGallery, at: 0)
            
            let clearCategory: String = self.galleryCategory.remove(at: index)
            self.galleryCategory.insert(clearCategory, at: 0)
        }
        if let index:Int = self.galleryCategory.index(of: "Color") {
            let colorGallery: Constants.ImageGallery = self.imageAndThumbnailFromGallery.remove(at: index)
            self.imageAndThumbnailFromGallery.insert(colorGallery, at: 1)
            
            let colorCategory: String = self.galleryCategory.remove(at: index)
            self.galleryCategory.insert(colorCategory, at: 1)
        }
    }
    
    private func ImageGalleryFro(category: String) -> Constants.ImageGallery? {
        guard imageAndThumbnailFromGallery.count > 0 else {
            return nil
        }
        if let index:Int = galleryCategory.index(of: category) {
            return self.imageAndThumbnailFromGallery[index]
        }
        return nil
    }
}












