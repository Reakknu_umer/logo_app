//
//  NewLogoViewController.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 21/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

let TAB_ONE: Int = 1
let TAB_TWO: Int = 2
let TAB_THREE: Int = 3

class NewLogoViewController: MenuViewController, MenuViewContriollerDelegate, UIImagePickerControllerDelegate,
UINavigationControllerDelegate, BVCanvasDelegate{

    @IBOutlet weak var canvas: BVCanvas!
    @IBOutlet weak var viewBottomContainer: UIView!
    @IBOutlet weak var zlayerTableView: UITableView!
    @IBOutlet weak var contraintHeightOfCollectionView: NSLayoutConstraint!

    @IBOutlet var tabButtons: [UIButton]!
    private var currentlyEditingLabel:BVLabelView!
    private var currentlyEditingSvg:BVSvgView!
    
    public var objects = [UIView]()
    private var selectedTab: Int = TAB_ONE
    public var bottomSheetType:BottomSheetType = .OPACITY
    
    private var imagePicker:UIImagePickerController?
    private var idealHeightOfCollectionView:CGFloat  = 0
    
    
    public var selectedSvgThumb: String = ""
    public var selectedSvgCategory: String = ""
    
    fileprivate var snapshot: UIView?
    fileprivate var sourceIndexPath: IndexPath?
    fileprivate var customeViews = [UIView]()
    
    var selectedLogoModel : LogoModel!
    
    
//    private var lmLogo: LMLogo?
//    private var lmDocument: LMDocument?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self
        self.idealHeightOfCollectionView = self.contraintHeightOfCollectionView.constant
        self.onTab1ButtonTapped(tabButtons[0])
        
//        lmDocument = self.ArchiveData()
//        lmDocument?.close(completionHandler: nil)
        setupLogoModel()
        self.addZlayerGestureRecognizer()
    }
    
    func setupLogoModel() {
        self.view.layoutIfNeeded()
        if selectedSvgThumb.count > 0 {
            print("selected svg thumbnail is \(selectedSvgThumb) from category \(selectedSvgCategory)")
            selectedSvgThumb = Utility.addExtensionTo(file: selectedSvgThumb, fileExtension: "@3x")
            self.createNewSvgView(selectedSvgThumb, category: selectedSvgCategory)
            //selectedSvgThumb = ""
            //selectedSvgCategory = ""
        }
        if selectedLogoModel != nil {
            print("canvas.frame = \(canvas.frame)")
            
            let scale = canvas.frame.size.width / CGFloat(selectedLogoModel.width!)
            
            for layer in selectedLogoModel.layers! {
                if layer.type == "image" {
                    self.makeCanvasBackgroundClear()
                    setCanvasImage(UIImage(named: layer.name)!)
                }
                if layer.type == "svg" {
                    let path = Bundle.main.path(forResource: layer.name, ofType: "svg")!
                    canvas.delegate = self
                    canvas.addSvgView(path, andFrame: CGRect(x: (CGFloat(layer.x) * scale), y: (CGFloat(layer.y) * scale), width: (CGFloat(layer.width) * scale), height: (CGFloat(layer.height) * scale)))
                    print("svg = \(CGRect(x: (CGFloat(layer.x) * scale), y: (CGFloat(layer.y) * scale), width: (CGFloat(layer.width) * scale), height: (CGFloat(layer.height) * scale)))")

                }
                if layer.type == "text" {
                    print("scale = \(scale))")
                    
                    canvas.addLabel()
                    canvas.setTextOpacity(1.0)
                    canvas.currentlyEditingLabel.border?.strokeColor = UIColor.red.cgColor
                    canvas.setFontName(layer.name!)
                    canvas.textColor = UIColor(hexString: layer.color!)
                    //canvas.textColor = .orange
                    canvas.currentlyEditingLabel.labelTextView.text = layer.value!
                    canvas.currentlyEditingLabel.frame = CGRect(x: (CGFloat(layer.x) * scale), y: (CGFloat(layer.y) * scale), width: (CGFloat(layer.width) * scale), height: (CGFloat(layer.height) * scale))
                    canvas.fontSize = (CGFloat(layer.font_size) * scale)
                    canvas.currentlyEditingLabel.labelTextView.textAlignment = .center
                    print("text = \(CGRect(x: (CGFloat(layer.x) * scale), y: (CGFloat(layer.y) * scale), width: (CGFloat(layer.width) * scale), height: (CGFloat(layer.height) * scale)))")
                    print("text AFTER = \(canvas.currentlyEditingLabel.frame)")

                }
            }
            canvas.currentlyEditingLabel.hideEditingHandles()
            
        }
        selectedLogoModel = nil
    }

    override func menuFrame()-> CGRect {
        return canvas.frame
    }
    
    override func currentTab()-> Int {
        print("selected tab: \(self.selectedTab)")
        return self.selectedTab
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let row = indexPath.row
        if self.selectedTab == TAB_ONE{
            self.actionTab1(row)
        }else if self.selectedTab == TAB_TWO{
            self.actionTab2(row)
        }else{ //--tab 3
            self.actionTab3(row)
        }
    }
    
    @IBAction func onTab1ButtonTapped(_ sender: UIButton) {
        self.selectedTab = TAB_ONE
        self.makeCurrentButtonSelected(sender)
        self.reloadButtonTextCollectionViewForTab(sender.tag)
        self.bottomCollectionViewLayoutForTabs()
    }

    @IBAction func onTab2ButtonTapped(_ sender: UIButton) {
        self.selectedTab = TAB_TWO
        self.makeCurrentButtonSelected(sender)
        self.reloadButtonTextCollectionViewForTab(sender.tag)
        self.bottomCollectionViewLayoutForTabs()
    }

    @IBAction func onTab3ButtonTapped(_ sender: UIButton) {
        self.selectedTab = TAB_THREE
        self.makeCurrentButtonSelected(sender)
        self.reloadButtonTextCollectionViewForTab(sender.tag)
        self.bottomCollectionViewLayoutForTab3()
    }
    
    private func makeCurrentButtonSelected(_ currentButton: UIButton) {
    
        guard !currentButton.isSelected else {
            return
        }
        self.tabButtons.forEach { (button) in
            button.isSelected = false
        }
        currentButton.isSelected = true
    }
    
    private func reloadButtonTextCollectionViewForTab(_ tab: Int) {
        UIView.transition(with:self.buttonTextCollectionView!, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.contraintHeightOfCollectionView.constant = tab == TAB_THREE
                ? self.viewBottomContainer.frame.height-16 : self.idealHeightOfCollectionView
            self.buttonTextCollectionView?.reloadData()
        })
    }
    
    private func bottomCollectionViewLayoutForTabs() {
        let cellSize = CGSize(width:60 , height:60)
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1.0, bottom: 1, right: 1.0)
        layout.minimumLineSpacing = 4
        layout.minimumInteritemSpacing = 8
        self.buttonTextCollectionView?.isScrollEnabled = true
        self.buttonTextCollectionView?.setCollectionViewLayout(layout, animated: false)
    }
    
    private func bottomCollectionViewLayoutForTab3() {
        let width = (self.viewBottomContainer.frame.width/3) - 20
        let offset:CGFloat = width/6
        let cellSize = CGSize(width:width - offset, height:width)
        let layout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1.0, left: 25.0, bottom: 1.0, right: 1.0)
        layout.minimumLineSpacing = 25
        layout.minimumInteritemSpacing = 30
        self.buttonTextCollectionView?.isScrollEnabled = true
        self.buttonTextCollectionView?.setCollectionViewLayout(layout, animated: false)
    }
    
    public func deselectAllItemsInCollectionView(){
        self.buttonTextCollectionView?.selectItem(at: nil, animated: true, scrollPosition: [])
    }
    
    private func askCameraPermission(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) == true{
            if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                self.openCameraPicker()
            } else if AVCaptureDevice.authorizationStatus(for:AVMediaType.video) ==  AVAuthorizationStatus.denied{
                self.showCameraPermissionDialog()
            }else {
                AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                    if granted == true {
                        self.openCameraPicker()
                    } else {
                        self.showCameraPermissionDialog()
                    }
                })
            }
        }else{
            self.alert("Your device does not have camera!")
        }
    }
    
    // MARK: -  For Photos
    private func checkPhotoLibraryPermission() {
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            self.openPhotoPicker()
            break
            
        case .denied, .restricted :
            self.showPhotoPermissionDialog()
            break
            
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                switch status {
                case .authorized:
                    self.openPhotoPicker()
                    break
                // as above
                case .denied, .restricted:
                    self.showPhotoPermissionDialog()
                    break
                // as above
                case .notDetermined:
                    self.alert("Unexpected error occured for accessing photo library")
                    break
                }
            }
        }
    }
    
    private func alert(_ message:String){
        let controller = UIAlertController.init(title: "Choose Image", message: message, preferredStyle: .alert)
        let actionCancel = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        controller.addAction(actionCancel)
        self.present(controller, animated: true, completion: nil)
    }
    
    private func showPhotoPermissionDialog(){
        self.alert("Go to settings and grant permission for photos access")
    }
    
    private func openPhotoPicker(){
        self.imagePicker?.sourceType = .photoLibrary
        self.imagePicker?.allowsEditing = true
        self.present(self.imagePicker!, animated: true, completion: nil)
    }
    
    private func openCameraPicker(){
        self.imagePicker?.sourceType = .camera
        self.imagePicker?.allowsEditing = true
        self.present(self.imagePicker!, animated: true, completion: nil)
    }
    
    private func showCameraPermissionDialog(){
        self.alert("Go to settings and grant permission for camera access")
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //-- MARK: Callback after images chosesen from app lirary or camera
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        self.setCanvasImage(image)
//        lmLogo?.backgroundImage = image
        
//        print(image.size)
    }
    
    private func actionTab1(_ row:Int){
        if row == 0{
            self.createNewText()
            return
        }
        
        switch row {
        case 1:
            self.bottomSheetType = .FONT
        case 2:
            self.bottomSheetType = .OPACITY
            break
        case 3:
            self.bottomSheetType = .COLOR
        case 4:
            self.bottomSheetType = .TEXTSPACING
        case 5:
            self.bottomSheetType = .SATURATION
        case 6:
            self.bottomSheetType = .BRIGHTNESS
        default:
            break
        }
        
        if self.currentlyEditingLabel != nil {
            currentlyEditingLabel.showEditingHandles()
            currentlyEditingLabel.becomeFirstResponder //puts cursor on text field
        }
        self.performSegue(withIdentifier: "PopUpViewController", sender: self)
    }
    
    private func actionTab2(_ row:Int){
        if row == 0{
            self.performSegue(withIdentifier: "SymbolsViewController", sender: self)
            return
        }
        else if row == 1{
            self.bottomSheetType = .SATURATION
        }
        else if row == 2{
            self.bottomSheetType = .OPACITY
        }
        else if row == 3{
            self.bottomSheetType = .COLOR
        }
        else if row == 4{
            self.bottomSheetType = .BRIGHTNESS
        }
        
        self.performSegue(withIdentifier: "PopUpViewController", sender: self)
    }
    
    internal func actionTab3(_ row:Int){
        if row == TAB3_CAMERA{
            self.askCameraPermission()
            return
        }
        
        if row == TAB3_MOBILEGALLERY{
            self.checkPhotoLibraryPermission()
            return
        }

        if row == TAB3_COLOR{
            self.bottomSheetType = .BACKGROUNDCOLOR
            self.selectedTab = TAB_THREE
            self.performSegue(withIdentifier: "PopUpViewController", sender: self)
            return
        }
        
        if row == TAB3_APPGALLERY{
            self.selectedTab = TAB_THREE
            self.bottomSheetType = .THUMBNAIL
            self.performSegue(withIdentifier: "PopUpViewController", sender: self)
        }
    }
    
    internal func removeCanvasImage() {
        self.canvas.image = nil
    }
    internal func makeCanvasBackgroundClear() {
        self.removeCanvasImage()
        self.canvas.backgroundColor = CANVAS_DEFAULT_COLOR
        //--TODO: Or set backgeround pattern for basic users
    }
    
    internal func setCanvasImage(_ image: UIImage) {
        self.makeCanvasBackgroundClear()
        self.canvas.contentMode = .scaleAspectFill
        self.canvas.image = image
    }
    
    internal func setCanvasImageWith(name: String, gallery: String) {
        
        let imageGalleryPath = Utility.galleryPathFor(gallery)
        let imageRelativePath = Utility.relativePathFor(file: name, type: GALLERY_THUMBNAIL_FILE_EXTN, directory: imageGalleryPath)
        guard let backgroundImage: UIImage = UIImage(named: imageRelativePath) else {
            return
        }
        self.makeCanvasBackgroundClear()
        setCanvasImage(backgroundImage)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if self.selectedTab == TAB_TWO {
            if let popupViewController = segue.destination as? PopUpViewController {
                popupViewController.selectedTab = self.selectedTab
            }
//        }
        
        if let vc = segue.destination as? PopUpViewController {
            vc.newLogoVC = self
            vc.sheetType = self.bottomSheetType
            vc.heightOftheSheet = self.viewBottomContainer.frame.height
            //            popUpViewController = vc
        }
    }
}

extension NewLogoViewController {
    //-- MARK: MenuViewController Delegates
    func onMenuButtonTapped(_ index: Int) {
        switch index {
        case Constants.MenuButtonIndex.undo:
            self.canvas.undo()
            break
        case Constants.MenuButtonIndex.redo:
            self.canvas.redo()
            break
        case Constants.MenuButtonIndex.angle:
            currentlyEditingLabel.bounds = CGRect(x: 0, y: 0, width: 200, height: 100)
            break
        case Constants.MenuButtonIndex.copy:
            break
        case Constants.MenuButtonIndex.layer:
            self.zlayerTableView.isHidden = !self.zlayerTableView.isHidden
            if (self.zlayerTableView.isHidden == false) {
                self.zlayerTableView.alpha = 0.6
                self.canvas.bringSubview(toFront: self.zlayerTableView)
                self.customeViews = self.getSubviewsOfView(view: self.view)
                self.zlayerTableView.delegate = self
                self.zlayerTableView.dataSource = self
                self.zlayerTableView.reloadData()
            }
            break
        default:
            break
        }
    }

    private func createNewText() {
        canvas.delegate = self
        canvas.addLabel()
        canvas.setTextOpacity(1.0)
        canvas.currentlyEditingLabel.border?.strokeColor = UIColor.red.cgColor
        canvas.setFontName("HelveticaNeue")
//        canvas.fontSize = 50
    }
    
    private func createNewSvgView(_ svgFile: String, category: String) {
        let symbolPathForSvg = Utility.symbolPathFor(category, fileType: Constants.SymbolType.SVG)
        var svgWithRelativePath = Utility.relativePathFor(file: svgFile, type: SYMBOL_SVG_FILE_EXTN, directory: symbolPathForSvg)
        svgWithRelativePath = Utility.addExtensionTo (file: svgWithRelativePath, fileExtension: SYMBOL_SVG_FILE_EXTN)
        
        canvas.delegate = self
        canvas.addSvgView(svgWithRelativePath)
    }
    
    //--MARK: Call back (unwind segue) from SymbolsViewController
    @IBAction func unwindToPreviousViewController(segue: UIStoryboardSegue) {
        if let sourceViewController = segue.source as? SymbolsViewController {
            let selectedCategory = sourceViewController.selectedSvgCategory
            var selectedSvgThumb = sourceViewController.selectedSvgThumb
            guard selectedSvgThumb.count > 0 || selectedCategory.count > 0 else {
                return
            }
            
            print("selected svg thumbnail is \(selectedSvgThumb) from category \(selectedCategory)")
            
            //--FIXME: Its a temporary fix due to avg files have @3x suffixes. TODO: Remove @3x from all svg files.
            selectedSvgThumb = Utility.addExtensionTo(file: selectedSvgThumb, fileExtension: "@3x")
            
            self.createNewSvgView(selectedSvgThumb, category: selectedCategory)
        }
    }
    
    @IBAction func unwindFromImageGallery(segue: UIStoryboardSegue) {
        if let sourceViewController = segue.source as? ImageGalleryViewController {
            self.setCanvasImageWith(name: sourceViewController.selectedFromImageGallery, gallery: sourceViewController.selectedGallery)
        }
    }
    
//    //-- MARK: BVCanvas Delegates
//    public func svgViewDidClose(_ label: BVSvgView) {
//        print("Delegate called for SVG IS CLOSED")
//    }
//    public func svgViewDidSelected(_ label: BVSvgView) {
//        currentlyEditingSvg = label
//        self.onTab2ButtonTapped(self.tabButtons[1])
//        print("Delegate called for SVG IS SELECTED \(label.frame)")
//    }
//
//    public func svgViewDidShowEditingHandles(_ label: BVSvgView) {
//        print("Delegate called for svgViewDidShowEditingHandles")
//    }
//
//    public func svgViewDidHideEditingHandles(_ label: BVSvgView) {
//        print("Delegate called for svgViewDidHideEditingHandles")
//    }
    
    public func svgViewDidBeginEditing(_ label: BVSvgView) {    //-- begin moving or rotating
        print("Delegate called for svg BECOME FIRST RESPONDER")
    }
    
    
    public func svgViewDidEndEditing(_ label: BVSvgView) {
        currentlyEditingSvg = label
        print("Delegate called for svgViewDidEndEditing")
    }
    
    public func svgViewDidBeginMoving(_ label: BVSvgView) {
        currentlyEditingSvg = label
//        print("Delegate called for svg IS MOVING OR ROTATING")
    }
    
//    public func svgViewDidMoving(_ label: BVSvgView) {
//        print("Delegate called for svg IS STILL MOVING OR ROTATING")
//    }
    
    
    
    
    
    
    public func labelViewDidClose(_ label: BVLabelView) {
        print("Delegate called for LABEL IS CLOSED")
    }
    
    public func labelViewDidShowEditingHandles(_ label: BVLabelView) {
        print("Delegate called for labelViewDidShowEditingHandles")
    }
    
    public func labelViewDidHideEditingHandles(_ label: BVLabelView) {
        print("Delegate called for labelViewDidHideEditingHandles")
    }
    
    public func labelViewDidBeginEditing(_ label: BVLabelView) {    //-- begin moving or rotating
        print("Delegate called for LABEL BECOME FIRST RESPONDER")
    }
    
    
    public func labelViewDidEndEditing(_ label: BVLabelView) {
        currentlyEditingLabel = label
        print("Delegate called for labelViewDidEndEditing")
    }
    
    public func labelViewDidBeginMoving(_ label: BVLabelView) {
        currentlyEditingLabel = label
//        print("Delegate called for LABEL IS MOVING OR ROTATING")
    }
    
    public func labelViewDidMoving(_ label: BVLabelView) {
//        print("Delegate called for LABEL IS STILL MOVING OR ROTATING")
    }
    
    public func labelViewDidSelected(_ label: BVLabelView) {
        
        currentlyEditingLabel = label
        self.onTab1ButtonTapped(self.tabButtons[0])
        print("Delegate called in NewLogoVC for LABEL IS SELECTED")
    }
    
//-- MARK: Data saving
//    private func ArchiveData () -> LMDocument {
//        lmLogo = LMLogo()
//        lmLogo!.backgroundImage = canvas.image
//        let name:String = DataManager.sharedInstance().uniqueFilename()
//        var backgroundImage: UIImage? = nil
//        if self.canvas.image != nil {
//            backgroundImage = self.canvas.image!
//        }
//        let document: LMDocument = DataManager.sharedInstance().archiveLogo(lmLogo, withName: name, backgroundImage:backgroundImage, closeAfterSaving: false)
//        return document
//    }
}


//extension NewLogoViewController {
//
//    //-- MARK: MenuViewController Delegates
//    func onMenuButtonTapped(_ index: Int) {
//        switch index {
//        case Constants.MenuButtonIndex.undo:
//            self.canvas.undo()
//            break
//        case Constants.MenuButtonIndex.redo:
//            self.canvas.redo()
//            break
//        case Constants.MenuButtonIndex.angle:
//            break
//        case Constants.MenuButtonIndex.copy:
//            break
//        case Constants.MenuButtonIndex.layer:
//            self.zlayerTableView.isHidden = !self.zlayerTableView.isHidden
//            if (self.zlayerTableView.isHidden == false) {
//                self.zlayerTableView.alpha = 0.6
//                self.canvas.bringSubview(toFront: self.zlayerTableView)
//                self.customeViews = self.getSubviewsOfView(view: self.view)
//                self.zlayerTableView.delegate = self
//                self.zlayerTableView.dataSource = self
//                self.zlayerTableView.reloadData()
//            }
//            break
//        default:
//            break
//        }
//    }
//
//}
    //-- MARK: Gesture Recognizer and other methods for Zlayer
extension NewLogoViewController {
    
    func addZlayerGestureRecognizer(){
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressGestureRecognized(longPress:)))
        self.zlayerTableView.addGestureRecognizer(longPress)
    }
    
    @objc func longPressGestureRecognized(longPress: UILongPressGestureRecognizer) {
        let state = longPress.state
        let location = longPress.location(in: self.zlayerTableView)
        guard let indexPath = self.zlayerTableView.indexPathForRow(at: location) else {
            self.cleanup()
            return
        }

        switch state {
        case .began:
            sourceIndexPath = indexPath
            guard let cell = self.zlayerTableView.cellForRow(at: indexPath) else { return }
            snapshot = self.customSnapshotFromView(inputView: cell)
            guard  let snapshot = self.snapshot else { return }
            
            var center = cell.center
            snapshot.center = center
            snapshot.alpha = 0.0
            self.zlayerTableView.addSubview(snapshot)
            
            let correspondingView:UIView? = customeViews[indexPath.row]
            if correspondingView != nil {
                self.arrangeZLeve(forceThisViewToSelect: correspondingView)
            }
            
            UIView.animate(withDuration: 0.25, animations: {
                center.y = location.y
                snapshot.center = center
                snapshot.transform = CGAffineTransform(scaleX: 1.01, y: 1.01)
                snapshot.alpha = 0.98
                cell.alpha = 0.0
            }, completion: { (finished) in
                cell.isHidden = true
            })
            break
        case .changed:
            guard  let snapshot = self.snapshot else { return }
            var center = snapshot.center
            center.y = location.y
            snapshot.center = center
            guard let sourceIndexPath = self.sourceIndexPath  else { return }
            
            if indexPath != sourceIndexPath {
//                customeViews.swapAt(indexPath.row, sourceIndexPath.row)
//                swap(&customeViews[indexPath.row], &customeViews[sourceIndexPath.row])
//                self.arrangeZLeve(destinationIndexPath:indexPath, sourceIndexPath: sourceIndexPath)
                
                let movedObject = self.customeViews[sourceIndexPath.row]
                customeViews.remove(at: sourceIndexPath.row)
                customeViews.insert(movedObject, at: indexPath.row)
                self.arrangeZLeve()
//                print("\(sourceIndexPath.row) => \(indexPath.row)")
                self.zlayerTableView.moveRow(at: sourceIndexPath, to: indexPath)
                self.sourceIndexPath = indexPath
            }
            break
        default:
            guard let cell = self.zlayerTableView.cellForRow(at: indexPath) else { return }
            guard  let snapshot = self.snapshot else { return }
            cell.isHidden = false
            cell.alpha = 0.0
            
            self.arrangeZLeve()
            
            UIView.animate(withDuration: 0.25, animations: {
                snapshot.center = cell.center
                snapshot.transform = CGAffineTransform.identity
                snapshot.alpha = 0
                cell.alpha = 1
            }, completion: { (finished) in
                self.cleanup()
            })
        }
    }
    
    private func cleanup() {
        self.sourceIndexPath = nil
        snapshot?.removeFromSuperview()
        self.snapshot = nil
        self.zlayerTableView.reloadData()
    }
    
    private func customSnapshotFromView(inputView: UIView) -> UIView? {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0)
        if let CurrentContext = UIGraphicsGetCurrentContext() {
            inputView.layer.render(in: CurrentContext)
        }
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        UIGraphicsEndImageContext()
        let snapshot = UIImageView(image: image)
        snapshot.layer.masksToBounds = false
        snapshot.layer.cornerRadius = 10
        snapshot.layer.shadowOffset = CGSize(width: -2, height: -0)
        snapshot.layer.shadowRadius = 0
        snapshot.layer.shadowOpacity = 0.2
        snapshot.layer.borderColor = UIColor.red.cgColor
        snapshot.layer.borderWidth = 1
        return snapshot
    }
    
    private func customThumbnailFrom(view: UIView) -> UIImage? {
        let hadler: Bool = !view.isHandlerHidden
        if hadler {
            view.hideHandler()
        }
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0)
        if let CurrentContext = UIGraphicsGetCurrentContext() {
            view.layer.render(in: CurrentContext)
        }
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            UIGraphicsEndImageContext()
            return nil
        }
        UIGraphicsEndImageContext()
        let snapshot = UIImageView(image: image)
//        snapshot.layer.masksToBounds = false
        snapshot.backgroundColor = .clear
        snapshot.layer.cornerRadius = 0
        snapshot.layer.borderColor = UIColor.red.cgColor
        snapshot.layer.borderWidth = 4
        
        if hadler {
            view.showHandler()
        }
        return snapshot.image
    }

    func getSubviewsOfView<T: UIView>(view: UIView) -> [T] {
        var subviewArray = [T]()
        if self.canvas.subviews.count == 0 {
            return subviewArray
        }
        for subview in self.canvas.subviews {
//            subviewArray += self.getSubviewsOfView(view: subview) as [T]
            if let subview = subview as? T {
                if (subview is BVLabelView || subview is BVSvgView) {
                    subviewArray.append(subview)
                }
            }
        }
        subviewArray.reverse()
        return subviewArray
    }
    
    func arrangeZLeve(forceThisViewToSelect: UIView? = nil) {
        if customeViews.count == 0 { return }
        
        let sourceView: UIView
        var bringSubviewToFront: Bool = true
        
        if (forceThisViewToSelect != nil) {
            bringSubviewToFront = false
            sourceView = forceThisViewToSelect!
        }
        else {
            sourceView = customeViews[0]
        }
        
        for view in customeViews.reversed() {
            view.makeViewResignFirstResponder()
        }
        sourceView.makeViewBecomeFirstResponder(bringSubviewToFront: bringSubviewToFront)
        
        //-- assure layers view is alway on top of canvas
        if self.zlayerTableView.isHidden == false {
            self.canvas.bringSubview(toFront: self.zlayerTableView)
        }
    }
    
    func addBorderTo(cell: UITableViewCell) {
        cell.backgroundColor = .clear
        cell.layer.cornerRadius = 8
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.black.cgColor
    }
}

extension UIView {
    
    func makeViewBecomeFirstResponder(bringSubviewToFront: Bool = true) {
        if self is BVLabelView {
            let thisView: BVLabelView = self as! BVLabelView
            thisView.showEditingHandles()
            thisView.becomeFirstResponder()
            if bringSubviewToFront {
                self.superview?.bringSubview(toFront: thisView)
            }
        }
        else if self is BVSvgView {
            let thisView: BVSvgView = self as! BVSvgView
            thisView.showEditingHandles()
            thisView.becomeFirstResponder()
            if bringSubviewToFront {
                self.superview?.bringSubview(toFront: thisView)
            }
        }
    }
    
    func makeViewResignFirstResponder() {
        if self is BVLabelView {
            let thisView: BVLabelView = self as! BVLabelView
            thisView.hideEditingHandles()
            thisView.resignFirstResponder()
        }
        else if self is BVSvgView {
            let thisView: BVSvgView = self as! BVSvgView
            thisView.hideEditingHandles()
            thisView.resignFirstResponder()
        }
    }
    
    var isHandlerHidden: Bool {
        if self is BVLabelView {
            let thisView: BVLabelView = self as! BVLabelView
            return thisView.isEditingHandlerHidden
        }
        else if self is BVSvgView {
            let thisView: BVSvgView = self as! BVSvgView
            return thisView.isEditingHandlerHidden
        }
        return true
    }
    
    func hideHandler() {
        if self is BVLabelView {
            let thisView: BVLabelView = self as! BVLabelView
            thisView.hideEditingHandles()
        }
        else if self is BVSvgView {
            let thisView: BVSvgView = self as! BVSvgView
            thisView.hideEditingHandles()
        }
    }
    
    func showHandler() {
        if self is BVLabelView {
            let thisView: BVLabelView = self as! BVLabelView
            thisView.showEditingHandles()
        }
        else if self is BVSvgView {
            let thisView: BVSvgView = self as! BVSvgView
            thisView.showEditingHandles()
        }
    }
}


extension NewLogoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customeViews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ZLayerCell", for: indexPath) as! ZLayerCell
        let tappedView = self.customeViews[indexPath.row]
        let snapshot = self.customThumbnailFrom(view: tappedView)
        cell.thumbnail.image = snapshot
        self.addBorderTo(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tappedView = self.customeViews[indexPath.row]
        self.arrangeZLeve(forceThisViewToSelect: tappedView)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let tappedView = self.customeViews[indexPath.row]
    }
}












