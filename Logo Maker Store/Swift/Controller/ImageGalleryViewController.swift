//
//  ImageGalleryViewController.swift
//  Logo Maker Store
//
//  Created by Md Shafi Ahmed on 02/09/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

//-- MARK: Tab 3 image display
class ImageGalleryViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var thumbnailCollectionView: UICollectionView!
    
    private var galleryCategory = [String] ()
    private var thumbnailsForCategory = [String] ()
    private var selectedType:Int  = 0
    public var selectedFromImageGallery: String = ""
    public var selectedGallery: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ignoreString = "Menu"
        galleryCategory = Utility.contentFrom(path: GALLERY_CATEGORY_PATH, byDeletingExtn: true, ignoreStringSeparatedByComma: ignoreString)
        galleryCategory.sort()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoryCollectionView{ //-- load only symbol categories
            return self.galleryCategory.count
        }
        else{
            let category: String = self.galleryCategory[self.selectedType]
            self.loadThumbnailsFor(category: category)
            return self.thumbnailsForCategory.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoryCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryViewCell", for: indexPath) as! CategoryViewCell
            
            let category = self.galleryCategory[indexPath.row]
            cell.mainTitle.text = category
            let categoryThumbFileName = GALLERY_THUMBNAIL_FILE_NAME
            let pathForGalleryThumbnail = Utility.galleryPathFor(category)
            let imageWithRelativePath = Utility.relativePathFor(file: categoryThumbFileName,
                                                                type: GALLERY_THUMBNAIL_FILE_EXTN, directory: pathForGalleryThumbnail)
        
            let image = UIImage(contentsOfFile: imageWithRelativePath)
            cell.mainImage.image = image
            cell.mainImage.contentMode = .scaleToFill
            return cell
            
        }else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailViewCell", for: indexPath) as! ThumbnailViewCell
            let category = self.galleryCategory[self.selectedType]
            let thumbnail = self.thumbnailsForCategory[indexPath.row]
            let galleryPathForThumbnail = Utility.galleryPathFor(category)
            let imageWithRelativePath = Utility.relativePathFor(file: thumbnail, type: GALLERY_THUMBNAIL_FILE_EXTN, directory: galleryPathForThumbnail)
            let thumbImage = UIImage(contentsOfFile: imageWithRelativePath)
            cell.thumbImage?.image = thumbImage
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoryCollectionView {
            self.selectedType = indexPath.row
//            self.categoryCollectionView.reloadData()
            UIView.transition(with: self.thumbnailCollectionView, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.thumbnailCollectionView.reloadData()
            })
        }
        else if collectionView == self.thumbnailCollectionView {
            if (self.thumbnailsForCategory.count > 0) {
                selectedFromImageGallery = self.thumbnailsForCategory[indexPath.row]
                selectedGallery = self.galleryCategory[selectedType]
                print("Thumbnail clicked: \(selectedFromImageGallery)")
            }
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any) {
        //        self.presentingViewController?.dismiss(animated: true, completion: nil)
        performSegue(withIdentifier: "unwindFromImageGalleryToNewLogoViewController", sender: self)
    }
    
    @IBAction func actionOkay(_ sender: UIButton) {
        self.actionDismiss("")
    }
    
}

extension ImageGalleryViewController {
    
    private func loadThumbnailsFor(category: String) {
        guard category.count > 0 else {
            return
        }
        let ignoreString: String = "thumb"
        let galleryPathForThumbnail = Utility.galleryPathFor(category)
        self.thumbnailsForCategory = Utility.contentFrom(path: galleryPathForThumbnail, byDeletingExtn: true, ignoreStringSeparatedByComma: ignoreString)
        //        for filename in self.thumbnailsForCategory {
        //            print(filename)
        //        }
    }
}
















