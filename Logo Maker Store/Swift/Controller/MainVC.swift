//
//  MainVC.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 21/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UICollectionViewDelegate , UICollectionViewDataSource , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var categoryContentView: UIView!
    
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var mainSlider: Slider!
    
    @IBOutlet weak var menuTableView: UITableView!
    
    @IBOutlet weak var sliderSuperView: UIView!
    
    
    
    @IBOutlet weak var testCategoryTitle: UILabel!
    
    @IBOutlet weak var testCollectionView: UICollectionView!
    
    
    
    @IBOutlet weak var menuTopConstraint: NSLayoutConstraint!
    
    private var symbolsCategories = [String] ()

    
    var testCategories : [LogoModel] = [LogoModel]()
    
    
    @IBOutlet weak var testCategoryTitle2: UILabel!
    
    @IBAction func testAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToLogos", sender: "abstract")
    }
    
    @IBAction func testAction2(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToLogos", sender: "clasic")
    }
    
    @IBOutlet weak var testCollectionView2: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupSlider()
        
        symbolsCategories = Utility.contentFrom(path: SYMBOL_CATEGORY_PATH, byDeletingExtn: true)
        symbolsCategories.sort()
        
        
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
        menuTopConstraint.constant = view.frame.size.height
        
        contentViewHeightConstraint.constant = 1000.0
        
        //setupCollections()
        
        //printFontNames()

        setTestCategories()
    }
    
    
    func setTestCategories() {
        
        if let path = Bundle.main.path(forResource: "Logos", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResults = jsonResult as? [[String: Any]] {
                    for logoDict in jsonResults {
                        let logo = LogoModel.deserialize(from: logoDict)
                        self.testCategories.append(logo!)
                    }
                }
            } catch {
                
            }
        }

        
        testCategoryTitle.text = "Abstract"
        testCategoryTitle2.text = "Classic"

        testCollectionView.tag = 1
        testCollectionView.delegate = self
        testCollectionView.dataSource = self
        
        testCollectionView2.tag = 2
        testCollectionView2.delegate = self
        testCollectionView2.dataSource = self
    }
    
    
    
    func printFontNames() {
        let fontFamilyNames = UIFont.familyNames
        
        print("FONTS//////")
        for familyName in fontFamilyNames {
            
//            print("Font Family Name = [\(familyName)]")
            let names = UIFont.fontNames(forFamilyName: familyName)
            print("\(names)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupSlider() {
        let data : NSMutableArray = NSMutableArray()
        data.add("sliderImage1")
        data.add("sliderImage2")
        mainSlider.slides = data
        mainSlider.setupSliderView()
    }
    
    func setupCollections() {
        contentViewHeightConstraint.constant = 200.0 * (140.0 * CGFloat(symbolsCategories.count))

        self.view.layoutIfNeeded()
        for i in 0..<symbolsCategories.count {
            let category = symbolsCategories[i]
            let categoryView = CategoryCollectionView.instanceFromNib()

            //categoryView.frame = CGRect(x: 0.0, y: contentViewHeightConstraint.constant, width: view.frame.size.width, height: 140.0)
            categoryView.categoryCollectionView.tag = i
            categoryView.categoryCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MainLogoCollectionViewCell")
            //categoryView.categoryCollectionView.delegate = self
            //categoryView.categoryCollectionView.dataSource = self
            categoryView.categoryTitle.text = category
            categoryContentView.backgroundColor = .orange
            categoryContentView.translatesAutoresizingMaskIntoConstraints = false
            

            
            categoryContentView.addSubview(categoryView)
            
            
            categoryView.addConstraint(NSLayoutConstraint(item: categoryView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: view.frame.size.width))
            
            
            
            
            //categoryView.addConstraint(NSLayoutConstraint(item: categoryView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute,multiplier: 1, constant: 140.0))
            let heightConstraint = NSLayoutConstraint(item: categoryView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 140)
            
            
            categoryView.addConstraint(heightConstraint)

            
            let verticalSpace = NSLayoutConstraint(item: categoryView, attribute: .top, relatedBy: .equal, toItem: sliderSuperView, attribute: .bottom, multiplier: 1, constant: (CGFloat(140.0 * Double(i))))
            NSLayoutConstraint.activate([verticalSpace])
            
            categoryContentView.addConstraint(NSLayoutConstraint(item: categoryContentView, attribute: .centerX, relatedBy: .equal, toItem: categoryView, attribute: .centerX, multiplier: 1, constant: 0))
            
            
            
            self.view.layoutIfNeeded()
        }

        
       
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if sender is String {
            let vc = segue.destination as! LogosVC
            vc.selectedCategory = sender as! String
            vc.testCategories = testCategories
        }
        if sender is LogoModel {
            let vc = segue.destination as! NewLogoViewController
            vc.selectedLogoModel = sender as! LogoModel
        }
    }
 
    
    @IBAction func menuAction(_ sender: UIButton) {
        menuTopConstraint.constant = 0
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 2.0, initialSpringVelocity: 3.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    @IBAction func menuCloseAction(_ sender: UIButton) {
        menuTopConstraint.constant = view.frame.size.height
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 2.0, initialSpringVelocity: 3.0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @IBAction func newAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ToNewLogo", sender: self)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 1 {
            let abstractArray = testCategories.filter { $0.category == "abstract"}
            return abstractArray.count
        }
        else{
            let classicArray = testCategories.filter { $0.category == "clasic"}
            return classicArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : TestCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestCollectionViewCell", for: indexPath) as! TestCollectionViewCell
        if collectionView.tag == 1 {
            let abstractArray = testCategories.filter { $0.category == "abstract"}
            let logoModel = abstractArray[indexPath.row]
            cell.thumbImage.image = UIImage(named: logoModel.thumb)
            cell.thumbImage.backgroundColor = .orange
        }
        else{
            let classicArray = testCategories.filter { $0.category == "clasic"}
            let logoModel = classicArray[indexPath.row]
            cell.thumbImage.image = UIImage(named: logoModel.thumb)
            cell.thumbImage.backgroundColor = .orange
        }
        
       
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 1 {
            let abstractArray = testCategories.filter { $0.category == "abstract"}
            let logoModel = abstractArray[indexPath.row]
            self.performSegue(withIdentifier: "ToNewLogo", sender: logoModel)

        }
        else{
            let classicArray = testCategories.filter { $0.category == "clasic"}
            let logoModel = classicArray[indexPath.row]
            self.performSegue(withIdentifier: "ToNewLogo", sender: logoModel)

        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return symbolsCategories.count
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        if indexPath.row == 0 {
            cell.menuTitleTxt.text = "Abstract"
        }
        else{
            cell.menuTitleTxt.text = "Classic"
        }
        //cell.menuTitleTxt.text = symbolsCategories[indexPath.row]
        cell.backgroundColor = .clear
        return cell;
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.menuCloseAction(UIButton())
        if indexPath.row == 0 {
            self.performSegue(withIdentifier: "ToLogos", sender: "abstract")
        }
        else{
            self.performSegue(withIdentifier: "ToLogos", sender: "clasic")
        }
        //self.performSegue(withIdentifier: "ToLogos", sender: symbolsCategories[indexPath.row])
    }
    
    

}
