//
//  MenuViewController.swift
//  Logo Maker Store
//
//  Created by Shafi Ahmed on 30/07/18.
//  Copyright © 2018 Shafi Ahmed. All rights reserved.
//

import UIKit
import ExpandingMenu

//internal let TAB3_CLEAR: Int = 0
internal let TAB3_COLOR: Int = 3
internal let TAB3_CAMERA: Int = 0
internal let TAB3_MOBILEGALLERY: Int = 1
internal let TAB3_APPGALLERY: Int = 2

@objc public protocol MenuViewContriollerDelegate: NSObjectProtocol {
    @objc func onMenuButtonTapped(_ index: Int)-> Void //-- optional protocol to be implemented in extension
}

class MenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
//    private let tab3Titles = ["Camera", "Mobile Gallery", "Abstract", "Color", "App Gallery"]
    private let galleryTitles = ["Camera", "Mobile Gallery", "App Gallery"] //-- tab3 three menus
    private let tab2Titles = ["Symbols", "Saturation", "Opacity", "Color", "Brightness"]

    var delegate: MenuViewContriollerDelegate?
    
    private var tab: Int = 1
    private var menuTitles = [String] ()
    private var menuButton: ExpandingMenuButton? = nil
    @IBOutlet weak var buttonTextCollectionView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func initialize() {
        self.buttonTextCollectionView?.delegate = self;
        self.buttonTextCollectionView?.dataSource = self;
        menuTitles = ["Add Text", "Add Fonts", "Opacity", "Color", "Spacing", "Saturation", "Brightness"]

        self.setupMenuButton();
    }
    
    private func setupMenuButton() {
        self.view.layoutIfNeeded()
        let menuFrame:CGRect = CGRect(x:self.menuFrame().size.width - Constants.Menu.xInset,
                                      y:self.menuFrame().size.height - Constants.Menu.yInset,
                                      width:Constants.Menu.width, height:Constants.Menu.height)
        menuButton = ExpandingMenuButton(frame: menuFrame, centerImage: UIImage(named: "menuBtnMain")!,
                                                 centerHighlightedImage: UIImage(named: "menuBtnMain")!)
        self.view.addSubview(menuButton!)
        
        let undoButton = ExpandingMenuItem(size:menuFrame.size, image: UIImage(named: "menuBtnUndo")!,
                                        highlightedImage: UIImage(named: "menuBtnUndo")!, backgroundImage: UIImage(named: "menuBtnUndo"),
                                        backgroundHighlightedImage: UIImage(named: "menuBtnUndo")) { () -> Void in
                                            self.onMenuButtonTapped(Constants.MenuButtonIndex.undo)
        }

        let redoButton = ExpandingMenuItem(size:menuFrame.size, image: UIImage(named: "menuBtnRedo")!,
                                        highlightedImage: UIImage(named: "menuBtnRedo")!, backgroundImage: UIImage(named: "menuBtnRedo"),
                                        backgroundHighlightedImage: UIImage(named: "menuBtnRedo")) { () -> Void in
                                            self.onMenuButtonTapped(Constants.MenuButtonIndex.redo)
        }
        
        let angleButton = ExpandingMenuItem(size:menuFrame.size, image: UIImage(named: "menuBtnAngle")!,
                                        highlightedImage: UIImage(named: "menuBtnAngle")!, backgroundImage: UIImage(named: "menuBtnAngle"),
                                        backgroundHighlightedImage: UIImage(named: "menuBtnAngle")) { () -> Void in
                                            self.onMenuButtonTapped(Constants.MenuButtonIndex.angle)
        }
        
        let copyButton = ExpandingMenuItem(size:menuFrame.size, image: UIImage(named: "menuBtnCopy")!,
                                            highlightedImage: UIImage(named: "menuBtnCopy")!, backgroundImage: UIImage(named: "menuBtnAngle"),
                                            backgroundHighlightedImage: UIImage(named: "menuBtnCopy")) { () -> Void in
                                                self.onMenuButtonTapped(Constants.MenuButtonIndex.copy)
        }
        
        let layerButton = ExpandingMenuItem(size:menuFrame.size, image: UIImage(named: "menuBtnLayer")!,
                                           highlightedImage: UIImage(named: "menuBtnLayer")!, backgroundImage: UIImage(named: "menuBtnLayer"),
                                           backgroundHighlightedImage: UIImage(named: "menuBtnLayer")) { () -> Void in
                                            self.onMenuButtonTapped(Constants.MenuButtonIndex.layer)
                                            
        }

        menuButton?.addMenuItems([undoButton, redoButton, angleButton, copyButton, layerButton])
    }
    
    @IBAction func onBackButtonTapped(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension MenuViewController {

    //TODO: check if this is a proper implementation for this function to be overrided in its inherited class
    @objc func menuFrame()-> CGRect {
        return self.menuFrame()
    }
    
    @objc func currentTab()-> Int {
        return self.currentTab()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        self.tab = self.currentTab()
        print("section num: \(section) and tab num: \(self.tab)")
        
        if self.tab == 3{
            return self.galleryTitles.count
        }else if self.tab == 2{
            return self.tab2Titles.count
        }
        
        //-- default return Tab1
        return menuTitles.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.tab == 3{
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as! NewLogoCollectionViewCell
            cell = self.adjustCellLook(cell)
            let title = self.galleryTitles[indexPath.row]
            
            let galleryMenuPathForCategory = Utility.galleryMenuPathFor(title)
            let imageWithRelativePath = Utility.relativePathFor(file: title, type: GALLERY_THUMBNAIL_FILE_EXTN, directory: galleryMenuPathForCategory)
            let image = UIImage(named: imageWithRelativePath)
            
            cell.mainTitle.text = title.lowercased()
            cell.mainImage.image = image// UIImage(named: title)
            return cell
        }
        
        if self.tab == 2{
//            let cell = self.collectionView(collectionView, cellForItemAt: indexPath) as! NewLogoCollectionViewCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewLogoCollectionViewCell", for: indexPath) as! NewLogoCollectionViewCell
            let title = self.tab2Titles[indexPath.row]
            cell.mainTitle.text = title
            cell.mainImage.image = UIImage(named: title)
            return cell
        }

        //-- tab 1
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewLogoCollectionViewCell", for: indexPath) as! NewLogoCollectionViewCell
        let title: String = menuTitles[indexPath.row];
        cell.mainTitle.text = title;
        cell.mainImage.image = UIImage(named:title)

        return cell
    }

    private func adjustCellLook(_ aCell: NewLogoCollectionViewCell) -> NewLogoCollectionViewCell {
        let cell: NewLogoCollectionViewCell = aCell
//        var frame: CGRect = cell.frame
//        frame = CGRect(x: 5, y: 5, width: frame.size.width - 10, height: frame.size.height - 30)
//        cell.mainImage.frame = frame
//        cell.mainImage.center = cell.center
        cell.mainImage.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    private func onMenuButtonTapped(_ index: Int) {
        if let delegate:MenuViewContriollerDelegate = self.delegate {
            if delegate.responds(to: #selector(MenuViewContriollerDelegate.onMenuButtonTapped(_:))) {
                delegate.onMenuButtonTapped(index)
            }
        }
    }
}









