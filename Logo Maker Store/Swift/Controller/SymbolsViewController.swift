//
//  SymbolsViewController.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 24/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

//-- Tab 2 symbol display
class SymbolsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var thumbnailCollectionView: UICollectionView!
    
    private var symbolsCategory = [String] ()
    private var thumbnailsForCategory = [String] ()
    private var backgroundGroundCategory = [String] ()
    private var selectedType:Int  = 0
    public var selectedSvgThumb: String = ""
    public var selectedSvgCategory: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        symbolsCategory = Utility.contentFrom(path: SYMBOL_CATEGORY_PATH, byDeletingExtn: true)
        backgroundGroundCategory = Utility.contentFrom(path: GALLERY_CATEGORY_PATH, byDeletingExtn: true)
        symbolsCategory.sort()
        
        //-- make first item selected in Category CollectionView
        self.makeCollectionViewItemSelectedAt(row: 0, section: 0)
        
    }
    
    private func makeCollectionViewItemSelectedAt(row: Int, section: Int) {
        let indexPath = IndexPath(row: 0, section: 0)
        self.categoryCollectionView.selectItem(at: indexPath, animated: false, scrollPosition: .bottom)
//        self.categoryCollectionView.delegate?.collectionView!(self.categoryCollectionView, didSelectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.categoryCollectionView{ //-- load only symbol categories
            return self.symbolsCategory.count
        }
        else{
            let category: String = self.symbolsCategory[self.selectedType]
            self.loadThumbnailsFor(category: category)
            return self.thumbnailsForCategory.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.categoryCollectionView{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryViewCell", for: indexPath) as! CategoryViewCell
            let title = self.symbolsCategory[indexPath.row]
            cell.mainTitle.text = title
            
            let symbolPathForCategory = Utility.symbolPathFor(title, fileType: Constants.SymbolType.CATEGORY)
            let imageWithRelativePath = Utility.relativePathFor(file: title, type: SYMBOL_THUMBNAIL_FILE_EXTN, directory: symbolPathForCategory)
            let image = UIImage(contentsOfFile: imageWithRelativePath)
            cell.mainImage.image = image
            cell.mainImage.contentMode = .scaleToFill
            cell.borderderWidth = 3
            cell.cornerRadius = 6
            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbnailViewCell", for: indexPath) as! ThumbnailViewCell
            let category = self.symbolsCategory[self.selectedType]
            let thumbnail = self.thumbnailsForCategory[indexPath.row]
            
            let symbolPathForThumbnail = Utility.symbolPathFor(category, fileType: Constants.SymbolType.THUMBNAIL)
            let imageWithRelativePath = Utility.relativePathFor(file: thumbnail, type: SYMBOL_THUMBNAIL_FILE_EXTN, directory: symbolPathForThumbnail)
            let thumbImage = UIImage(contentsOfFile: imageWithRelativePath)
            cell.thumbImage.image = thumbImage
            cell.borderderWidth = 3
            cell.cornerRadius = 6
            
            return cell
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.categoryCollectionView {
            self.selectedType = indexPath.row
//            self.categoryCollectionView.reloadData()
            UIView.transition(with: self.thumbnailCollectionView, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.thumbnailCollectionView.reloadData()
            })
        }
        else if collectionView == self.thumbnailCollectionView {
            if (self.thumbnailsForCategory.count > 0) {
                selectedSvgThumb = self.thumbnailsForCategory[indexPath.row]
                selectedSvgCategory = self.symbolsCategory[selectedType]
//                print("Thumbnail clicked: \(selectedSvgThumb)")
            }
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOkay(_ sender: UIButton) {
        self.actionDismiss("")
        performSegue(withIdentifier: "unwindSegueToNewLogoViewController", sender: self)
    }
    
}

extension SymbolsViewController {
    
    private func loadThumbnailsFor(category: String) {
        guard category.count > 0 else {
            return
        }
        let symbolPathForThumbnail = Utility.symbolPathFor(category, fileType: Constants.SymbolType.THUMBNAIL)
        self.thumbnailsForCategory = Utility.contentFrom(path: symbolPathForThumbnail, byDeletingExtn: true)
    }
}










