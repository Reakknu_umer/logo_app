//
//  MainVC.swift
//  Logo Maker Store
//
//  Created by JEFRI SINGH on 21/06/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

class LogosVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var logoCategoryTxt: UILabel!
    
    @IBOutlet weak var mainLogoCollectionView: UICollectionView!
    
    var testCategories : [LogoModel] = [LogoModel]()

    
    var selectedCategory = ""
 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainLogoCollectionView.delegate = self
        mainLogoCollectionView.dataSource = self
        logoCategoryTxt.text = selectedCategory
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let abstractArray = testCategories.filter { $0.category == selectedCategory}
        return abstractArray.count
        /*
        let thumbnailsForCategory = self.loadThumbnailsFor(category: selectedCategory)
        return thumbnailsForCategory!.count*/
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MainLogoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainLogoCollectionViewCell", for: indexPath) as! MainLogoCollectionViewCell
        
        let abstractArray = testCategories.filter { $0.category == selectedCategory}
        
        let logoModel = abstractArray[indexPath.row]
        cell.mainLogoImageView.image = UIImage(named: logoModel.thumb)

        
        /*
        let thumbnailsForCategory = self.loadThumbnailsFor(category: selectedCategory)
        let symbolPathForThumbnail = Utility.symbolPathFor(selectedCategory, fileType: Constants.SymbolType.THUMBNAIL)
        let imageWithRelativePath = Utility.relativePathFor(file: thumbnailsForCategory![indexPath.row], type: SYMBOL_THUMBNAIL_FILE_EXTN, directory: symbolPathForThumbnail)
        let thumbImage = UIImage(contentsOfFile: imageWithRelativePath)
        cell.mainLogoImageView.image = thumbImage
*/
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.frame.size.width / 3.0
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let abstractArray = testCategories.filter { $0.category == selectedCategory}
        self.performSegue(withIdentifier: "ToNewLogo", sender: abstractArray[indexPath.row])
        /*
        let thumbnailsForCategory = self.loadThumbnailsFor(category: selectedCategory)
        self.performSegue(withIdentifier: "ToNewLogo", sender: thumbnailsForCategory![indexPath.row])*/
    }
    
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ToNewLogo" {
            let vc = segue.destination as! NewLogoViewController
            vc.selectedLogoModel = sender as! LogoModel

            //vc.selectedSvgCategory = selectedCategory
            //vc.selectedSvgThumb = sender as! String
        }
    }
    
    
    func loadThumbnailsFor(category: String) -> [String]? {
        guard category.count > 0 else {
            return nil
        }
        let symbolPathForThumbnail = Utility.symbolPathFor(category, fileType: Constants.SymbolType.THUMBNAIL)
        return Utility.contentFrom(path: symbolPathForThumbnail, byDeletingExtn: true)
    }

}
