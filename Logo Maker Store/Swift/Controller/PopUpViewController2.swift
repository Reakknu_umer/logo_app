//
//  PopUpViewController2.swift
//  Logo Maker Store
//
//  Created by Md Shafi Ahmed on 10/09/18.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

//public enum BottomSheetType{
//    case FONT, TEXTSPACING, COLOR, BACKGROUNDCOLOR, OPACITY, SATURATION, BRIGHTNESS, THUMBNAIL
//}

class PopUpViewController2: UIViewController {
    
    private let kFontDefault: String = "Arial"
    private let kFontSizeDefault: CGFloat = 14.0
//    private let kFontSpaceDefault: CGFloat = 50.0
    
    @IBOutlet weak var viewSheet: UIView!
    @IBOutlet weak var viewPanel: UIView!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var constraintHeightOfTheSheet: NSLayoutConstraint!
    
    ///Slider
    @IBOutlet weak var slider: ASValueTrackingSlider!
    @IBOutlet weak var viewSlider: UIView!

    public var newLogoVC: NewLogoViewController!
    public var heightOftheSheet:CGFloat = 0
    public var sheetType:BottomSheetType!
    public var selectedTab: Int = TAB_ONE
    public var color:UIColor = .cyan
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.constraintHeightOfTheSheet.constant = self.heightOftheSheet
        self.alignView()
        
        self.slider.popUpViewCornerRadius = 20.0;
        self.slider.setMaxFractionDigitsDisplayed (0); //-- set slider not to display decimal value
        self.slider.popUpViewColor = UIColor.init(hue: 0.52, saturation: 0.90, brightness: 0.80, alpha: 0.8)
        self.slider.font = UIFont.init(name: kFontDefault, size: kFontSizeDefault)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // self.openAnimation()
    }

    private func alignView(){
        let width  = self.view.frame.width-16
        let positionX = self.view.center.x
        let positionY = self.viewPanel.frame.height/2
        
        switch self.sheetType {
            
        case .BRIGHTNESS?, .BACKGROUNDCOLOR?:
            
            if self.sheetType == .BRIGHTNESS {
                self.labelTitle.text = "Brightness"
                if (self.selectedTab == TAB_ONE) {
                    let savedValue = Float(self.newLogoVC.canvas.textBrightness * 100)
                    slider.value = savedValue
                }
                else if (self.selectedTab == TAB_TWO) {
                    let savedValue = Float(self.newLogoVC.canvas.svgBrightness() * 100)
                    slider.value = savedValue
                }
                else if (self.selectedTab == TAB_THREE) {
                    
                }
            }
            else if self.sheetType == .BACKGROUNDCOLOR {
                if self.selectedTab == TAB_THREE {
                    self.labelTitle.text = "Background Color"
                    let savedValue = Float(self.newLogoVC.canvas.canvasBackgroundColor * 100)
                    slider.value = savedValue
                }
            }
            
            self.viewSlider.isHidden = false
            self.viewPanel.addSubview(self.viewSlider)
            self.viewSlider.frame.size.width = width
            self.viewSlider.layer.position.x = positionX
            self.viewSlider.layer.position.y = positionY
            self.slider.setThumbImage(UIImage(named: "Slider Icon"), for: .normal)
        
        default:
            break
        }
    }
    
    @IBAction func onSliderTouchUpInsideAndOutside(_ sender: UISlider) {
        performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        if self.selectedTab == TAB_ONE {
            performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
        else if self.selectedTab == TAB_TWO{
            performActionWithSliderValueForSVGTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
        else if self.selectedTab == TAB_THREE{
            performActionWithSliderValueForImageTab(value: CGFloat(sender.value), action: Constants.SliderAction.touchUpInside)
        }
    }
    
    @IBAction func onSliderValueChanged(_ sender: UISlider) {
        if self.selectedTab == TAB_ONE {
            performActionWithSliderValueForTexTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
        else if self.selectedTab == TAB_TWO{
            performActionWithSliderValueForSVGTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
        else if self.selectedTab == TAB_THREE{
            performActionWithSliderValueForImageTab(value: CGFloat(sender.value), action: Constants.SliderAction.valueChanged)
        }
    }
    
    //-- Tab1
    func performActionWithSliderValueForTexTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
            
        case .BRIGHTNESS?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.textBrightness = (value * 0.01)
            }
        default:
            break
        }
    }
    
    //-- Tab2
    func performActionWithSliderValueForSVGTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
            
        case .BRIGHTNESS?:
            if action == Constants.SliderAction.valueChanged {
                self.newLogoVC.canvas.beginUndoGrouping()
                self.newLogoVC.canvas.setSvgBrightness(value * 0.01)
            }
        default:
            break
        }
    }
    
    //-- Tab3
    func performActionWithSliderValueForImageTab(value:CGFloat, action: Int) {
        if action == Constants.SliderAction.touchUpInside || action == Constants.SliderAction.touchUpOutside{
            self.newLogoVC.canvas.endUndoGrouping()
        }
        
        switch self.sheetType {
            
        case .BACKGROUNDCOLOR?:
            if action == Constants.SliderAction.valueChanged {
//                self.newLogoVC.removeCanvasImage()
                self.newLogoVC.canvas.canvasBackgroundColor = (value * 0.01)
            }
        default:
            break
        }
    }
    
    private func openAnimation(){
        UIView.animate(withDuration: 0.3) {
            self.newLogoVC.buttonTextCollectionView?.alpha = 0
        }
    }
    
    @IBAction func actionDismiss(_ sender: Any?) {
        UIView.animate(withDuration: 0.3, animations: {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }) { (bool) in
            
        }
        performSegue(withIdentifier: "unwindFromPopupViewController2", sender: self)
    }
}










