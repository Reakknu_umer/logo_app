//
//  CategoryCollectionView.swift
//  Logo Maker Store
//
//  Created by iBuildX on 13/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit

class CategoryCollectionView: UIView {

    
    @IBOutlet weak var categoryTitle: UILabel!
    
    @IBOutlet weak var categoryMoreBtn: UIButton!
    
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    
    class func instanceFromNib() -> CategoryCollectionView {
        return UINib(nibName: "CategoryCollectionView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CategoryCollectionView
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
