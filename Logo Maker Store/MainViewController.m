//
//  MainViewController.m
//  Logo Maker Store
//
//  Created by iBuildX on 21/05/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "MainViewController.h"
#import "MainLogoCollectionViewCell.h"
#import "MenuTableViewCell.h"
#import "Logo_Maker_Store-Swift.h"

@interface MainViewController ()

@end


@implementation MainViewController

NSArray *myArray = nil;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupSlider];
    
    myArray = @[@"Old School", @"Vintage", @"Hipstar", @"NFL", @"Retro",@"Classic",@"Colorfull",@"Abstract Baseball",@"Badges"];
    
    
    self.myLogosCollectionView.delegate = self;
    self.myLogosCollectionView.dataSource = self;
    
    
    self.basicLogoCollectionView.delegate = self;
    self.basicLogoCollectionView.dataSource = self;
    
    self.iconTypeLogoCollectionView.delegate = self;
    self.iconTypeLogoCollectionView.dataSource = self;
    
    self.letterLogoCollectionView.delegate = self;
    self.letterLogoCollectionView.dataSource = self;
    
    self.menuTableView.delegate = self;
    self.menuTableView.dataSource = self;
    
    self.menuTopConstraint.constant = self.view.frame.size.height;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupSlider {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    [data addObject:@"sliderImage1"];
    [data addObject:@"sliderImage2"];
    self.mainSlider.slides = data;
    [self.mainSlider setupSliderView];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}


- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MainLogoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainLogoCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return myArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuTableViewCell"];
    cell.menuTitleTxt.text = myArray[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self menuCloseAction:nil];
    [self performSegueWithIdentifier:@"ToLogos" sender:self];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)menuAction:(UIButton *)sender {
    self.menuTopConstraint.constant = 0;
    [UIView animateWithDuration:0.3 delay:0.0 usingSpringWithDamping:2.0 initialSpringVelocity:3.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)menuCloseAction:(UIButton *)sender {
    self.menuTopConstraint.constant = self.view.frame.size.height;
    [UIView animateWithDuration:0.3 delay:0.0 usingSpringWithDamping:2.0 initialSpringVelocity:3.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)newAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToNewLogo" sender:self];
}
@end
