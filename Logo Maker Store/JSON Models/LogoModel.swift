
//
//  LogoModel.swift
//  Logo Maker Store
//
//  Created by iBuildX on 13/10/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

import UIKit
import HandyJSON

public class LogoModel: HandyJSON {
    var height: Float!
    var width: Float!
    var thumb: String!
    var name: String!
    var category: String!

   
    var layers: Array<LayersModel>?
    
    
    required public init() {}
}


class LayersModel: HandyJSON {
    
    var type: String!
    var name: String!
    var x: Float!
    var y: Float!
    var z: Float!
    var height: Float!
    var width: Float!
    var font_size: Float!
    var value: String!
    var color: String!

    required init() {}
}
/*
{
    "height" : "600",
    "width" : "600",
    "thumb" : "pre_made_1.jpg",
    "name" : "pre_made_1",
    "category" : "test",
    "layers" : [
    {
    "type" : "image",
    "name" : "background_1.jpg",
    "x" : "0",
    "y" : "0",
    "z" : "0",
    "height" : "600",
    "width" : "600"
    },
    {
    "type" : "svg",
    "name" : "symbol_1.svg",
    "x" : "10",
    "y" : "20",
    "z" : "1",
    "height" : "200",
    "width" : "300"
    },
    {
    "type" : "svg",
    "name" : "symbol_2.svg",
    "x" : "200",
    "y" : "200",
    "z" : "2",
    "height" : "100",
    "width" : "50"
    },
    {
    "type" : "text",
    "name" : "SF_UI",
    "x" : "200",
    "y" : "100",
    "z" : "3",
    "height" : "300",
    "width" : "50",
    "font_size" : "20"
    }
    ]
}*/
